import tensorflow as tf
import numpy as np
import os,glob,cv2,time
import sys,argparse
import operator
import shutil
## Let us restore the saved model 
# for graphic card start
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto(
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
    # device_count = {'GPU': 1}
)
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
set_session(sess)

# for graphic card end
#sess = tf.Session()
# Step-1: Recreate the network graph. At this step only graph is created.
saver = tf.train.import_meta_graph('model/plants-model.meta')
# Step-2: Now let's load the weights saved using the restore method.
saver.restore(sess, tf.train.latest_checkpoint('model/'))
#classes = ['Apple___Apple_scab','Apple___Black_rot','Apple___Cedar_apple_rust','Apple___healthy','ClusterFig___Bacterial_spot','ClusterFig___healthy','Grape___Esca_(Black_Measles)','Grape___healthy']
classes = ['Tomato___Bacterial_spot','Tomato___healthy','Tomato___Late_blight','Tomato___Septoria_leaf_spot','Tomato___Tomato_Yellow_Leaf_Curl_Virus']
num_classes = len(classes)
for className in classes:
	dir="training_data\\"+className+"\\"
	dirName=className
	x = [os.path.join(r,file) for r,d,f in os.walk(dir) for file in f]
	x.sort(key=os.path.getmtime)
	# First, pass the path of the image
	millis = str(round(time.time() * 1000))
	logfile = open('output/'+millis+'.csv','w')
	logframes = open('output/'+millis+'_frames.csv','w')

	#x.sort(key=lambda x: os.path.getmtime(x))
	for f in x:

		#dir_path = os.path.dirname(os.path.realpath(__file__))
		#print(dir_path)
		#image_path=sys.argv[1] 
		#filename = dir_path +'/' +image_path
		filename=f
		image_size=128
		num_channels=3
		images = []
		# Reading the image using OpenCV
		image = cv2.imread(filename)
		# Resizing the image to our desired size and preprocessing will be done exactly as done during training
		image = cv2.resize(image, (image_size, image_size),0,0, cv2.INTER_LINEAR)
		images.append(image)
		images = np.array(images, dtype=np.uint8)
		images = images.astype('float32')
		images = np.multiply(images, 1.0/255.0) 
		#The input to the network is of shape [None image_size image_size num_channels]. Hence we reshape.
		x_batch = images.reshape(1, image_size,image_size,num_channels)



		# Accessing the default graph which we have restored
		graph = tf.get_default_graph()

		# Now, let's get hold of the op that we can be processed to get the output.
		# In the original network y_pred is the tensor that is the prediction of the network
		y_pred = graph.get_tensor_by_name("y_pred:0")

		## Let's feed the images to the input placeholders
		x= graph.get_tensor_by_name("x:0") 
		print(x)
		y_true = graph.get_tensor_by_name("y_true:0") 
		
		y_test_images = np.zeros((1, num_classes)) 


		### Creating the feed_dict that is required to be fed to calculate y_pred 
		feed_dict_testing = {x: x_batch, y_true: y_test_images}
		result=sess.run(y_pred, feed_dict=feed_dict_testing)
		r=",".join(map(str, result));
		# result is of this format [probabiliy_of_rose probability_of_sunflower]
		#print(result[0])
		min_index, min_value = max(enumerate(result[0]), key=operator.itemgetter(1))
		result[0][min_index]=0;
		min_index1, min_value1 = max(enumerate(result[0]), key=operator.itemgetter(1))
		#print(min_index)
		#print(min_value)
		print("{}>{}>{}>{}>{} \n".format(filename, classes[min_index],min_value, classes[min_index1],min_value1));
		i=filename.rfind('\\')
		frameno=filename[(i+1):].rsplit(".")[0]
		if dirName == classes[min_index]:
			if not os.path.exists("ideal_training/"+dirName):
				os.makedirs("ideal_training/"+dirName)
			shutil.copy2(filename, "ideal_training/"+dirName+"/"+filename[(i+1):])
		s="{}>{}>{}>{}>{}>{} \n".format(dirName,frameno, classes[min_index],min_value, classes[min_index1],min_value1);
		logfile.write(s);
		if(min_value>0.4):
			logframes.write("{}\n".format(frameno));
	logfile.close()
	logframes.close()