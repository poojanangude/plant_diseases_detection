-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema plantdisease
--

CREATE DATABASE IF NOT EXISTS plantdisease;
USE plantdisease;

--
-- Definition of table `fertilizer`
--

DROP TABLE IF EXISTS `fertilizer`;
CREATE TABLE `fertilizer` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phlevel` longtext NOT NULL,
  `fertilizerneed` longtext NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fertilizer`
--

/*!40000 ALTER TABLE `fertilizer` DISABLE KEYS */;
INSERT INTO `fertilizer` (`fid`,`phlevel`,`fertilizerneed`) VALUES 
 (1,'up','increase the pH of soil include wood ash, industrial calcium oxide (burnt lime), magnesium oxide, basic slag (calcium silicate), and oyster shells. These products increase the pH of soils through various acid-base reactions. '),
 (2,'equal','NO need to add fertilizer'),
 (3,'down',' it is often more efficient to add phosphorus, iron, manganese, copper and/or zinc instead, because deficiencies of these nutrients are the most common reasons for poor plant growth in calcareous soils.');
/*!40000 ALTER TABLE `fertilizer` ENABLE KEYS */;


--
-- Definition of table `plantdisease`
--

DROP TABLE IF EXISTS `plantdisease`;
CREATE TABLE `plantdisease` (
  `idplantdisease` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `palntName` varchar(45) DEFAULT NULL,
  `plantDisease` varchar(45) DEFAULT NULL,
  `symptoms` longtext,
  `preventive` longtext,
  `pesticide` longtext,
  `pesticidecost` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`idplantdisease`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plantdisease`
--

/*!40000 ALTER TABLE `plantdisease` DISABLE KEYS */;
INSERT INTO `plantdisease` (`idplantdisease`,`palntName`,`plantDisease`,`symptoms`,`preventive`,`pesticide`,`pesticidecost`) VALUES 
 (1,'Cotton','Angular leaf spot','Small water-soaked spots appear on the under surface of cotyledons, which may dry and wither. Such spots also appear on the leaves. They become angular bound by veinlets and turn brown to black in colour. Several small spots may coalesce. The infected petiole may collapse. Elongated, sunken and dark brown to black lesions appear on stem, petioles and branches.The young stems may be girdled and killed in the black arm phase. Sunken black lesions may be seen on the bolls. Young boll may fall-off. The attacked stem becomes weak. Bacterial slime is exuded on the brown lesions. Discolouration of lint may take place.','Cultural and Sanitary Methods\r\n\r\nX. axonopodis pv. malvacearum cannot survive in the soil outside of crop residues and is therefore readily controlled with rotations. One crop season without cotton is usually sufficient to virtually eliminate crop residues as a source of primary inoculum. If this is combined with seed certification to ensure that crops used for seed production are free of bacterial blight, the disease can be controlled even where susceptible varieties are grown (Schnathorst, 1966). Cotton seed can also be treated with bactericides to reduce the risk of seed transmission (see Seed Treatment under Seedborne Aspects of Disease).','Carbendazim 12 Mancozeb 63 WP                                                                                    \r\n2. (a) Externally seed borne infection can be eradicated by delinting the seed with Cone H2SO4 for 5 minutes, wash with lime solution to neutralise the effect and finally washing with running water to remove the residue and drying seeds.\r\n\r\n(b) Internally seed borne infection can be eradicated by soaking seeds overnight in 100 ppm streptomycin sulphate or Agrimycin.\r\n\r\n3. Secondary spread of the disease can be controlled by spraying the crop with streptomycin sulphate 100 ppm + Copper oxychloride (0.25%) at an interval of 15 days.',500),
 (2,'Cotton','Grey mildew','The fungus usually attacks the older leaves causing irregular to angular, pale, translucent spots. They are usually restricted by the vein lets and appear mostly on the lower surface of the leaf though occasionally on the upper surface. A few to over a hundred spots may be found on a single leaf. In severe infections the leaves turn yellowish brown and fall off prematurely.',NULL,NULL,500),
 (4,'Cotton','Reddening or lalya','Leaves of the affected cotton plants turn yellow or red during all the stages of growth. The discolouration starts at the margins and extend inwards. The red colour may also develop as patches in interveinal portion of the leaves. In severe cases, the whole leaf is involved. The affected leaves roll down wards, begin to dry and ultimately shed. The affected plant exhibits early fruiting and excessive shedding of the bolls.','Avoid planting the susceptible hybrids/varieties.  ???? ???????? ?????????? ?Provide mulching if reddening is due to drought conditions.  ?Balanced and timely use of fertilizer.','Green Meta - Liquid Bio Pesticide - metarhizium anisopliae - Quantity : 500ML                          Application of F.Y.M. or compost or nitrogenous manures which may help to improve the physical condition of soil ultimately minimising the incidence.\r\n\r\n2. Spray 2% diammonium phosphate or 1% urea. Avoid hot winds by providing wind breaks like shevari.\r\n\r\n3. The chlorophyll in the leaf cell decomposes and probably results in the formation of anthocyanin pigment as soon as temperature falls below 34°C.',290),
 (5,'Promogranate','Anthracnose: Colletotrichum gloeosporioides','1.Appears as small regular or irregular dull violet or black leaf spots with yellowish halos.2.Leaves turn yellow and fall out.3.Diseased portions appear with minute, black dots representing acervuli.','Avoid planting highly susceptible species including Modesto ash (Fraxinus velutina variety glabra), American sycamore (Platanus occidentalis), and the London plane tree (P. acerifolia or P. hybrida). Although anthracnose doesn’t affect California sycamore (P. racemosa) in the southern part of the state, it does infect this tree in the north, so it is better to avoid planting it in this region. The ash varieties Moraine and Raywood and the Evergreen (Shamel) ash are more resistant to anthracnose than other varieties. For evergreen Chinese elm, plant the more resistant Drake cultivar instead of True Green or Evergreen. Table 1 shows the relative susceptibility of some landscape trees to anthracnose.','Sixer dhanuka Carbendazim/ Difenconazole or Thiophanate methyl at 0.25ml/lit sprays at fort-nightly intervals have been found effective.      2.Spraying of Difenconazole 25 EC at 1.0 ml/lit or Prochloraz 45 EC at 0.75ml/lit were effective against anthracnose disease.       3.Spraying of systemic fungicides namely Hexaconazole @1ml/lit / Thiophanate methyl @ 1g/lit/ Carbendazim @ 1g/lit at 20 days interval is quite effective. Among the contact fungicides Chlorothalonil (2g/l) followed by Mancozeb (2g/l) were more efficacious',340),
 (6,'Promogranate',' Bacterial blight: Xanthomonas axonopodis','Appearance of one to several small water soaked, dark coloured irregular spots on leaves resulting in premature defoliation under severe cases.','1.Wide row spacing\r\n2. Selection of disease free seedlings for fresh planting\r\n3.Pruning affected branches, fruits regularly and burning\r\n4.Bahar should be done in Hasta or Ambe bahar\r\n5.Give minimum four month rest after harvesting the fruits','Before pruning it should be sprayed with 1% Bordeaux mixture\r\nAfter Ethrel  spraying or defoliation, Paste or smear with 0.5g Streptomycin Sulphate + 2.5g Copper oxy chloride  + 200g red oxide per lit of water.\r\nSpray 0.5 g Streptomycin Sulphate or Bacterinashak +2.5 g Copper oxy chloride per litre of water.\r\nNext day or another day spray with 1 g ZnSo4 +1 g MgSo4 +1 g Boron +1g CaSo4 per lit of water.',1400),
 (7,'Promogranate','Cercospora fruit spot : Cercospora punicae','Light zonate brown spots appear on the leaves and fruits. Black and elliptic spots appear on the twigs. \r\nThe affected areas in the twigs become flattened and depressed with raised edge.','The diseased fruits should be collected and destroyed.\r\nPruning and destruction of diseased twigs','Spray the crop with Hexaconazole 5 EC or Propiconazole 25 EC or Difenconazole 25 EC @ 1 ml / lit.\r\nKitazin 48 EC 2 ml / lit or Carbendazim (1g) or Chlorothalonil 75 WP (2.5 g) or Copper oxychloride 50 WP (3 g) per lit.\r\nThiophanate methyl / Carbendazim @ 1g / lit were most effective.\r\nAmong the contact fungicides Chlorothalonil @ 2 ml followed by Mancozeb @ 2 g were more efficacious.',500),
 (8,'Grapes','Downy mildew','The fungus is an obligate pathogen which can attack all green parts of the vine.\r\nSymptoms of this disease are frequently confused with those of powdery mildew. Infected leaves develop pale yellow-green lesions which gradually turn brown. Severely infected leaves often drop prematurely.\r\nInfected petioles, tendrils, and shoots often curl, develop a shepherd\'s crook, and eventually turn brown and die.\r\nYoung berries are highly susceptible to infection and are often covered with white fruiting structures of the fungus. Infected older berries of white cultivars may turn dull gray-green, whereas those of black cultivars turn pinkish red.','Avoid planting the susceptible hybrids/varieties.  ???? ???????? ?????????? ?Provide mulching if reddening is due to drought conditions.  ?Balanced and timely use of fertilizer.','Spray the crop with Hexaconazole 5 EC or Propiconazole 25 EC or Difenconazole 25 EC @ 1 ml / lit.\r\nKitazin 48 EC 2 ml / lit or Carbendazim (1g) or Chlorothalonil 75 WP (2.5 g) or Copper oxychloride 50 WP (3 g) per lit.\r\nThiophanate methyl / Carbendazim @ 1g / lit were most effective.\r\nAmong the contact fungicides Chlorothalonil @ 2 ml followed by Mancozeb @ 2 g were more efficacious.',1400),
 (9,'Grapes','Powdery mildew','Powdery mildew, caused by the fungus Uncinulanecator, can infect all green tissues of the grapevine.\r\nTissues are generally susceptible to infection throughout the growing season.\r\nDiseased leaves appear whitish gray, dusty, or have a powdery white appearance.','Avoid planting the susceptible hybrids/varieties.  ???? ???????? ?????????? ?Provide mulching if reddening is due to drought conditions.  ?Balanced and timely use of fertilizer.','Spray the crop with Hexaconazole 5 EC or Propiconazole 25 EC or Difenconazole 25 EC @ 1 ml / lit.\r\nKitazin 48 EC 2 ml / lit or Carbendazim (1g) or Chlorothalonil 75 WP (2.5 g) or Copper oxychloride 50 WP (3 g) per lit.\r\nThiophanate methyl / Carbendazim @ 1g / lit were most effective.\r\nAmong the contact fungicides Chlorothalonil @ 2 ml followed by Mancozeb @ 2 g were more efficacious.',1400),
 (10,'Grapes','Anthracnose',' On the leaves,  small brown spots develop which fall out when the tissue dies creating a shot-hole effect. Young affected shoots become distorted and dieback. ','Avoid planting the susceptible hybrids/varieties.  ???? ???????? ?????????? ?Provide mulching if reddening is due to drought conditions.  ?Balanced and timely use of fertilizer.','Spray the crop with Hexaconazole 5 EC or Propiconazole 25 EC or Difenconazole 25 EC @ 1 ml / lit.\r\nKitazin 48 EC 2 ml / lit or Carbendazim (1g) or Chlorothalonil 75 WP (2.5 g) or Copper oxychloride 50 WP (3 g) per lit.\r\nThiophanate methyl / Carbendazim @ 1g / lit were most effective.\r\nAmong the contact fungicides Chlorothalonil @ 2 ml followed by Mancozeb @ 2 g were more efficacious.',1400),
 (11,'Tomato','Late blight','Powdery mildew, caused by the fungus Uncinulanecator, can infect all green tissues of the grapevine.\r\nTissues are generally susceptible to infection throughout the growing season.\r\nDiseased leaves appear whitish gray, dusty, or have a powdery white appearance.','Avoid planting the susceptible hybrids/varieties.  ???? ???????? ?????????? ?Provide mulching if reddening is due to drought conditions.  ?Balanced and timely use of fertilizer.','Spray the crop with Hexaconazole 5 EC or Propiconazole 25 EC or Difenconazole 25 EC @ 1 ml / lit.\r\nKitazin 48 EC 2 ml / lit or Carbendazim (1g) or Chlorothalonil 75 WP (2.5 g) or Copper oxychloride 50 WP (3 g) per lit.\r\nThiophanate methyl / Carbendazim @ 1g / lit were most effective.\r\nAmong the contact fungicides Chlorothalonil @ 2 ml followed by Mancozeb @ 2 g were more efficacious.',1400),
 (12,'Tomato','Leaf Mold',' On the leaves,  small brown spots develop which fall out when the tissue dies creating a shot-hole effect. Young affected shoots become distorted and dieback. ','Avoid planting the susceptible hybrids/varieties.  ???? ???????? ?????????? ?Provide mulching if reddening is due to drought conditions.  ?Balanced and timely use of fertilizer.','Spray the crop with Hexaconazole 5 EC or Propiconazole 25 EC or Difenconazole 25 EC @ 1 ml / lit.\r\nKitazin 48 EC 2 ml / lit or Carbendazim (1g) or Chlorothalonil 75 WP (2.5 g) or Copper oxychloride 50 WP (3 g) per lit.\r\nThiophanate methyl / Carbendazim @ 1g / lit were most effective.\r\nAmong the contact fungicides Chlorothalonil @ 2 ml followed by Mancozeb @ 2 g were more efficacious.',1400),
 (13,'Tomato','Septoria leaf spot',' On the leaves,  small brown spots develop which fall out when the tissue dies creating a shot-hole effect. Young affected shoots become distorted and dieback. ','Avoid planting the susceptible hybrids/varieties.  ???? ???????? ?????????? ?Provide mulching if reddening is due to drought conditions.  ?Balanced and timely use of fertilizer.','Spray the crop with Hexaconazole 5 EC or Propiconazole 25 EC or Difenconazole 25 EC @ 1 ml / lit.\r\nKitazin 48 EC 2 ml / lit or Carbendazim (1g) or Chlorothalonil 75 WP (2.5 g) or Copper oxychloride 50 WP (3 g) per lit.\r\nThiophanate methyl / Carbendazim @ 1g / lit were most effective.\r\nAmong the contact fungicides Chlorothalonil @ 2 ml followed by Mancozeb @ 2 g were more efficacious.',1400);
/*!40000 ALTER TABLE `plantdisease` ENABLE KEYS */;


--
-- Definition of table `useraccounts`
--

DROP TABLE IF EXISTS `useraccounts`;
CREATE TABLE `useraccounts` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uname` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `phoneno` varchar(45) DEFAULT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useraccounts`
--

/*!40000 ALTER TABLE `useraccounts` DISABLE KEYS */;
INSERT INTO `useraccounts` (`userid`,`uname`,`pass`,`phoneno`,`fname`,`lname`) VALUES 
 (5,'project','project','9766750000','rajesh','agrawal');
/*!40000 ALTER TABLE `useraccounts` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
