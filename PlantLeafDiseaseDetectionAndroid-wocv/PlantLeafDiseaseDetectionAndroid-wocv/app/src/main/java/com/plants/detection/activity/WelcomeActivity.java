package com.plants.detection.activity;


import android.Manifest;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Window;

import com.plants.detection.opencv.AndroidConstants;
import com.plants.detection.opencv.StringHelper;

import java.util.HashMap;
import java.util.Map;


public class WelcomeActivity extends CommonActivity {

    private boolean mPermissionReady;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.welcome);
//        findViewById(R.id.btnOpenCv).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mPermissionReady) {
//                    startActivity(new Intent(WelcomeActivity.this, CameraCaptureActivity.class));
//                }
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storagePermssion = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        mPermissionReady = cameraPermission == PackageManager.PERMISSION_GRANTED
                && storagePermssion == PackageManager.PERMISSION_GRANTED;
        if (!mPermissionReady) {
            requirePermissions();
        } else {
            checkConnectivity();
        }
    }

    private void requirePermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Map<String, Integer> perm = new HashMap<>();
        perm.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_DENIED);
        perm.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_DENIED);
        for (int i = 0; i < permissions.length; i++) {
            perm.put(permissions[i], grantResults[i]);
        }
        if (perm.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && perm.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            mPermissionReady = true;
            checkConnectivity();
        } else {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    || !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(this)
                        .setMessage(R.string.permission_warning)
                        .setPositiveButton(R.string.dismiss, null)
                        .show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void checkConnectivity() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                SharedPreferences s = PreferenceManager
                        .getDefaultSharedPreferences(WelcomeActivity.this);

                boolean success = checkConnectivityServer();
                if (success) {
                    SharedPreferences.Editor editor = s.edit();
                    editor.putString("MAIN_SERVER_IP",
                            com.plants.detection.opencv.AndroidConstants.MAIN_SERVER_IP + "");
                    editor.putString("MAIN_SERVER_PORT",
                            AndroidConstants.MAIN_SERVER_PORT + "");

                    editor.putString("MAIN_SERVER_CONTEXT",
                            AndroidConstants.MAIN_SERVER_CONTEXT + "");

                    editor.commit();
                    proceed();

                } else {

                    String MAIN_SERVER_IP = s.getString("MAIN_SERVER_IP",
                            AndroidConstants.MAIN_SERVER_IP);
                    String MAIN_SERVER_PORT = s.getString("MAIN_SERVER_PORT",
                            AndroidConstants.MAIN_SERVER_PORT);
                    String MAIN_SERVER_CONTEXT = s.getString("MAIN_SERVER_CONTEXT",
                            AndroidConstants.MAIN_SERVER_CONTEXT);
                    if (!MAIN_SERVER_IP
                            .equalsIgnoreCase(AndroidConstants.MAIN_SERVER_IP)
                            || !MAIN_SERVER_PORT
                            .equalsIgnoreCase(AndroidConstants.MAIN_SERVER_PORT)) {
                        success = checkConnectivityServer(MAIN_SERVER_IP,
                                StringHelper.n2i(MAIN_SERVER_PORT));
                        if (success) {
                            AndroidConstants.MAIN_SERVER_IP = MAIN_SERVER_IP;
                            AndroidConstants.MAIN_SERVER_PORT = MAIN_SERVER_PORT;
                            AndroidConstants.MAIN_SERVER_CONTEXT = MAIN_SERVER_CONTEXT;
                            proceed();
                        } else {
                            System.out.println("Redirecting to Config 1");
                            go(ConfigTabActivity.class);
                        }
                    } else {
                        System.out.println("Redirecting to Config 2");
                        go(ConfigTabActivity.class);
                    }
                }
            }
        }, 5000);

    }

    public void proceed() {
        go(MainMenuActivity.class);
    }
}
