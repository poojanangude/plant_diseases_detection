package com.plants.detection.opencv;

import android.graphics.Bitmap;

public class AndroidConstants {



    public static final String LOG_TAG = "CvCameraPreview";
    public static String MAIN_SERVER_IP = "192.168.0.122";
    public static String MAIN_SERVER_PORT = "8080";
    public static String MAIN_SERVER_CONTEXT = "PlantPathology";

    public static String HTTP_URL() {
        return "http://" + AndroidConstants.MAIN_SERVER_IP + ":"
                + AndroidConstants.MAIN_SERVER_PORT
                + "/"+MAIN_SERVER_CONTEXT+"/";
    }
    public static String AJAX_URL() {
        return HTTP_URL() + "tiles/ajax.jsp?phone=1&";
    }
    public static Bitmap bitmap=null;
    public static String plantProbabilities_all="";
    public static String plantClasses_all="";

    public static  int previousIndex=0;
    public static String plantProbability="";
    public static String plantName="";
    public static String plantDisease="";
    public static String symptoms="";
    public static String preventive="";
    public static String pesticides="";
}
