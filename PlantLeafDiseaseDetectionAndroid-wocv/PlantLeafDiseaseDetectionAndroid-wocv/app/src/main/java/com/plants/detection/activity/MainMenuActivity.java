package com.plants.detection.activity;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import com.plants.detection.opencv.AndroidConstants;
import com.plants.detection.opencv.HttpView;

import java.io.FileOutputStream;
import java.io.IOException;


public class MainMenuActivity extends CommonActivity {

    private boolean mPermissionReady;
    private static final int pic_id = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        findViewById(R.id.btnOpenCv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                go(CameraCaptureActivity.class);

                // Create the camera_intent ACTION_IMAGE_CAPTURE
                // it will open the camera for capture the image
                Intent camera_intent
                        = new Intent(MediaStore
                        .ACTION_IMAGE_CAPTURE);

                // Start the activity with camera_intent,
                // and request pic id
                startActivityForResult(camera_intent, pic_id);
            }
        });

        findViewById(R.id.recognizePictureBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(PlantAnalysisActivity.class);
            }
        });
//        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                go(PlantCompareActivity.class);
//            }
//        });

    }

    // This method will help to retrieve the image
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {

        // Match the request 'pic id with requestCode
        if (requestCode == pic_id) {

            // BitMap is data structure of image file
            // which stor the image in memory
            Bitmap photo = (Bitmap) data.getExtras()
                    .get("data");

            String fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/test.png";
            try {
                FileOutputStream out = new FileOutputStream(fileName);

                photo.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                out.close();
                Intent intent = new Intent(MainMenuActivity.this, PlantAnalysisActivity.class);
                intent.putExtra("filename", fileName);
                intent.putExtra("capture", "1");
                startActivity(intent);
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (IOException e) {
                e.printStackTrace();
            }


            // Set the image in imageview for display
//            click_image_id.setImageBitmap(photo);
        }
    }

    public void viewDetails() {
        String url = AndroidConstants.AJAX_URL() + "methodId=getPhValue";
        Object obj = HttpView.connect2ServerObject(url);
        String returnOutput = "";
        if (obj != null) {
            try {
                returnOutput = (String) obj;
                AlertDialog alertDialog = new AlertDialog.Builder(
                        this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Pesticide Info");

                // Setting Dialog Message
                if (returnOutput.length() > 2) {
                    String data[] = returnOutput.split("->");
                    String finalData = "";
                    //pm.getPesticide()+"->"+pm.getPesticidecost()+"->"     +pm.getPreventive();
                    finalData = "PH Value:" + data[0] + "\nFertilizer to provied:" + data[1];
                    alertDialog.setMessage(finalData);


                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog closed
                            Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
