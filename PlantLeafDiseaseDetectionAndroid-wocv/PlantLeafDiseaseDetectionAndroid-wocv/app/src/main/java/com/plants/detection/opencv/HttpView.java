package com.plants.detection.opencv;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.util.Log;


import com.plants.detection.activity.R;
import com.plants.detection.helper.IOUtil;
import com.plants.detection.helper.SSLContextFactory;

import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.Reader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.net.ssl.SSLContext;


public class HttpView {
    //	public static String result[][] = new String[500][];
//	public static int count = -1;
    static String TAG = AndroidConstants.LOG_TAG;


    public String connectToServer(String url) {
        // TODO Auto-generated method stub
        URL myURL;
        try {
            myURL = new URL(url);
            Log.v(TAG, "Sending JSON URL " + url);
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL
                    .openConnection();

//                if (myURLConnection instanceof HttpsURLConnection) {
//                    Log.v(TAG, "Setting SSL " + url);
//                    ((HttpsURLConnection) myURLConnection).setSSLSocketFactory(sslContext.getSocketFactory());
//                }
            // String userCredentials = "username:password";
            myURLConnection.setRequestMethod("GET");
//                myURLConnection.setRequestProperty("Content-Type",
//                        "application/x-www-form-urlencoded");
//
//                myURLConnection.setRequestProperty("Content-Language", "en-US");
            myURLConnection.setUseCaches(false);
            myURLConnection.setDoInput(true);
            myURLConnection.setDoOutput(false);

            Log.v(TAG, "Writing  JSON" + url);

            StringBuffer buffer = new StringBuffer();
            if (myURLConnection.getResponseCode() == 200) {
                InputStream is = myURLConnection.getInputStream();
                while (true) {
                    byte[] a = new byte[1024];
                    int ret = is.read(a);
                    if (ret == -1) {
                        break;
                    }
                    buffer.append(new String(a, 0, ret));
                }
                is.close();

                Log.v(TAG, "RESPONSE => " + buffer.toString());
                return buffer.toString();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }


    public static Bitmap drawable_from_url(String url) {
        Bitmap x = null;
        try {

            System.out.println("URL IS " + url);
            HttpURLConnection connection = (HttpURLConnection) new URL(url)
                    .openConnection();
            connection.setRequestProperty("User-agent", "Mozilla/4.0");

            connection.connect();
            InputStream input = connection.getInputStream();

            x = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return x;
    }

    public static boolean checkConnectivityServer(String ip, int port) {
        boolean success = false;
        try {
            Socket soc = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(ip, port);
            soc.connect(socketAddress, 3000);
            success = true;
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        System.out.println(" Connecting to server " + success);
        return success;

    }


//    public static String createURL(HashMap param) {
//
//        String parameterURLString = "";
//
//        Set set = param.keySet();
//        // Get an iterator
//        Iterator i = set.iterator();
//        // Display elements
//        while (i.hasNext()) {
//            String key = StringHelper.n2s(i.next());
//            String value = StringHelper.n2s(param.get(key));
//            value = URLEncoder.encode(value);
//            parameterURLString += "&" + key + "=" + value;
//
//        }
//
//        if (parameterURLString.length() > 0) {
//            parameterURLString = parameterURLString.substring(1);
//
//        }
//        String url = AndroidConstants.AUTH_URL() + parameterURLString;
//        System.out.println("url  " + url);
//        return url;
//    }


    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("HttpView", ex.toString());
        }
        return null;
    }

    public static String urlEncode(String sUrl) {
        if (sUrl == null) {
            return null;
        }
        int i = 0;
        String urlOK = "";
        while (i < sUrl.length()) {
            if (sUrl.charAt(i) == '<') {
                urlOK = urlOK + "%3C";
            } else if (sUrl.charAt(i) == '/') {
                urlOK = urlOK + "%2F";
            } else if (sUrl.charAt(i) == '\\') {
                urlOK = urlOK + "%2F";
            } else if (sUrl.charAt(i) == '>') {
                urlOK = urlOK + "%3E";
            } else if (sUrl.charAt(i) == ' ') {
                urlOK = urlOK + "%20";
            } else if (sUrl.charAt(i) == ':') {
                urlOK = urlOK + "%3A";
            } else if (sUrl.charAt(i) == '-') {
                urlOK = urlOK + "%2D";
            } else {
                urlOK = urlOK + sUrl.charAt(i);
            }
            i++;
        }
        return (urlOK);
    }

    public static String getResponseBody(HttpResponse response) {
        String response_text = null;
        HttpEntity entity = null;
        try {
            entity = response.getEntity();
            response_text = _getResponseBody(entity);

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (IOException e1) {
                }
            }
        }
        return response_text;
    }

    public static String _getResponseBody(final HttpEntity entity)
            throws IOException, ParseException {
        if (entity == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        InputStream instream = entity.getContent();
        if (instream == null) {
            return "";
        }

        if (entity.getContentLength() > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(

                    "HTTP entity too large to be buffered in memory");
        }
        String charset = getContentCharSet(entity);
        if (charset == null) {
            charset = HTTP.DEFAULT_CONTENT_CHARSET;
        }
        Reader reader = new InputStreamReader(instream, charset);
        StringBuilder buffer = new StringBuilder();
        try {

            char[] tmp = new char[1024];
            int l;
            while ((l = reader.read(tmp)) != -1) {
                buffer.append(tmp, 0, l);
            }

        } finally {
            reader.close();
        }
        return buffer.toString();
    }

    public static String getContentCharSet(final HttpEntity entity)
            throws ParseException {

        if (entity == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }

        String charset = null;

        if (entity.getContentType() != null) {

            HeaderElement values[] = entity.getContentType().getElements();

            if (values.length > 0) {

                NameValuePair param = values[0].getParameterByName("charset");

                if (param != null) {

                    charset = param.getValue();

                }

            }

        }

        return charset;

    }

    public SSLContext sslContext = null;

    public void initializeSSL(Context context) {
        try {
            Log.v(TAG, "Start Getting SSL Context ");
            int lastResponseCode;
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(context.getString(R.string.server_cert_asset_name));
            String serverCACerticate = IOUtil.readFully(inputStream);
            Log.v(TAG, "serverCACerticate " + serverCACerticate);
            InputStream clientCertFile = assetManager.open(context.getString(R.string.client_cert_file_name));
            String clientPassword = context.getString(R.string.client_cert_password);
            Log.v(TAG, "serverCACerticate " + clientPassword + " clientCertFile " + clientCertFile);
            sslContext = SSLContextFactory.getInstance().makeContext(clientCertFile, clientPassword, serverCACerticate);

            CookieHandler.setDefault(new CookieManager());
            Log.v(TAG, "Getting SSL Context " + sslContext);
        } catch (Exception e) {
            Log.v(TAG, "ERROR in Getting SSL Context " + e.getMessage());
            e.printStackTrace();
        }

    }


    public static ArrayList fetchDataArrayServer(String url) {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        HttpResponse response;

        String sArr = "";
        ArrayList s = new ArrayList();

        try {


            Log.v(TAG, "Executing " + url);
            response = httpclient.execute(httppost);
            Log.v(TAG, "UPLOAD: executed");

            s = HttpView.getResponseBodyObject(response);
            Log.v(TAG, "Got Response: " + s);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    public static ArrayList getResponseBodyObject(HttpResponse response) {
        ArrayList response_text = null;
        HttpEntity entity = null;
        try {
            entity = response.getEntity();
            response_text = _getResponseBodyObject(entity);

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        System.out.println("response_text size " + response_text.size());
        return response_text;
    }

    public static ArrayList _getResponseBodyObject(final HttpEntity entity)
            throws IOException, ParseException {
        ArrayList arr = new ArrayList();
        if (entity == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        InputStream instream = entity.getContent();
        if (instream == null) {
            return null;
        }

        if (entity.getContentLength() > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(

                    "HTTP entity too large to be buffered in memory");
        }
        String charset = getContentCharSet(entity);
        if (charset == null) {
            charset = HTTP.DEFAULT_CONTENT_CHARSET;
        }
        ObjectInputStream reader = new ObjectInputStream(instream);
        StringBuilder buffer = new StringBuilder();
        try {
            char[] tmp = new char[1024];
            Object o;
            while ((o = reader.readObject()) != null) {
                if (o instanceof ArrayList) {
                    arr = (ArrayList) o;
                }
                System.out.println("no of elements: " + arr.size());
            }
//			 for (int a = 0; a < elements.length; a++) {
//		            System.out.println(elements[a]);
//		        }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            reader.close();
        }
        return arr;
    }

    public static Object connect2ServerObject(String url) {
        Log.v(TAG, "Reading Object");
        Log.v(TAG, url);
        Object o = null;
        URL u;
        try {
            u = new URL(url);
            URLConnection uc = u.openConnection();

            ObjectInputStream ois = new ObjectInputStream(uc.getInputStream());
            o = ois.readObject();
            System.out.println(o);
            u = null;

        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return o;
    }
}
