package com.plants.detection.util;

import java.io.Serializable;

public class PlantModel implements Serializable {
	public static final long serialVersionUID = 12121L; 
	public String idplantdisease, palntName, plantDisease, symptoms,
			preventive, pesticide, pesticidecost;

	public String getIdplantdisease() {
		return idplantdisease;
	}

	public void setIdplantdisease(String idplantdisease) {
		this.idplantdisease = idplantdisease;
	}

	public String getPalntName() {
		return palntName;
	}

	public void setPalntName(String palntName) {
		this.palntName = palntName;
	}

	public String getPlantDisease() {
		return plantDisease;
	}

	public void setPlantDisease(String plantDisease) {
		this.plantDisease = plantDisease;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getPreventive() {
		return preventive;
	}

	public void setPreventive(String preventive) {
		this.preventive = preventive;
	}

	public String getPesticide() {
		return pesticide;
	}

	public void setPesticide(String pesticide) {
		this.pesticide = pesticide;
	}

	public String getPesticidecost() {
		return pesticidecost;
	}

	public void setPesticidecost(String pesticidecost) {
		this.pesticidecost = pesticidecost;
	}

}
