//package com.plants.detection.activity;
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.media.Ringtone;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Environment;
//import android.os.PowerManager;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ProgressBar;
//import android.widget.Toast;
//
//import org.bytedeco.javacpp.opencv_core.Mat;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Timer;
//import java.util.TimerTask;
//
//import backup.OMRTestPhone;
//import util.opencv.AndroidConstants;
//import util.opencv.Polygon;
//
//import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
//import static org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;
//
//
///**
// * Created by djalmaafilho.
// */
//public class CameraCaptureActivity extends CommonActivity implements CvCameraPreview.CvCameraViewListener {
//    public static final String TAG = "CameraCaptureActivity";
//
//    private CascadeClassifier faceDetector;
//    private String[] nomes = {"", "Y Know You"};
//    private int absoluteFaceSize = 0;
//    private CvCameraPreview cameraView;
//    boolean takePhoto;
//
//    boolean trained;
//
//    private boolean hasPermissions(Context context, String... permissions) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
//            for (String permission : permissions) {
//                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.camera_capture);
//        if (Build.VERSION.SDK_INT >= 23) {
//            String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
//            if (!hasPermissions(this, PERMISSIONS)) {
//                ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
//            }
//        }
//
//        android.os.StrictMode.ThreadPolicy tp = android.os.StrictMode.ThreadPolicy.LAX;
//        android.os.StrictMode.setThreadPolicy(tp);
//
//        cameraView = (CvCameraPreview) findViewById(R.id.camera_view);
//        cameraView.setCvCameraViewListener(this);
//        takePhoto = true;
//        Button btTrain = (Button) findViewById(R.id.btTrain);
//        btTrain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                success = true;
//            }
//        });
////        Button btReset= (Button) findViewById(R.id.btReset);
////        btTrain.setVisibility(Button.INVISIBLE);
////        btReset.setVisibility(Button.INVISIBLE);
//       /* whole = (LinearLayout) findViewById(R.id.topLinearLayout);
//        middleLayout = (LinearLayout) findViewById(R.id.middleLayout);
//        topLayout = (LinearLayout) findViewById(R.id.topLayoutLL);
//        progressBar = (ProgressBar) findViewById(R.id.progressBar);
//        infoBar = (TextView) findViewById(R.id.infoBar);
//        topLeft = (LinearLayout) findViewById(R.id.topLeftLL);
//        topRight = (LinearLayout) findViewById(R.id.topRightLL);
//        bottomLeft = (LinearLayout) findViewById(R.id.bottomLeftLL);
//        bottomRight = (LinearLayout) findViewById(R.id.bottomRightLL);
//        bottomLayoutThreeLL = (LinearLayout) findViewById(R.id.bottomLayoutThreeLL);
//        progressBar.setVisibility(ProgressBar.INVISIBLE);*/
//        /*deviceInformationModel = new DeviceInformationModel(CameraCaptureActivity.this);
//        mWakeLock = ((PowerManager) this.getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, getClass().getName());
//        mWakeLock.acquire();*/
//        ca = new CheckOMRAsync();
//
//
////        final int formatToBeUsed= opencv_videoio.VideoWriter.fourcc((byte)'X', (byte)'2', (byte)'6', (byte)'4');
////        opencv_videoio.VideoWriter vw = new opencv_videoio.VideoWriter();
////
////        // VideoWriter.fourcc((byte)'X', (byte)'2', (byte)'6', (byte)'4')  VideoWriter.fourcc((byte) 'M', (byte) 'J', (byte) 'P', (byte) 'G')
////        //const string& filename, int fourcc, double fps, Size frameSize, bool isColor=true
////        vw.open("a.mp4",formatToBeUsed, 15,
////                new opencv_core.Size(320, 240), true);
////
////        //(int) cap.get(CV_CAP_PROP_FRAME_WIDTH), (int) cap.get(CV_CAP_PROP_FRAME_HEIGHT)
////        if (!vw.isOpened()) {
////            System.err.println("Could not open video");
////            Log.i(LOG_TAG, "Could not open video ================>>>>"  );
////        }
////        uiTimer.scheduleAtFixedRate(uiUpdater,0,50);
//    }
//
//    PowerManager.WakeLock mWakeLock = null;
//  /*  LinearLayout whole = null;
//    LinearLayout middleLayout = null;
//    LinearLayout topLayout = null;
//
//    LinearLayout topLeft = null;
//    LinearLayout topRight = null;
//    LinearLayout bottomLeft = null;
//    LinearLayout bottomRight = null, bottomLayoutThreeLL = null;
//    ProgressBar progressBar = null;
//    TextView infoBar = null;*/
//    HashMap hm = new HashMap();
//    ArrayList<Polygon> fourCorners = new ArrayList<>();
//    ArrayList<Polygon> fourCornersBackup = new ArrayList<>();
//    int[][] cornerPoints = new int[4][4];
//
//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//
//        double bitmapWidth = cameraView.frameWidth;
//        double bitmapHeight = cameraView.frameHeight;
//        Log.i(LOG_TAG, "bitmapWidth bitmapHeight  " + bitmapWidth + " " + bitmapHeight);
//      /*  double perX = bitmapWidth * 1.0 / whole.getWidth();
//        double perY = bitmapHeight * 1.0 / whole.getHeight();
//
//        hm.put("ScreenWidth", whole.getWidth());
//        hm.put("ScreenHeight", whole.getHeight());
//
//
//        hm.put("middleLayoutX", topLeft.getX());
//        hm.put("middleLayoutY", topLayout.getHeight());
//        hm.put("middleLayoutWidth", topRight.getX() + topRight.getWidth() - topLeft.getX());
//        hm.put("middleLayoutHeight", middleLayout.getHeight());*/
//      /*  int middleLayoutX = n2i(hm.get("middleLayoutX"));
//        int middleLayoutY = n2i(hm.get("middleLayoutY"));
//        int middleLayoutWidth = n2i(hm.get("middleLayoutWidth"));
//        int middleLayoutHeight = n2i(hm.get("middleLayoutHeight"));
//        cx = (int) (middleLayoutX * perX);
//        cy = (int) (middleLayoutY * perY);
//        cwidth = (int) (middleLayoutWidth * perX);
//        cheight = (int) (middleLayoutHeight * perY);
//
//
//        LinearLayout[] a = new LinearLayout[]{topLeft, topRight, bottomLeft, bottomRight};
//        for (int i = 0; i < a.length; i++) {
//            LinearLayout corner = a[i];
//            int x = (int) (corner.getX() * perX), width = (int) (corner.getWidth() * perY), height = (int) (corner.getHeight() * perY);
//            int y = 0;
//            if (i > 1) {
//                y = (int) ((bottomLayoutThreeLL.getY() + corner.getY()) * perY);
//            } else {
//                y = (int) ((corner.getY()) * perY);
//            }*/
//           /* Polygon left = new Polygon.Builder().
//                    addVertex(new Point(x - cx, y)).
//                    addVertex(new Point(x + width - cx, y)).
//                    addVertex(new Point(x + width - cx, y + height)).
//                    addVertex(new Point(x - cx, y + height)).build();
//            cornerPoints[i] = new int[]{x - cx, y, width, height};
//            Log.i(LOG_TAG, "Corner Points  " + x + " " + y + " " + width + " " + height);
//            fourCorners.add(left);
//
//        }*/
//        fourCornersBackup = (ArrayList<Polygon>) fourCorners.clone();
////        int[] location = new int[2];
////        middleLayout.getLocationInWindow(location);
//
////        toast(0 + " " + topLayout.getHeight() + " " + middleLayout.getWidth() + " " + middleLayout.getHeight());
//    }
//
//    @Override
//    public void onCameraViewStarted(int width, int height) {
//        absoluteFaceSize = (int) (width * 0.32f);
//    }
//
//    @Override
//    public void onCameraViewStopped() {
//
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (mWakeLock != null && mWakeLock.isHeld())
//            mWakeLock.acquire();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (mWakeLock != null && mWakeLock.isHeld())
//            mWakeLock.acquire();
//
//        if (cameraView != null) {
//            cameraView.stopCameraPreview();
//            finish();
//        }
//    }
//
//    private void capturePhoto(Mat rgbaMat) {
////        try {
////            TrainHelper.takePhoto(getBaseContext(), 1, TrainHelper.qtdPhotos(getBaseContext()) + 1, rgbaMat.clone(), faceDetector);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//        takePhoto = false;
//    }
//
////    private void recognize(opencv_core.Rect dadosFace, Mat grayMat, Mat rgbaMat) {
////        Mat detectedFace = new Mat(grayMat, dadosFace);
////        resize(detectedFace, detectedFace, new Size(TrainHelper.IMG_SIZE, TrainHelper.IMG_SIZE));
////
////        IntPointer label = new IntPointer(1);
////        DoublePointer reliability = new DoublePointer(1);
////        faceRecognizer.predict(detectedFace, label, reliability);
////        int prediction = label.get(0);
////        double acceptanceLevel = reliability.get(0);
////        String name;
////        if (prediction == -1 || acceptanceLevel >= ACCEPT_LEVEL) {
////            name = getString(R.string.unknown);
////        } else {
////            name = nomes[prediction] + " - " + acceptanceLevel;
////        }
////        int x = Math.max(dadosFace.tl().x() - 10, 0);
////        int y = Math.max(dadosFace.tl().y() - 10, 0);
////        putText(rgbaMat, name, new Point(x, y), FONT_HERSHEY_PLAIN, 1.4, new opencv_core.Scalar(0, 255, 0, 0));
////    }
//
//    //    void showDetectedFace(RectVector faces, Mat rgbaMat) {
////        int x = faces.get(0).x();
////        int y = faces.get(0).y();
////        int w = faces.get(0).width();
////        int h = faces.get(0).height();
////
////        rectangle(rgbaMat, new Point(x, y), new Point(x + w, y + h), opencv_core.Scalar.GREEN, 2, LINE_8, 0);
////    }
////
////    void noTrainedLabel(opencv_core.Rect face, Mat rgbaMat) {
////        int x = Math.max(face.tl().x() - 10, 0);
////        int y = Math.max(face.tl().y() - 10, 0);
////        putText(rgbaMat, "No trained or train unavailable", new Point(x, y), FONT_HERSHEY_PLAIN, 1.4, new opencv_core.Scalar(0, 255, 0, 0));
////    }
//    long time = System.currentTimeMillis();
//
//    public static int n2i(Object d) {
//        int i = 0;
//        if (d != null) {
//            String dual = d.toString().trim();
//            try {
//                i = new Double(dual).intValue();
//            } catch (Exception e) {
//                System.out.println("Unable to find integer value");
//            }
//        }
//        return i;
//    }
//
//    private final String LOG_TAG = AndroidConstants.LOG_TAG;
//    public int cx = 0, cy = 0, cwidth = 0, cheight = 0;
//
//    @Override
//    public Mat onCameraFrame(Mat rgbaMat) {
////        Mat greyMat = new Mat(rgbaMat.rows(), rgbaMat.cols());
////        cvtColor(rgbaMat, greyMat, CV_BGR2GRAY);
//        try {
//
//
////            System.out.println("SIZE OF DATA:" + rgbaMat.rows() + " " + rgbaMat.cols());
////        opencv_core.Rect rectcrop=new opencv_core.Rect(1,1,100,100);
////        Mat newimg=new Mat(rgbaMat,rectcrop);
////            int ScreenWidth = n2i(hm.get("ScreenWidth"));
////
////            int ScreenHeight = n2i(hm.get("ScreenHeight"));
////
////
////            int bitmapWidth = rgbaMat.cols();
////            int bitmapHeight = rgbaMat.rows();
////            System.out.println("OMR TEST aaa  :" + bitmapWidth + " " + bitmapHeight);
////            double perX = bitmapWidth * 1.0 / ScreenWidth;
////            double perY = bitmapHeight * 1.0 / ScreenHeight;
//
//
////            System.out.println("OMR TEST aaa Hashmap hm :" + hm);
////            System.out.println("OMR TEST aaa Hashmap perx,pery:" + perX + "," + perY);
//
//            if (success) {// first time delay
//                String fileName = "tempOMR_Orginal_" + System.currentTimeMillis() + ".png";
//                Log.i(LOG_TAG, "   " + cx + " " + cy + " " + cwidth + " " + cheight);
//                Mat m = rgbaMat.clone();
//                boolean taskNotRunning = ca.getStatus() != AsyncTask.Status.RUNNING;
//                Log.i(LOG_TAG, " Task Running  Status" + taskNotRunning + " " + time + " ");
//                if (taskNotRunning) {
//                    ca = new CheckOMRAsync();
//                    ca.execute(m, fileName);
//                }
//                Log.i(LOG_TAG, " name " + fileName + "----------------------------------------------------------------------");
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return rgbaMat;
//    }
//
//    CheckOMRAsync ca = null;
////    DeviceInformationModel deviceInformationModel = null;
//    UIUpdater uiUpdater = null;
//
//  /*  void showInfo(final String msg) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                infoBar.setText(msg);
//            }
//        });
//    }*/
//
//    OMRTestPhone omr = null;
//    Timer uiTimer = null;
//
//    class UIUpdater extends TimerTask {
//
//        @Override
//        public void run() {
//            if (omr != null) {
//                if (omr.currentIndexFound != -1) {
////                    Log.i(LOG_TAG, "Current Index in timer " + omr.currentIndexFound);
//                    setCornerGreen(omr.currentIndexFound);
//                }
//
//            } else {
//                setCornerGreen(4);
//            }
//        }
//    }
//
//
//    public void setCornerGreen(final int index) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                /*if (index == 0) {
//                    topLeft.setBackgroundResource(R.drawable.my_bullet_background_green);
//                } else if (index == 1) {
//                    topRight.setBackgroundResource(R.drawable.my_bullet_background_green);
//                } else if (index == 2) {
//                    bottomLeft.setBackgroundResource(R.drawable.my_bullet_background_green);
//                } else if (index == 3) {
//                    bottomRight.setBackgroundResource(R.drawable.my_bullet_background_green);
//                } else if (index == 4) {    // make all red
//                    topLeft.setBackgroundResource(R.drawable.my_bullet_background);
//                    topRight.setBackgroundResource(R.drawable.my_bullet_background);
//                    bottomLeft.setBackgroundResource(R.drawable.my_bullet_background);
//                    bottomRight.setBackgroundResource(R.drawable.my_bullet_background);
//
//                } else if (index == 5) {// make all green
//                    topLeft.setBackgroundResource(R.drawable.my_bullet_background_green);
//                    topRight.setBackgroundResource(R.drawable.my_bullet_background_green);
//                    bottomLeft.setBackgroundResource(R.drawable.my_bullet_background_green);
//                    bottomRight.setBackgroundResource(R.drawable.my_bullet_background_green);
////                    playSound();
//                }*/
//
//            }
//        });
//
//    }
//
//    public void playSound() {
//        try {
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
//            r.play();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private class CheckOMRAsync extends AsyncTask<Object, String, String> {
//        boolean b = false;
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            success = false;
//            showProgress(ProgressBar.VISIBLE);
//            setCornerGreen(4);
//            uiUpdater = new UIUpdater();
//            uiTimer = new Timer();
//            uiTimer.scheduleAtFixedRate(uiUpdater, 0, 50);
//        }
//        File f =null;
//        @Override
//        protected String doInBackground(Object... mats) {
//
//            Mat mat = (Mat) mats[0];
//
//            String fileName = (String) mats[1];
//            String name = fileName;
//            File dir = new File(Environment.getExternalStorageDirectory().toString() + "/Database");
//            if (!dir.exists()) {
//                dir.mkdirs();
//            }
//          f = new File(dir.getAbsolutePath(), name);
//            try {
//                f.createNewFile();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            imwrite(f.getAbsolutePath(), mat);
//            mat.release();
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            showProgress(ProgressBar.INVISIBLE);
//            System.out.println(" f.getAbsolutePath() "+ f.getAbsolutePath());
//            int countGreenPer=OMRTestPhone.countGreen( f.getAbsolutePath());
//
//
//            if(countGreenPer>20){
//                Intent intent = new Intent(CameraCaptureActivity.this, PlantAnalysisActivity.class);
//                intent.putExtra("filename", f.getAbsolutePath());
//                System.out.println("f.getAbsolutePath() "+f.getAbsolutePath());
////            sendData(f, AndroidConstants.AJAX_URL()+"methodId=uploadImage");
//                toast( " Found Plant Image "+f.getName()+" "+countGreenPer);
//                startActivity(intent);
//            }else{
//                toast("Invalid Plant Image. Please show plant image. "+f.getName()+" GreenPer "+countGreenPer);
//            }
//
//
//
//        }
//    }
//
//
//
//    public void cancelTimer() {
//        try {
//            if (uiUpdater != null)
//                uiUpdater.cancel();
//            if (uiTimer != null)
//                uiTimer.cancel();
//            uiUpdater = null;
//            uiTimer = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    boolean success = false;
//
//
//
//
//    void showProgress(final int showOrHide) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//
//
//            }
//        });
//    }
//
//    void alertMessage(final String msg) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
////                int remainigPhotos = TrainHelper.PHOTOS_TRAIN_QTY - TrainHelper.qtdPhotos(getBaseContext());
//                Toast.makeText(getBaseContext(), "OMR Sheet Detected " + msg, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//}
