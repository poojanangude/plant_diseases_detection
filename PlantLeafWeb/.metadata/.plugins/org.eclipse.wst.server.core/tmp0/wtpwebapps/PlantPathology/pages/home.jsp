<!DOCTYPE html>
<html lang="en">

<head>
<jsp:include page="../tiles/include_files.jsp"></jsp:include>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	<!-- Preloader -->
	<div id="preloader">
		<div id="load"></div>
	</div>

<%@include file="../tiles/topmenu.jsp" %>

	<!-- Section: intro -->
	<section id="intro" class="intro">

		<div class="slogan" >
			<h2>
				WELCOME Home</span>
			</h2>
			<h4>Logged in</h4>
			 

		</div>
		<div class="page-scroll">
			<a href="#service" class="btn btn-circle"> <i
				class="fa fa-angle-double-down animated"></i>
			</a>
		</div>
	</section>
	<!-- /Section: intro -->

	
	<!-- /Section: contact -->

	<jsp:include page="../tiles/footer.jsp"></jsp:include>

<script>

function fnSubmit(){
	

	 var str = $( "#contact-form" ).serialize();
	$.post("<%=request.getContextPath()%>/tiles/ajax.jsp?methodId=checkLogin",
			str,
			function(data) {
		data=$.trim(data);
		if(data=='false'){
			alert(data);
			alert('Invalid Credentials. Please try again.');
			$('#contact-form')[0].reset();
		}else{
			window.location.href='<%=request.getContextPath()%>/pages/home.jsp';
		}
  

			});


}
</script>
</body>

</html>
