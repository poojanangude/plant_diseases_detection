<!DOCTYPE html>
<%@page import="java.util.HashMap"%>
<%@page import="com.helper.StringHelper"%>

<html lang="en">

<head>
<jsp:include page="../tiles/include_files.jsp"></jsp:include>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	<!-- Preloader -->
	<div id="preloader">
		<div id="load"></div>   
	</div>
<%@include file="../tiles/topmenu.jsp" %>

	<!-- Section: intro -->
	<section id="intro" class="intro">

		<div class="slogan" style="width: 100%; ">
			<h2>
				WELCOME <%=name %> TO <span class="text_color">Plant Pathology Detection</span>   
			</h2>
<!-- 			<h4>Web Clustering & Search</h4> -->
			<%if(!isLogin){ %>
			<form name="contact-form" id="contact-form" action="javascript:fnSubmit();" enctype="mulipart/form-data" method="post">
				<div class="col-md-3" style="margin: 0 auto;float: none;">  
					<div class="form-group">
						<label for="name" style="color: white;">UserName</label> <input type="text"   
							class="form-control" id="uname" name="uname" placeholder="Enter User Id" value="1234"
							required="required" />
					</div>
			  		<div class="form-group">
						<label for="name"  style="color: white;"> Password</label>
<!-- 						<div class="input-group"> -->
							<input type="password" class="form-control" id="pass" name="pass" value="1234"
								placeholder="Enter password" required="required" />
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-skin pull-right"
							id="btnContactUs">Login</button>
							<button type="button" onclick="javascript:window.location.href='<%=request.getContextPath()%>/pages/user_registration.jsp';" class="btn btn-skin pull-right"
							id="btnContactUs">Register</button>
					</div>
					<BR><BR>
				</div>
			</form>

		<%} %>
		</div>
	
	</section>
	
	<!-- /Section: intro -->



<script>

function fnSubmit(){
	

	 var str = $( "#contact-form" ).serialize();
// 	 alert(str);
	$.post("<%=request.getContextPath()%>/tiles/ajax.jsp?methodId=checkLogin",
			str,
			function(data) {
		data=$.trim(data);
		if(data=='false'){
			alert('Invalid Credentials. Please try again.');
			$('#contact-form')[0].reset();
		}else{
			window.location.href='<%=request.getContextPath()%>/pages/home.jsp';
		}
  

			});


}
</script>
</body>

</html>
