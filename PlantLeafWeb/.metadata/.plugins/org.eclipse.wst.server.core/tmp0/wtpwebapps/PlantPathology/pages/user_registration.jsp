<!DOCTYPE html>

<%@page import="com.helper.StringHelper"%>
<html lang="en">

<head>
<jsp:include page="../tiles/include_files.jsp"></jsp:include>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">



	<jsp:include page="../tiles/topmenu.jsp"></jsp:include>
	<!-- Section: contact -->
	<section id="contact" class="home-section text-center">
		<div class="heading-contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="wow bounceInDown" data-wow-delay="0.4s">
							<div class="section-heading">
								<h2>Registration</h2>
								<i class="fa fa-2x fa-angle-down"></i>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">

			<div class="row">
				<div class="col-lg-2 col-lg-offset-5">
					<hr class="marginbot-50">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="boxed-grey">
						<form id="contact-form" action="javascript:fnSubmit();">
							<div class="row">
								<div class="col-md-6">
								
									<div class="form-group">
										<label for="name">First Name</label> 
										<input type="text" class="form-control" style="text-transform: capitalize;" id="name" name="fname" tabindex="1"
										title="Please enter only characters" pattern="[A-Za-z]+" 
										  placeholder="Enter First name" required="required" />
									</div>   

<div class="form-group">
										<label for="name">Phone No</label> <input type="text" 	title="Please enter minimum 10 digits" pattern="[0-9]{10}"
											class="form-control" name="phoneno" tabindex="4"  id="phone" placeholder="Enter Phone No"
											required="required" />
									</div>				

								<div class="form-group">
										<label for="name">Password</label> <input type="password"
											class="form-control" id="pass" tabindex="8"  title="Please enter minimum 6 to 20 characters" pattern="{6,20}" 
											placeholder="Enter Password" name="pass" required="required" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="name">Last Name</label> <input type="text" 	title="Please enter only characters" pattern="[A-Za-z]+"  
											class="form-control" name="lname"  style="text-transform: capitalize;" id="name" tabindex="2"  placeholder="Enter Last name"
											required="required" />
									</div>
									
										<div class="form-group">
										<label for="name">User Name </label> <input type="text" 	title="Please enter minimum 6 to 20 characters" pattern="{6,20}" 
											class="form-control" id="uname" name="uname" tabindex="7"  placeholder="User Name"
											required="required" />
									</div>
									
									<div class="form-group">
										<label for="name">Confirm Password</label> <input
											type="password" class="form-control" name="cuserpass"  tabindex="9"  id="cuserpass" 	title="Please enter minimum 6 to 20 characters" pattern="{6,20}"
											placeholder="Enter Confirm Password" required="required" />
									</div>
								</div>
								<div class="col-md-12">
								&nbsp;&nbsp;
										<button type="reset" id="ResetBtnId"
										class="btn btn-skin pull-right" >Reset</button>&nbsp;&nbsp;
									<button type="submit" id="RegisterMeBtn"
										class="btn btn-skin pull-right"   tabindex="11"  >Register
										Me2!</button>
								</div>
							</div>
						</form>
					</div>
				</div>


			</div>

		</div>
	</section>
	<!-- /Section: contact -->


</body>
<script>
function fnSubmit(){
	
	if($('#pass').val()!=$('#cuserpass').val()){
		alert('Password and confirm password do not match!');
		return;
	}
	
	 var str = $( "#contact-form" ).serialize();
	$.post("<%=request.getContextPath()%>/tiles/ajax.jsp?methodId=registerNewUser",
			str,
			function(data) {
data=$.trim(data);  
if(data.indexOf('Successfully')!=-1){
	alert(data);
	$('#contact-form')[0].reset();
	window.location.href='<%=request.getContextPath()%>/pages/index.jsp';
}else{
	alert(data);
}



			});


}
</script>
</html>
