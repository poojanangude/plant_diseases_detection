<%@page import="com.diseasedetection.PythonConnector"%>
<%@page import="com.helper.UserModel"%>
<%@page import="com.helper.ConnectionManager"%>

<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.io.InputStream"%>

<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="com.helper.HttpHelper"%>
<%@page import="com.constant.ServerConstants"%>
<%@page import="java.io.File"%>
<%@page import="java.io.BufferedOutputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="org.jfree.chart.ChartUtilities"%>
<%@page import="org.jfree.data.category.DefaultCategoryDataset"%>
<%@page import="java.io.ObjectOutputStream"%>
<%@page import="com.helper.StringHelper"%>
<%@page import="java.util.HashMap"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%
	String sMethod = StringHelper.n2s(request.getParameter("methodId"));
	String returnString = "";
	boolean bypasswrite = false;
	// 	UserModel um=null;

	HashMap parameters = StringHelper.displayRequest(request);
	System.out.println("****parameters " + parameters);
	if (sMethod.equalsIgnoreCase("uploadImage")) {

		HashMap uploadMap = HttpHelper.parseMultipartRequest(request);
		System.out.println("****" + uploadMap);
		uploadMap.putAll(parameters);
		FileItem fi1 = (FileItem) uploadMap.get("uploaded_file1ITEM");
		FileItem fi2 = (FileItem) uploadMap.get("uploaded_file2ITEM");
		System.out.println("in uplooad file");
		System.out.println(fi1.getName());
		System.out.println(fi2.getName());
		System.out.println("data" + uploadMap);
		try {
			System.out.println("file path");
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		System.out.println("in uplooad file");
		returnString = ConnectionManager.uploadImg(fi1, fi2);
		if (returnString.length() > 0) {
			System.out.print("successfully uploaded");
			request.getRequestDispatcher(
					"../pages/uploaddata.jsp?success=1").forward(
					request, response);
		} else {
			System.out.print("File not uploaded");
			request.getRequestDispatcher(
					"../pages/uploaddata.jsp?success=0").forward(
					request, response);
		}
	}
	if (sMethod.equalsIgnoreCase("uploadImage2")) {
		HashMap uploadMap = HttpHelper.parseMultipartRequest(request);
		System.out.println("****" + uploadMap);
		uploadMap.putAll(parameters);
		FileItem fi = (FileItem) uploadMap.get("uploaded_fileITEM");

		// 	InputStream is=fi.getInputStream();
		InputStream is = (InputStream) uploadMap
				.get("uploaded_fileFILE");
		File f = new File(ServerConstants.WORKSPACE_PATH
				+ "\\testingFile.jpg");

		FileOutputStream outputStream = new FileOutputStream(f);

		int read = 0;
		byte[] bytes = new byte[1024];
		while ((read = is.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		outputStream.close();
		System.out.print("successfully uploaded");
		request.getRequestDispatcher(
				"../pages/checkDisease.jsp?success=1").forward(request,
				response);
	} else if (sMethod.equalsIgnoreCase("getImage")) {
		String filename = StringHelper.n2s(request
				.getParameter("filename"));
		try {
			UserModel um = (UserModel) session
					.getAttribute("USER_MODEL");
			String uid = um.getUid();
			// 	         String fileName = request.getParameter("image");             
			FileInputStream fis = new FileInputStream(new File(
					ServerConstants.WORKSPACE_PATH + "/" + filename));
			BufferedInputStream bis = new BufferedInputStream(fis);
			response.setContentType("image/png");
			BufferedOutputStream output = new BufferedOutputStream(
					response.getOutputStream());
			for (int data; (data = bis.read()) > -1;) {
				output.write(data);
			}
			output.flush();
			output.close();
			bis.close();
			fis.close();
			return;
		} catch (IOException e) {

		} finally {
			// 	    	  output.
			// close the streams    
		}
	} else if (sMethod.equalsIgnoreCase("getDecision")) {
		OutputStream responseBody = response.getOutputStream();

		File f = new File(ServerConstants.WORKSPACE_PATH
				+ "\\testingFile.jpg");
		int per = ConnectionManager.countGreen(f.getAbsolutePath());
		   
		if (per >=7) { //	Green Colored Leaf   
			returnString = ServerConstants.lastOutput;
			String decision = returnString.split("#")[0];
			String outputstr = ConnectionManager
					.getPesticideInfo(decision);
			System.out.println("Dicision Is:" + returnString);
			returnString = returnString + "=>" + outputstr;
		} else {
			returnString = "Not a leaf" + "=>" + "NA";

		}
		ObjectOutputStream os = new ObjectOutputStream(responseBody);
		os.writeObject(returnString);
		os.flush();
		os.close();

		return;
	}

	else if (sMethod.equalsIgnoreCase("compare")) {
		returnString = ConnectionManager.compare();
	} else if (sMethod.equalsIgnoreCase("kitData")) {
		returnString = ConnectionManager.kitdata(parameters);
	} else if (sMethod.equalsIgnoreCase("registerNewUser")) {
		returnString = ConnectionManager.insertUser(parameters);
	} else if (sMethod.equalsIgnoreCase("checkLogin")) {
		UserModel um2 = ConnectionManager.checkLogin(parameters);
		if (um2 != null) {
			session.setAttribute("USER_MODEL", um2);
			returnString = "true";
		} else {
			returnString = "false";
		}
	} else if (sMethod.equalsIgnoreCase("logout")) {

		UserModel um2 = (UserModel) session.getAttribute("USER_MODEL");
		session.removeAttribute("USER_MODEL");
		bypasswrite = true;
%>
<script>
			window.location.href="<%=request.getContextPath()%>/pages/index.jsp";
</script>
<%
	}

	// 	outputStream.write();

	if (!bypasswrite) {
		out.println(returnString);

	}
%>
