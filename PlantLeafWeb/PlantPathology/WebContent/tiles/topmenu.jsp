
<%@page import="com.helper.UserModel"%>
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-main-collapse">
				<i class="fa fa-bars"></i>
			</button>
			<a class="navbar-brand" href="index.html"> <!--                     <h2>T-Finder </h2> -->

			</a>
		</div>
		<%
			UserModel um = null;
			String role = "";
			boolean isLogin = false;
			String name = "";
			if (session.getAttribute("USER_MODEL") != null) {
				um = (UserModel) session.getAttribute("USER_MODEL");
				name = um.getUname();
				isLogin = true;
			}
		%>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div
			class="collapse navbar-collapse navbar-right navbar-main-collapse">
			<ul class="nav navbar-nav">
				<%
					if (isLogin) {
				%>
				<li class="active"><a title="Logged in as <%=role%>"> Welcome <%=name%></a></li>
				<li ><a
					href="<%=request.getContextPath()%>/pages/home.jsp">Home</a></li>
<!-- 				<li ><a -->
<%-- 					href="<%=request.getContextPath()%>/pages/uploaddata.jsp">Check --%>
<!-- 						Leaf Effect</a></li> -->
<!-- 				<li ><a -->
<%-- 					href="<%=request.getContextPath()%>/pages/compare.jsp">View Comparison</a></li> --%>
				<li ><a
					href="<%=request.getContextPath()%>/pages/checkDisease.jsp">Check Disease</a></li>
		
				<li><a
					href="<%=request.getContextPath()%>/tiles/ajax.jsp?methodId=logout">Logout</a></li>
				<%
					} else {
				%>
				<li class="active"><a
					href="<%=request.getContextPath()%>/pages/index.jsp">Login</a></li>
				<%
					}
				%>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>