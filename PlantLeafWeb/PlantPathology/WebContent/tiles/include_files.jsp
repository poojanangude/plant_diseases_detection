    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Plant Pathology</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/theme/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="<%=request.getContextPath()%>/theme/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<%=request.getContextPath()%>/theme/css/animate.css" rel="stylesheet" />
	
		<link href="<%=request.getContextPath()%>/theme/css/looks.css" rel="stylesheet" />
    <!-- Squad theme CSS -->
    <link href="<%=request.getContextPath()%>/theme/css/style.css" rel="stylesheet">
	<link href="<%=request.getContextPath()%>/theme/color/default.css" rel="stylesheet">
    <!-- Core JavaScript Files -->
    <script src="<%=request.getContextPath()%>/theme/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/theme/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/theme/js/jquery.easing.min.js"></script>	
	<script src="<%=request.getContextPath()%>/theme/js/jquery.scrollTo.js"></script>
	<script src="<%=request.getContextPath()%>/theme/js/jquery.highlight-4.js"></script>
	<script src="<%=request.getContextPath()%>/theme/js/wow.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<%=request.getContextPath()%>/theme/js/custom.js"></script>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display" %>   