<!DOCTYPE html>

<%@page import="com.diseasedetection.PythonConnector"%>
<%@page import="com.constant.ServerConstants"%>
<%@page import="com.helper.ConnectionManager"%>
<%@page import="com.helper.StringHelper"%>
<html lang="en">


<head>

<%@include file="../tiles/include_files.jsp"%>
<style>
.container-1 input#search:hover,.container-1 input#search:focus,.container-1 input#search:active
	{
	outline: none;
	background: #ffffff;
}

a,.tp-caption a.btn:hover {
	color: #00b0d1;
}

.highlight {
	background-color: #FFFF88;
}
</style>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

	<%@include file="../tiles/topmenu.jsp"%>


	<!-- Section: services -->

	<section id="service" class="home-section text-center bg-gray">
		<div class="heading-about">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="wow bounceInDown" data-wow-delay="0.4s">
							<div class="section-heading">
								<h2>Data Uploading</h2>
								<i class="fa fa-2x fa-angle-down"></i> Preferred Categories
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="slogan" style="width: 100%;">


			<form name="contact-form" id="contact-form"
				enctype="multipart/form-data"
				action="<%=request.getContextPath()%>/tiles/ajax.jsp?methodId=uploadImage2"
				method="post">
				<div class="col-md-3" style="margin: 0 auto; float: none;">


					<div class="form-group">
						<label for="name"> Please select a crop leaf Image .....</label>
						<!-- 						<div class="input-group"> -->
						<input type="file" id="uploaded_file" name="uploaded_file"
							required="required" />
						<!-- 						</div> -->
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-skin pull-right"
							id="btnContactUs">Upload</button>

					</div>

				</div>
			</form>
			<%
				String success = StringHelper.n2s(request.getParameter("success"));
				if (success.length() > 0) {
					String returnString = PythonConnector.getDecision();
					String decision = returnString.split("#")[0];
					String outputstr = ConnectionManager
							.getPesticideInfoWeb(decision);
					outputstr = outputstr.replaceAll("->", "<BR><BR>");
			%>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<img title="<%=returnString%>"
							src="<%=request.getContextPath()%>/tiles/ajax.jsp?methodId=getImage&filename=testingFile.jpg"
							width="200" height="200" /><BR> File Uploaded to
						<%=ServerConstants.getFilePath()%><BR> <span><BR><%=outputstr%></span>

					</div>
				</div>
			</div>
			<%
				}
			%>



		</div>

	</section>

	<!-- /Section: services -->

	<!-- /Section: contact -->


</body>

</html>
