package util;

public class FertilizerModel {
String fid, phlevel, fertilizerneed;

public String getFid() {
	return fid;
}

public void setFid(String fid) {
	this.fid = fid;
}

public String getPhlevel() {
	return phlevel;
}

public void setPhlevel(String phlevel) {
	this.phlevel = phlevel;
}

public String getFertilizerneed() {
	return fertilizerneed;
}

public void setFertilizerneed(String fertilizerneed) {
	this.fertilizerneed = fertilizerneed;
}
}
