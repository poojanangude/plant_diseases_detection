package com.helper;

import java.io.Serializable;

public class UserModel implements Serializable {
	static final long serialVersionUID = 1121L;
	String uid, username, phoneno, uname, pass, udate;
	String preferredCatagories="";
	String preferredCatagoriesDisplay="";
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUdate() {
		return udate;
	}
	public void setUdate(String udate) {
		this.udate = udate;
	}
	public String getPreferredCatagories() {
		return preferredCatagories;
	}
	public void setPreferredCatagories(String preferredCatagories) {
		this.preferredCatagories = preferredCatagories;
	}
	public String getPreferredCatagoriesDisplay() {
		return preferredCatagoriesDisplay;
	}
	public void setPreferredCatagoriesDisplay(String preferredCatagoriesDisplay) {
		this.preferredCatagoriesDisplay = preferredCatagoriesDisplay;
	}	

}
