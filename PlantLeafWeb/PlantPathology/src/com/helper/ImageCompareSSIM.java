package com.helper;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.SwingWorker;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_videoio.CvCapture;

public class ImageCompareSSIM {

	SwingWorker sw = null, sw2 = null;
	IplImage frame = null;
	ImageIO image;
	boolean breakLoop = false;
	public static CvCapture capture = null, capture1 = null;
	public static BufferedImage buffimg = null, buffimg2 = null;
	public static BufferedImage capturedImage = null;
	public static String fileDir = "D:/imgs/";

	/** Creates new form FirstModule */
	public ImageCompareSSIM() {
	}

	public void luminance() {
		try {
			breakLoop = false;
			buffimg = ImageIO.read(new File("D:\\frame\\test\\0025.png"));
			buffimg2 = ImageIO.read(new File("D:\\test.jpg"));
			getLuminance(buffimg, buffimg2);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public double[] getLuminance(BufferedImage br, BufferedImage br2) {
		int w1 = br.getWidth();
		int h1 = br.getHeight();
		int w2 = br2.getWidth();
		int h2 = br2.getHeight();

		double muX = 0, muY = 0;
		double tempX = 0, tempY = 0;
		double tempX2 = 0, tempY2 = 0;
		double tempppp = 0, tempXY = 0;
		try {
			for (int i = 0; i < w1; i++) {
				for (int j = 0; j < h1; j++) {

					int pixel = br.getRGB(i, j);
					Color c = Color.decode(pixel + "");
					int r = (int) (c.getRed());
					int g = (int) (c.getGreen());
					int b = (int) (c.getBlue());

					int pixel2 = br2.getRGB(i, j);
					Color c2 = Color.decode(pixel2 + "");
					int r2 = (int) (c2.getRed());
					int g2 = (int) (c2.getGreen());
					int b2 = (int) (c2.getBlue());

					tempX = tempX + (r + g + b);
					tempY = tempY + (r2 + g2 + b2);
				}
			}
			int count = w1 * h1 - 1;
			int count2 = w2 * h2 - 1;
			muX = tempX / count;
			muY = tempY / count2;
			float c1 = 0.01f;
			// Luminance
			double luminanace = (float) ((2 * muX * muY + c1) / (muX * muX
					+ muY * muY + c1));

			tempX2 = 0;
			tempY2 = 0;
			for (int i = 0; i < w1; i++) {
				for (int j = 0; j < h1; j++) {
					int pixel = br.getRGB(i, j);
					Color c = Color.decode(pixel + "");
					int r = (int) (c.getRed());
					int g = (int) (c.getGreen());
					int b = (int) (c.getBlue());

					int pixel2 = br2.getRGB(i, j);
					Color c2 = Color.decode(pixel2 + "");
					int r2 = (int) (c2.getRed());
					int g2 = (int) (c2.getGreen());
					int b2 = (int) (c2.getBlue());

					double rgb = (r + g + b);
					double rgb2 = (r2 + g2 + b2);
					double tempo1 = rgb - muX;
					double tempo2 = rgb2 - muY;

					tempX2 = tempX2 + (tempo1 * tempo1);
					tempY2 = tempY2 + (tempo2 * tempo2);
					// tempppp = tempppp + (tempo1 * tempo2);
				}
			}
			double notX1 = Math.sqrt(tempX2 / (w1 * h1 - 1));
			double notY1 = Math.sqrt(tempY2 / (w1 * h1 - 1));

			float c22 = 0.03f;
			// contast
			double contast = (float) ((notX1 * notY1 + c22) / (notX1 * notX1
					+ notY1 * notY1 + c22));

			// tempX2 = 0;
			// for (int i = 0; i < w1; i++) {
			// for (int j = 0; j < h1; j++) {
			// int pixel = br.getRGB(i, j);
			// Color c = Color.decode(pixel + "");
			// int r = (int) (c.getRed());
			// int g = (int) (c.getGreen());
			// int b = (int) (c.getBlue());
			//
			// int pixel2 = br2.getRGB(i, j);
			// Color c2 = Color.decode(pixel2 + "");
			// int r2 = (int) (c2.getRed());
			// int g2 = (int) (c2.getGreen());
			// int b2 = (int) (c2.getBlue());
			//
			// double rgb = (r + g + b);
			// double rgb2 = (r2 + g2 + b2);
			// double tempo1 = rgb - muX;
			// double tempo2 = rgb2 - muY;
			//
			// tempX2 = tempX2 + (tempo1 * tempo2);
			//
			// // tempppp = tempppp + (tempo1 * tempo2);
			// }
			// }
			float notXY = (float) (tempX2 / (w1 * h1 - 1));
			float c3 = 0.04f;
			double structure = (float) ((notXY + c3) / (notX1 * notY1 + c3));

			double ssim = (float) (((2 * muX * muY + c1) * (2 * notXY + c22)) / ((muX
					* muX + muY * muY + c1) * (notX1 * notX1 + notY1 * notY1 + c22)));
			// SSIM <0.92 mismatch >=0.92 Match

			System.out.println("Structure: " + structure);
			return new double[] { luminanace, contast, structure, ssim };
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public float getLuminanceOptimized(BufferedImage br) {
		int w1 = br.getWidth();
		int h1 = br.getHeight();

		int tempX = 0;
		for (int i = 0; i < w1; i++) {
			for (int j = 0; j < h1; j++) {

				int pixel = br.getRGB(i, j);
				Color c = Color.decode(pixel + "");
				int r = (int) (c.getRed());
				int g = (int) (c.getGreen());
				int b = (int) (c.getBlue());

				tempX = tempX + (r + g + b);

			}
		}
		float brightness = (float) tempX / (float) (w1 * h1 * 3);
		return brightness;
	}

	public double[] getLuminanceOptimized(BufferedImage br, BufferedImage br2) {
		int w1 = br.getWidth();
		int

		h1 = br.getHeight();
		int

		w2 = br2.getWidth();
		int

		h2 = br2.getHeight();

		double

		muX = 0, muY = 0;
		double

		tempX = 0, tempY = 0;
		double

		tempX2 = 0, tempY2 = 0;
		double

		tempppp = 0, tempXY = 0;
		try

		{
			ArrayList<Integer> image1 = new ArrayList<Integer>();
			ArrayList<Integer

			> image2 = new ArrayList<Integer>();
			for

			(int i = 0; i <

			w1; i++)

			{
				for (int j = 0; j <

				h1; j++)

				{

					int pixel = br.getRGB(i, j);
					Color c

					= Color.decode(pixel + "");
					int

					r = (int) (c.getRed());
					int

					g = (int) (c.getGreen());
					int

					b = (int) (c.getBlue());
					image1.add

					(r + g + b);

					int

					pixel2 = br2.getRGB(i, j);
					Color c2

					= Color.decode(pixel2 + "");
					int

					r2 = (int) (c2.getRed());
					int

					g2 = (int) (c2.getGreen());
					int

					b2 = (int) (c2.getBlue());

					image2.add

					(r2 + g2 + b2);
					tempX =

					tempX + (r + g + b);
					tempY =

					tempY + (r2 + g2 + b2);
				}

			}
			int count = w1 * h1 - 1;
			int

			count2 = w2 * h2 - 1;
			muX =

			tempX / count;
			muY =

			tempY / count2;
			float

			c1 = 0.01f;
			// Luminance
			double

			luminanace = (float) ((2 * muX * muY + c1) / (muX * muX + muY * muY + c1));

			tempX2 =

			0;
			tempY2 =

			0;
			for

			(int i = 0; i <

			image1.size(); i++)

			{

				double rgb = image1.get(i);
				double

				rgb2 = image2.get(i);
				double

				tempo1 = rgb - muX;
				double

				tempo2 = rgb2 - muY;

				tempX2 =

				tempX2 + (tempo1 * tempo1);
				tempY2 =

				tempY2 + (tempo2 * tempo2);
				// tempppp = tempppp + (tempo1 * tempo2);

			}

			double notX1 = Math.sqrt(tempX2 / (w1 * h1 - 1));
			double

			notY1 = Math.sqrt(tempY2 / (w1 * h1 - 1));

			float

			c22 = 0.03f;
			// contast
			double

			contast = (float) ((notX1 * notY1 + c22) / (notX1 * notX1 + notY1
					* notY1 + c22));

			float

			notXY = (float) (tempX2 / (w1 * h1 - 1));
			float

			c3 = 0.04f;
			double

			structure = (float) ((notXY + c3) / (notX1 * notY1 + c3));

			double

			ssim = (float) (((2 * muX * muY + c1) * (2 * notXY + c22)) / ((muX
					* muX + muY * muY + c1) * (notX1 * notX1 + notY1 * notY1 + c22)));
			// SSIM <0.92 mismatch >=0.92 Match

			// System.out.println("Structure: " + structure);
			return

			new double[] { luminanace, contast, structure, ssim };
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void main(String args[]) {
		try {
		BufferedImage image1;
		 BufferedImage image2;
			
				image1 = ImageIO.read(new File("D:\\work\\project\\PlantPathology\\image\\2019-03-12\\1552403908271.jpg"));
				image2 = ImageIO.read(new File("D:\\work\\project\\PlantPathology\\image\\2019-03-12\\1552402956098.jpg"));

				double f[] = new ImageCompareSSIM().getLuminance(image1, image2);
				String success = "SSIM  \n "+
	                     "Luminanace "+f[0] + "\n"+
	                     "Contrast "+f[1] + "\n"+
	                     "Structure "+f[2] + "\n"+
	                     "SSIM "+f[3] + "\n";
				
				if(f[3]<0.92){
	            	 success = success + "\n MISMatch ";
	             }else{
	            	 success = success + "\n Match ";
	             }
				System.out.println("res = "+success);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
}
