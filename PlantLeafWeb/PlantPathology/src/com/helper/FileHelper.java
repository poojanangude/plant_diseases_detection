package com.helper;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class FileHelper {
	public static void main(String[] args) throws IOException {
//		StringBuffer sb = getFileContent("D:/114");
//		System.out.println(sb);
		
		String dst = "D:/work/project/PlantDiseaseDetection/python-cnn/training_data/Tomato___Late_blight";
		String src = "E:/Downloads/dataset/lateblight";
		File f[] = getFileList(src);
		for (int i = 0; i < f.length; i++) {
			File file = f[i];
			System.out.println(file.getAbsolutePath());
			convertFormat(file, dst + "/"+ file.getName().replace(".png", ".jpg"));
		}

	}

	public static boolean convertFormat(File inputImage,
			String outputImagePath) throws IOException {
		BufferedImage bufferedImage;

		try {

			// read image file
			bufferedImage = ImageIO.read(inputImage);

			// create a blank, RGB, same width and height, and a white
			// background
			BufferedImage newBufferedImage = new BufferedImage(
					bufferedImage.getWidth(), bufferedImage.getHeight(),
					BufferedImage.TYPE_INT_RGB);
			newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0,
					Color.WHITE, null);

			// write to jpeg file
			ImageIO.write(newBufferedImage, "jpg", new File(outputImagePath));

			System.out.println("Done");

		} catch (IOException e) {

			e.printStackTrace();

		}
		return false;
	}

	public static StringBuffer getFileContent(String filepath) {
		InputStream is = null;
		int i;
		char c;
		StringBuffer sb = new StringBuffer();

		try {
			File f = new File(filepath);
			System.out.println(f.getCanonicalPath());
			if (!f.exists()) {
				System.out.println("File Does NOT exist!!");
				return sb;
			}
			is = new FileInputStream(filepath);
			byte[] b = new byte[1024];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while ((i = is.read(b)) != -1) {
				// String s = new String(b);
				// sb.append(s.trim());
				baos.write(b, 0, i);
			}
			sb = new StringBuffer(new String(baos.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return sb;
	}

	public static ArrayList<String[]> parseFile(String fileName) {
		ArrayList<String[]> arr = new ArrayList<String[]>();
		StringBuffer sb = getFileContent(fileName);
		String[] tokens = sb.toString().split("\\|1234\\|");
		for (int i = 0; i < tokens.length; i++) {
			String string = tokens[i];
			String[] keyTweet = string.split("\\|\\|");
			arr.add(keyTweet);
		}
		return arr;
	}

	public static ArrayList<String[]> parseFile(String fileName,
			String rowDelim, String colDelim) {
		ArrayList<String[]> arr = new ArrayList<String[]>();
		StringBuffer sb = getFileContent(fileName);
		String[] tokens = sb.toString().split(rowDelim);
		for (int i = 0; i < tokens.length; i++) {
			String string = tokens[i];
			String[] keyTweet = string.split(colDelim);
			arr.add(keyTweet);
		}
		return arr;
	}

	public static File[] getFileList(String dirPath) {
		File f = new File(dirPath);
		try {
			System.out.println("Canonical Path " + f.getCanonicalPath());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		File[] a = f.listFiles();
		if (a != null) {
			System.out.println(" Got Files " + a.length);
		}
		return a;
	}

	// extn=.txt .jpg
	public static File[] getFileList(String dirPath, final String extn) {
		File f = new File(dirPath);
		try {
			System.out.println("Canonical Path " + f.getCanonicalPath());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		FilenameFilter textFilter = new FilenameFilter() {

			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.endsWith(extn)) {
					return true;
				} else {
					return false;
				}
			}
		};

		File[] a = f.listFiles(textFilter);
		if (a != null) {
			System.out.println(" Got Files " + a.length);
		}
		return a;
	}

}
