/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_core.CV_8UC1;
import static org.bytedeco.javacpp.opencv_core.meanStdDev;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_CHAIN_APPROX_SIMPLE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_EXTERNAL;
import static org.bytedeco.javacpp.opencv_imgproc.Laplacian;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY_INV;
import static org.bytedeco.javacpp.opencv_imgproc.approxPolyDP;
import static org.bytedeco.javacpp.opencv_imgproc.arcLength;
import static org.bytedeco.javacpp.opencv_imgproc.boundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.getPerspectiveTransform;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import javax.imageio.ImageIO;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.bytedeco.javacpp.opencv_core.Mat;

import util.FertilizerModel;
import util.PlantModel;

import com.bean.Kitdata;
import com.constant.ServerConstants;

/**
 * 
 * @author Admin
 */
public class ConnectionManager /* extends DatabaseHelper */{
	public static Connection getDBConnection() {
		Connection conn = null;
		try {
			Class.forName(ServerConstants.db_driver);
			conn = DriverManager.getConnection(ServerConstants.db_url,
					ServerConstants.db_user, ServerConstants.db_pwd);
			System.out.println("Got Connection");
		} catch (SQLException ex) {
			ex.printStackTrace();
			// JOptionPane.showMessageDialog(
			// null,
			// "Please start the mysql Service from XAMPP Console.\n"
			// + ex.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return conn;
	}

	public static String getCurrentDate() {
		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		System.out.println(currentDate);
		return currentDate;
	}

	public static String compare() {
		String success = "";
		File folder = new File(ServerConstants.WORKSPACE_PATH + "\\image\\"
				+ ConnectionManager.getCurrentDate());
		File img1 = null;
		File img2 = null;
		if (!folder.exists()) {
			success = "<tr><th colspan=\"2\">No Output</th></tr>";
		} else {
			try {
				File[] listOfFiles = folder.listFiles();

				img1 = new File(
						listOfFiles[listOfFiles.length - 1].getAbsolutePath());
				System.out.println("File PAth = " + img1.getAbsolutePath());
				img2 = new File(
						listOfFiles[listOfFiles.length - 2].getAbsolutePath());
				System.out.println("File PAth = " + img2.getAbsolutePath());
				BufferedImage image1;
				BufferedImage image2;
				image1 = ImageIO.read(img1);
				image2 = ImageIO.read(img2);

				double f[] = new ImageCompareSSIM()
						.getLuminance(image1, image2);

				success = "<tr><th colspan=\"2\">SSIM Result</th></tr>";
				success = success + "<tr><th>Luminanace</th><td>" + f[0]
						+ "</td></tr>";
				success = success + "<tr><th>Contrast</th><td>" + f[1]
						+ "</td></tr>";
				success = success + "<tr><th>Structure</th><td>" + f[2]
						+ "</td></tr>";
				success = success + "<tr><th>SSIM</th><td>" + f[3]
						+ "</td></tr>";

				String data = " SSIM\nLuminanace " + f[0] + "\n Contrast "
						+ f[1] + "\nStructure " + f[2] + "\nSSIM " + f[3]
						+ "\n";

				// jTextArea1.setText("SSIM  \n "+
				// "Luminanace "+f[0] + "\n"+
				// "Contrast "+f[1] + "\n"+
				// "Structure "+f[2] + "\n"+
				// "SSIM "+f[3] + "\n");

				if (f[3] < 0.92) {
					success = success
							+ "<tr><th>Result</th><td>Dissimilar</td></tr>";
					data = data + "\n Dissimilar ";
				} else {
					success = success
							+ "<tr><th>Result</th><td>Similar</td></tr>";
					data = data + "\n Similar ";
				}
				System.out.println(data);

				// for (int i = listOfFiles.length-1; i >=0 ; i--) {
				// if (listOfFiles[i].isFile()) {
				// System.out.println("File " + listOfFiles[i].getName());
				// //System.out.println("File PAth = " +
				// listOfFiles[i].getAbsolutePath());
				// img1 = new File(listOfFiles[i].getAbsolutePath());
				// System.out.println("File PAth = " + img1.getAbsolutePath());
				// }
				// }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return success;
	}

	public static String uploadImg(FileItem fi) {
		String res = "";
		try {
			// File f = new
			// File(ServerConstants.WORKSPACE_PATH+"\\"+ConnectionManager.getCurrentDate()+"\\"+System.currentTimeMillis()+".jpg");
			File f = new File(ServerConstants.WORKSPACE_PATH + "\\Disease\\"
					+ ConnectionManager.getCurrentDate());
			if (!f.exists()) {
				f.mkdirs();
				boolean result = f.setExecutable(true, false);
			}

			String fileName = new File(fi.getName()).getName();
			System.out.println("fileName= " + fileName);
			File filepath = new File(f.getAbsolutePath() + "\\"
					+ System.currentTimeMillis() + ".jpg");
			String path = StringHelper.n2s(filepath);
			System.out.println("path= " + path);
			fi.write(filepath);
			byte[] a = FileUtils.readFileToByteArray(filepath);

			res = "File Uploaded Successfully!";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	public static String uploadImg(FileItem fi1, FileItem fi2) {
		String res = "";
		try {
			// File f = new
			// File(ServerConstants.WORKSPACE_PATH+"\\"+ConnectionManager.getCurrentDate()+"\\"+System.currentTimeMillis()+".jpg");
			File f = new File(ServerConstants.WORKSPACE_PATH + "\\image\\"
					+ ConnectionManager.getCurrentDate());
			if (!f.exists()) {
				f.mkdirs();
				boolean result = f.setExecutable(true, false);
			}

			String fileName = new File(fi1.getName()).getName();
			System.out.println("fileName= " + fileName);
			File filepath = new File(f.getAbsolutePath() + "\\"
					+ System.currentTimeMillis() + ".jpg");
			String path = StringHelper.n2s(filepath);
			System.out.println("path= " + path);
			fi1.write(filepath);
			byte[] a = FileUtils.readFileToByteArray(filepath);

			fileName = new File(fi2.getName()).getName();
			System.out.println("fileName= " + fileName);
			filepath = new File(f.getAbsolutePath() + "\\"
					+ System.currentTimeMillis() + ".jpg");
			path = StringHelper.n2s(filepath);
			System.out.println("path= " + path);
			fi1.write(filepath);
			a = FileUtils.readFileToByteArray(filepath);

			res = "File Uploaded Successfully!";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	public static void main(String[] args) {
		// System.out.println("");
		getConnection();
		// Date date = new Date();
		// String currentDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
		// System.out.println(currentDate);
		// System.out.println(System.currentTimeMillis());
		compare();
	}

	public static Connection getConnection() {
		return getDBConnection();
	}

	public static void closeConnection(Connection conn) {
		try {
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static String getPesticideInfo(String args) {
		args = args.replace("_", " ");
		while (args.indexOf("  ") != -1)
			args = args.replace("  ", " ");
		String plantName = "";
		String diseaseName = "";
		try {

			plantName = args.substring(0, args.indexOf(" "));
			diseaseName = args.substring(args.indexOf(" ") + 1);
			diseaseName = diseaseName.trim();
		} catch (Exception e) {

		}

		List list = DBUtils.getBeanList(PlantModel.class,
				"Select * from plantdisease where palntName like '" + plantName
						+ "' and plantDisease like '" + diseaseName + "'");

		if (list.size() > 0) {

			PlantModel pm = (PlantModel) list.get(0);
			return pm.getPesticide() + "->" + pm.getPesticidecost() + "->"
					+ pm.getPreventive();
		} else {
			return "NA" + "->" + "NA" + "->" + "NA";
		}

	}

	public static int countGreen(String grabbedImageFilePath) {
		Mat grabbedImage =imread(grabbedImageFilePath);
		// imshow("SHOWWINDOW2", grabbedImage);
		cvtColor(grabbedImage, grabbedImage, COLOR_BGR2HSV);
		Mat mask = grabbedImage.clone();
		inRange(grabbedImage, new Mat(new int[] { 40, 60, 60 }), new Mat(
				new int[] { 80, 255, 255 }), mask);
		double area = grabbedImage.arrayWidth() * grabbedImage.arrayHeight();
		double whiteCnt = countNonZero(mask);
		double per = whiteCnt / area;
		per = per * 100;
		System.out.println("whiteCnt " + whiteCnt + " ==> " + per);
		return (int) per;
		// imshow("SHOWWINDOW", mask);
	}

	public static String getPesticideInfoWeb(String args) {

		args = args.replace("_", " ");
		while (args.indexOf("  ") != -1)
			args = args.replace("  ", " ");
		String plantName = "";
		String diseaseName = "";
		try {

			plantName = args.substring(0, args.indexOf(" "));
			diseaseName = args.substring(args.indexOf(" ") + 1);
			diseaseName = diseaseName.trim();
		} catch (Exception e) {

		}
		// idplantdisease, palntName, plantDisease, symptoms, preventive,
		// pesticide, pesticidecost

		List list = DBUtils.getBeanList(PlantModel.class,
				"Select * from plantdisease where palntName like '" + plantName
						+ "' and plantDisease like '" + diseaseName + "'");

		if (list.size() > 0) {
			PlantModel pm = (PlantModel) list.get(0);
			return "<table class='simple table-dark'><thead><tr><th>Feature</th><th>Details</th></tr></thead>"
					+ " <tr><td><b>Plant Type</b</td><td>"
					+ plantName
					+ "</td></tr>"
					+ " <tr><td><b>Diease</b</td><td>"
					+ diseaseName
					+ "</td></tr>"
					+ " <tr><td><b>Pesticides Needed </b</td><td>"
					+ pm.getPesticide()
					+ "</td></tr>"
					+ "<tr><td><b>Pesticides Cost </b></td><td>"
					+ pm.getPesticidecost()
					+ "</td></tr>"
					+ "<tr><td><b>Preventive Measure</b></td><td>"
					+ pm.getPreventive() + "</td></tr></table>";
		} else {
			return "<b>Plant Info not found</b>";
		}
	}

	public static List getAllPlants() {
		String q = "SELECT * FROM plantdisease";
		return DBUtils.getBeanList(PlantModel.class, q);
	}

	public static UserModel checkLogin(HashMap parameters) {
		String uname = StringHelper.n2s(parameters.get("uname"));
		String pass = StringHelper.n2s(parameters.get("pass"));

		String query = "SELECT * FROM useraccounts where uname like ? and pass = ?";
		UserModel um = null;
		List list = DBUtils.getBeanList(UserModel.class, query, uname, pass);
		if (list.size() > 0) {
			um = (UserModel) list.get(0);
		}
		return um;
	}

	public static String insertUser(HashMap parameters) {
		System.out.println(parameters);
		String success = "";
		// uid, username, phoneno, uname, pass, udate
		String fname = StringHelper.n2s(parameters.get("fname"));
		String lname = StringHelper.n2s(parameters.get("lname"));
	
		String phoneno = StringHelper.n2s(parameters.get("phoneno"));
		String pass = "";
		try {
			pass = StringHelper.n2s(parameters.get("pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String uname = StringHelper.n2s(parameters.get("uname"));

		String data = "Select 1 from useraccounts where phoneno like '"
				+ phoneno + "' OR uname like '" + uname + "'";
		boolean v = DBUtils.dataExists(data);
		if (!v) {
			
			//userid, username, pass, phoneno, fname, lname
			String sql = "insert into useraccounts (uname, pass, phoneno, fname, lname)  "
					+ "	values(?,?,?,?,?)";
			int list = DBUtils.executeUpdate(sql, uname,pass, phoneno,fname,lname);
			if (list > 0) {
				success = "User registered Successfully";

			} else {
				success = "Error adding user to database";
			}
		} else {
			success = "Duplicate user name or phone No";
		}
		return success;
	}

	public static String kitdata(HashMap parameters) {
		System.out.println(parameters);
		String success = "";
		// did, kitid, moisture, temp, co2, udate, area
		String moisture = StringHelper.n2s(parameters.get("moisture"));
		String temp = StringHelper.n2s(parameters.get("temp"));
		String co2 = StringHelper.n2s(parameters.get("co2"));
		String area = StringHelper.n2s(parameters.get("area"));
		String kitid = StringHelper.n2s(parameters.get("ArduinoKitId"));
		String IP = StringHelper.n2s(parameters.get("IP"));

		String data = "Select * from kitdata where did = (select max(did) FROM kitdata) and temp = "
				+ temp + " and co2 = " + co2 + " and moisture = " + moisture;
		boolean v = DBUtils.dataExists(data);
		if (!v) {
			String sql = "insert into kitdata (kitid, moisture, temp, co2, area) "
					+ "	values(?,?,?,?,?)";
			int list = DBUtils.executeUpdate(sql, kitid, moisture, temp, co2,
					area);
			if (list > 0) {
				success = "Kitdata Added Successfully";

			} else {
				success = "Error adding kitdata to database";
			}
		} else {
			success = "Duplicate user name or phone No";
		}
		return success;
	}

	public static List getKitData() {
		List list = DBUtils.getBeanList(Kitdata.class,
				"select *, date(udatetime) as udate from kitdata");
		return list;
	}
}
