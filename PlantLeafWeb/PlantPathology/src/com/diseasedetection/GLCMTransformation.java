package com.diseasedetection;

import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvLoadImage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.bytedeco.javacpp.opencv_core;

import swinghelper.image.operations.ImageHelper;
import util.opencv.OpenCVHelper;

import com.constant.ServerConstants;

/**
 *
 * GLCM Transformation [ Gray Level Co-occurance Matrix]
 */
public class GLCMTransformation {

    public final int width;
    public final int height;
    public final int levels;
    public /*
             * final
             */ int[][] leveled;
    private double[][] GLCM;
    public final boolean symmetric;
    public final boolean normalize;
    public final int dx;
    public final int dy;
    //
    public Double energy;
    public Double contrast;
    public Double homogeneity;
    public Double correlation;

    public GLCMTransformation(int[][] leveled, int dx, int dy, int levels, boolean symmetric, boolean normalize) {

        this.leveled = leveled;
        this.levels = levels;
        this.symmetric = symmetric;
        this.normalize = normalize;
        width = leveled.length;
        height = leveled[0].length;
        this.dx = dx;
        this.dy = dy;
    }

    /**
     * Factory, to create from the matrix instead of leveled A new leveled
     * matrix is computed. use GLCMs to create multiple GLCMs optimally
     *
     * @return
     */
    public static GLCMTransformation createGLCM(double[][] matrix, int dx, int dy, int levels, boolean symmetric, boolean normalize, double min, double max) {
        int[][] leveled = computeLeveled(matrix, levels, min, max);
        return new GLCMTransformation(leveled, dx, dy, levels, symmetric, normalize);
    }

    public double[][] getGLCM() {

        if (GLCM == null) {
            GLCM = computeGLCM();

        }
        return GLCM;
    }

    public double[][] computeGLCM() {
        if (GLCM == null) {
            GLCM = new double[levels][levels];

            int sum = 0;
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    //range
                    if (x + dx >= 0 && y + dy >= 0 && x + dx < width && y + dy < height) {
                        int v1 = leveled[x][y];
                        int v2 = leveled[x + dx][y + dy];
                        sum++;
                        GLCM[v1][v2]++;
                        if (symmetric) {
                            GLCM[v2][v1]++;
                        }
                    }
                }
            }

            //normalize
            if (normalize) {
                if (symmetric) {
                    sum *= 2;//was counted only once to optimize
                }
                for (int x = 0; x < levels; x++) {
                    for (int y = 0; y < levels; y++) {
                        GLCM[x][y] /= sum;
                    }
                }
            }
        }

        return GLCM;
    }

    public double computeContrast() {
        if (contrast == null) {
            if (!normalize) {
                throw new Error("must be normalized");
            }
            computeGLCM();

            contrast = 0d;

            for (int x = 0; x < levels; x++) {
                for (int y = 0; y < levels; y++) {
                    contrast += (x - y) * (x - y) * GLCM[x][y];
                }
            }
        }

        return contrast;
    }

    public double computeCorrelation() {
        if (correlation == null) {
            if (!normalize) {
                throw new Error("must be normalized");
            }
            computeGLCM();
            correlation = 0d;

            //
            double meanX = 0;
            double meanY = 0;

            for (int x = 0; x < levels; x++) {
                for (int y = 0; y < levels; y++) {
                    meanX += x * GLCM[x][y];
                    meanY += y * GLCM[x][y];
                }
            }

            //
            double stdX = 0;
            double stdY = 0;

            for (int x = 0; x < levels; x++) {
                for (int y = 0; y < levels; y++) {
                    stdX += (x - meanX) * (x - meanX) * GLCM[x][y];
                    stdY += (y - meanY) * (y - meanY) * GLCM[x][y];
                }
            }
            stdX = Math.sqrt(stdX);
            stdY = Math.sqrt(stdY);

            //
            for (int x = 0; x < levels; x++) {
                for (int y = 0; y < levels; y++) {
                    double num = (x - meanX) * (y - meanY) * GLCM[x][y];
                    double denum = stdX * stdY;
                    correlation += num / denum;
                }
            }
        }

        return correlation;
    }

    public double computeEnergy() {
        if (energy == null) {
            if (!normalize) {
                throw new Error("must be normalized");
            }
            computeGLCM();
            energy = 0d;

            for (int x = 0; x < levels; x++) {
                for (int y = 0; y < levels; y++) {
                    energy += GLCM[x][y] * GLCM[x][y];
                }
            }
        }

        return energy;
    }

    public double computeHomogeneity() {
        if (homogeneity == null) {
            if (!normalize) {
                throw new Error("must be normalized");
            }
            computeGLCM();
            homogeneity = 0d;

            for (int x = 0; x < levels; x++) {
                for (int y = 0; y < levels; y++) {
                    homogeneity += GLCM[x][y] / (1 + Math.abs(x - y));
                }
            }
        }
        return homogeneity;
    }

    /**
     * static so that it can be computed only once for an array of GLCM
     *
     * @param matrix
     * @param levels
     * @param min
     * @param max
     * @return
     */
    public static int[][] computeLeveled(double[][] matrix, int levels, double min, double max) {
        int[][] _leveled = new int[matrix.length][matrix[0].length];

        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {

                int l = (int) (Math.floor((matrix[x][y] - min) * levels / (double) max));
                if (l < 0) {
                    l = 0;
                } else if (l >= levels) {
                    l = levels - 1;
                }
                _leveled[x][y] = l;
            }
        }

        return _leveled;
    }

    /**
     * for GC
     */
    public void clean() {
        GLCM = null;
    }

    public void deepClean() {
        clean();
        leveled = null;
    }

    public void computeAllTextureFeatures() {
        computeContrast();
        computeCorrelation();
        computeEnergy();
        computeHomogeneity();
//        System.out.println("Contrast " + contrast);
//        System.out.println("correlation " + correlation);
//        System.out.println("energy " + energy);
//        System.out.println("homogeneity " + homogeneity);
    }

    public static double[] computeFeatures(File grayScaleImage) {
        try {
            BufferedImage bi = ImageIO.read(grayScaleImage);

            opencv_core.IplImage image = OpenCVHelper.buffered2ipl(bi);
            opencv_core.IplImage GrayImage = null;
            if (image.nChannels() >= 3) {
                GrayImage = opencv_core.IplImage.create(image.width(), image.height(), image.depth(), 1);
                cvCvtColor(image, GrayImage, CV_BGR2GRAY);

            } else {
                GrayImage = cvLoadImage(grayScaleImage.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            }

            BufferedImage grayImageBuffered = OpenCVHelper.ipl2buffered(GrayImage);
//            System.out.println("grayImageBuffered " + grayImageBuffered);
            double[][] grayPixel = ImageHelper.getGrayPixelArray(grayImageBuffered);
            GLCMTransformation g = GLCMTransformation.createGLCM(grayPixel, 1, 1, grayPixel[0].length, true, true, 0, 255);
            g.computeAllTextureFeatures();
            double[] d = FeatureExtraction.extractSingleImage(grayScaleImage.getAbsolutePath());
            double[] data = new double[70];    // 66+4
            data[0] = g.contrast;
            data[1] = g.correlation;
            data[2] = g.energy;
            data[3] = g.homogeneity;
            
            
            for (int i = 0; i < d.length; i++) {
                if (Double.isNaN(d[i])) {
                    data[4 + i] = 0;
                } else {
                    data[4 + i] = d[i];
                }

            }
            return data;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        generateTrainingCSV();
    }

    public static void generateTrainingCSV() {

        try {
            //        double[][] matrix = new double[][]{{255, 200, 2, 3}, {1, 1, 2, 3}, {1, 0, 2, 0}, {0, 0, 0, 3}};
            ////        GLCM g = new GLCM(glcm, 1, 1, 4, false, true);
            //        GLCMTransformation g = GLCMTransformation.createGLCM(matrix, 1, 1, 4, true, true, 0, 4);
            //        g.computeAllTextureFeatures();
            File trainingFile = new File(ServerConstants.WORKSPACE_PATH + "dataset/training.csv");
            FileOutputStream fos = new FileOutputStream(trainingFile);
            File dir = new File(ServerConstants.WORKSPACE_PATH + "dataset/");
//            System.out.println(trainingFile.getCanonicalPath());
//            DecimalFormat df = new DecimalFormat("#");
//            df.setMaximumFractionDigits(8);
            File[] dataDirectories = dir.listFiles();
            for (int l = 0; l < dataDirectories.length; l++) {
                File dirName = dataDirectories[l];
                if (dirName.isDirectory()) {
                    File[] data = dirName.listFiles();
                    for (int k = 0; k < data.length; k++) {
                        File f = data[k];
                        double[] d = computeFeatures(f);
                        StringBuffer sb = new StringBuffer();
                        sb.append(dirName.getName() + "," + f.getName() + ",");
                        for (int i = 0; i < d.length; i++) {
                            sb.append(d[i] + ",");
                        }
                        sb.append("\n");
                        fos.write(sb.toString().getBytes());
                        fos.flush();
                    }
                }
            }
            fos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}