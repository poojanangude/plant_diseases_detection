package com.diseasedetection;

public class TextureCoocurenceExtraction {

	public static int[][] binImage;
	public static int height;
	public static int width;

	public static double[] coocFeatures = new double[48];

	public static double[] getCoocFeatures() {
		return coocFeatures;
	}

	public TextureCoocurenceExtraction(int[][] in, int h, int w) {
		binImage = in;
		height = h;
		width = w;
	}

	public static void extract() {
		int[][] c;
		// V(1:48)=0;
		int sum = 0;
		for (int direction = 1; direction <= 3; direction++) {
			for (int angle = 1; angle <= 4; angle++) {
				int p = 16 * (direction - 1) + 4 * (angle - 1);
				c = coocMatirx(direction, angle);
				coocFeatures[p] = c[0][0];
				coocFeatures[p + 1] = c[0][1];
				coocFeatures[p + 2] = c[1][0];
				coocFeatures[p + 3] = c[1][1];
				sum += c[0][0];
				sum += c[0][1];
				sum += c[1][0];
				sum += c[1][1];	
				// V(p+1:p+4)=comatrix(I,i,j);
			}
		}
		for (int i = 0; i < 48; i++)
			coocFeatures[i] /= sum;

	}
    // Image CO-Occurance Matrix Generation
	public static int[][] coocMatirx(int direction, int angle) {
		int[] D = new int[2];
		int[][] C = new int[2][2];
		C[0][0] = 0;
		C[0][1] = 0;
		C[1][0] = 0;
		C[1][1] = 0;
		if (angle == 1) {
			D[0] = 0;
			D[1] = direction;
		} else if (angle == 2) {
			D[0] = -direction;
			D[1] = direction;
		} else if (angle == 3) {
			D[0] = -direction;
			D[1] = 0;
		} else if (angle == 4) {
			D[0] = -direction;
			D[1] = -direction;
		}
//		System.out.println(" D "+D[0]+" "+D[1]+"=======");
		int a, b, C1, C2;
		for (int i = 1; i < height; i++) {
			for (int j = 0; j < width; j++) {
				C1 = binImage[i][j];
				a = i + D[0];
				b = j + D[1];
//				System.out.println("== "+i+" "+j);
				if (a < 0 || b < 0 || a >= height || b >= width)
					continue;
				C2 = binImage[a][b];
				C[C1][C2] = C[C1][C2] + 1;
			}
		}
		return C;
	}

}
