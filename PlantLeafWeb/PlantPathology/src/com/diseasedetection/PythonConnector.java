package com.diseasedetection;

//import android.graphics.Bitmap;
//import android.os.Environment;
//import android.util.Log;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_core.CV_8U;
import static org.bytedeco.javacpp.opencv_core.CV_8UC1;
import static org.bytedeco.javacpp.opencv_core.meanStdDev;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_CHAIN_APPROX_SIMPLE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_EXTERNAL;
import static org.bytedeco.javacpp.opencv_imgproc.CV_THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.Laplacian;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY_INV;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_OTSU;
import static org.bytedeco.javacpp.opencv_imgproc.approxPolyDP;
import static org.bytedeco.javacpp.opencv_imgproc.arcLength;
import static org.bytedeco.javacpp.opencv_imgproc.boundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.getPerspectiveTransform;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.opencv_highgui.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.bytedeco.javacpp.FloatPointer;
import org.bytedeco.javacpp.indexer.FloatIndexer;
import org.bytedeco.javacpp.indexer.UByteBufferIndexer;
import org.bytedeco.javacpp.opencv_core.CvRect;
import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_imgproc.CLAHE;
import org.bytedeco.javacv.AndroidFrameConverter;
import org.bytedeco.javacv.Frame;

import util.ExecuteDOSCommand;

import com.constant.ServerConstants;
import com.helper.FileHelper;
import com.helper.StringHelper;
import com.svm.SvmClassifier;

//import util.opencv.OpenCVHelper;
//import util.opencv.Point;
//import util.opencv.Polygon;
//import util.opencv.QRRecognizer;
//import util.opencv.Scan;
//
//import com.google.zxing.BinaryBitmap;
//import com.google.zxing.LuminanceSource;

//import com.google.zxing.MultiFormatReader;
//import com.google.zxing.NotFoundException;
//import com.google.zxing.Result;
//
//import com.google.zxing.common.HybridBinarizer;

public class PythonConnector {

	public static Mat grabbedImage = null;
	public static String fileName = "";
	boolean showWindow = false;
	boolean saveImage = true;
	boolean showSysout = true;
	boolean drawColor = true;

	public static void main1(String[] args) {
		
		Mat grabbedImage = imread("C:\\Users\\rajesh\\Videos\\Desktop\\export.jpg");
		imshow("SHOWWINDOW2", grabbedImage);
		cvtColor(grabbedImage, grabbedImage, COLOR_BGR2HSV);
		Mat mask = grabbedImage.clone();
		inRange(grabbedImage, new Mat(new int[] { 40, 60, 60 }), new Mat(
				new int[] { 80, 255, 255 }), mask);
		double area=grabbedImage.arrayWidth()*grabbedImage.arrayHeight();
		double whiteCnt=countNonZero(mask);
		double per=whiteCnt/area;
		
		System.out.println("whiteCnt "+whiteCnt+" ==> "+per);
		imshow("SHOWWINDOW", mask);
		waitKey();
	}

	public static void main2(String[] args) throws IOException {

		String dir = "D:\\work\\project\\PlantDiseaseDetection\\PlantLeafDiseaseDetectionWeb\\dataset\\Sample Specific\\Cotton\\Angular leaf spot";// tempOMR_Orginal_1515502631815.png
		dir = "D:\\work\\project\\PlantDiseaseDetection\\PlantLeafDiseaseDetectionWeb\\testingFile.jpg";

		// dir =
		// "E:\\ReceivedFiles\\dataset [1]\\Sample Specific\\Sample Specific\\Cotton\\Angular leaf spot\\";
		File f = new File(dir);
		if (f.isDirectory()) {
			File[] files = f.listFiles();
			for (int safas = 0; safas < files.length; safas++) {
				File currentImage = files[safas];
				fileName = currentImage.getName();
				if (currentImage.isFile()) {
					System.out.println(currentImage.getName());
					int yelloPixelCount = extractDiseasedLeaf(currentImage);

					waitKey();
				}
			}
		} else {
			fileName = f.getName();
			extractDiseasedLeaf(f);
		}
	}

	public static String getDecision() {
		try {

			File currentimage = new File( ServerConstants.WORKSPACE_PATH+"/testingFile.jpg");
			String op = ExecuteDOSCommand.analyzeImage(currentimage
					.getAbsolutePath());
			ServerConstants.lastOutput=op;
			return op;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "";
	}
	public static void main(String[] args) {
		String src = "D:/work/project/PlantDiseaseDetection/python-cnn/training_data/Tomato___Late_blight";
		File f[] = FileHelper.getFileList(src);
		ArrayList list=new ArrayList();
		for (int i = 0; i < f.length; i++) {
			File file = f[i];
//			file.de
//			Tomato___Septoria_leaf_spot
			String str=getDecisionUsingFile(file);
			String decision=str.split("#")[0];
			
			if (!decision.equalsIgnoreCase("Tomato___Late_blight")) {
				System.out.println("***"+file.getName());
				file.delete();
			}
//			list.add(file.getName()+"=>"+);
		}
//		System.out.println("***"+getDecision());
//		String str=getDecision();
//		System.out.println(str+"***"+str.indexOf("Tomato___Septoria_leaf_spot"));
//		String decision=str.split("#")[0];
//		if (!decision.equalsIgnoreCase("Tomato___Septoria_leaf_spot")) {
//			System.out.println("***");
////			file.delete();
////		
//		}
	
	}
	public static String getDecisionUsingFile(File currentimage) {
		try {

//			File currentimage = new File( "D:\\work\\project\\PlantDiseaseDetection/testingFile.jpg");
			// int a = extractDiseasedLeaf(currentimage);
			// System.out.println("Color Based Yellow Pixels value:" + a);
			//
			// GLCMFeatureExtraction glcmfe = new GLCMFeatureExtraction(
			// currentimage, 15);
			// glcmfe.extract();
			// // System.out.println(glcmfe.getContrast() + "," +
			// // glcmfe.getHomogenity()
			// // + "," + glcmfe.getEntropy() + "," + glcmfe.getEnergy() + ","
			// // + glcmfe.getDissimilarity() + "," + glcmfe.getIDM() + ","
			// // + glcmfe.getDM());
			// HashMap returnMap = getDecisionMap();
			// double d = SvmClassifier.getSVMPredication(new double[] {
			// glcmfe.getContrast(), glcmfe.getHomogenity(),
			// glcmfe.getEntropy(), glcmfe.getEnergy(),
			// glcmfe.getDissimilarity(), glcmfe.getIDM(), glcmfe.getDM(),
			// glcmfe.getHomogenity(), glcmfe.getClusetrShade(),
			// glcmfe.getClusterProminence() });
			// String svmAnswer = StringHelper.n2s(returnMap.get(d));
			String op = ExecuteDOSCommand.analyzeImage(currentimage
					.getAbsolutePath());
			return op;
			// if (a > 10) {
			// return "Disease Leaf.!!!";
			// } else {
			// return "Normal Leaf.!!!";
			// }
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "";
	}


	public static HashMap getDecisionMap() {
		HashMap hm = new HashMap();
		hm.put(0.0, "Normal Image");
		hm.put(1.0, "Reddening Or Lalya");
		hm.put(2.0, "Angular Leaf Spot");
		hm.put(3.0, "Grey Mildew");
		return hm;
	}

	private static int extractDiseasedLeaf(File currentImage)
			throws IOException {
		String s = currentImage.getAbsolutePath();
		PythonConnector omr = new PythonConnector();
		System.out.println("Current Image " + s);
		omr.grabbedImage = imread(s);
		if (omr.showWindow)
			imshow("Input", omr.grabbedImage);
		try {
			// if(isAndroid){
			//
			// System.out.println(":Convertign Image to RGB as its BGR");
			// cvtColor(omr.grabbedImage , omr.grabbedImage , CV_BGR2RGB);
			// imshow("RGBA", omr.grabbedImage);
			// }
			System.out.println(dir.getCanonicalPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// try {
		// System.out.println(dir.getCanonicalPath());
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		Mat hsv = grabbedImage.clone();
		cvtColor(grabbedImage, hsv, CV_BGR2HLS_FULL);
		// UByteBufferIndexer fl= hsv.createIndexer();
		// StringBuffer sb = new StringBuffer();
		// for (int row = 0; row < hsv.rows(); row++) {
		// System.out.print(row + ",");
		// for (int col = 0; col < hsv.cols(); col++) {
		// // if(mat.get(row, col)!=255&&mat.get(row, col)!=0)
		// System.out.print(fl.get(row,col,0) + ",");
		// }
		// System.out.print("\n");
		// }

		Mat combine = new Mat(hsv.rows(), hsv.cols() * 2, hsv.type());
		grabbedImage.copyTo(new Mat(combine, new Rect(0, 0, hsv.cols(), hsv
				.rows())));

		hsv.copyTo(new Mat(combine, new Rect(hsv.cols(), 0, hsv.cols(), hsv
				.rows())));
		// omr.checkValidOMR();
		// equalizeHist(combine, combine);
		// // 11,17,33 68,251,216 29,32,36 65,96,255
		// for green leaf
		inRange(hsv, new Mat(new int[] { 11, 17, 33 }), new Mat(new int[] { 68,
				251, 216 }), hsv);
		if (omr.showWindow)
			imshow("Test", hsv);
		// for green leaf
		// inRange(hsv, new Mat(new int[]{14,111,75}), new Mat(new
		// int[]{30,125,95}), hsv);

		// imshow("hsv", hsv);
		UByteBufferIndexer gr = grabbedImage.createIndexer();
		UByteBufferIndexer fl = hsv.createIndexer();
		for (int row = 0; row < hsv.rows(); row++) {
			// System.out.print(row + ",");
			for (int col = 0; col < hsv.cols(); col++) {
				// if(mat.get(row, col)!=255&&mat.get(row, col)!=0)
				if (fl.get(row, col) == 0)
					gr.put(row, col, new int[] { 0, 0, 0 });
				// System.out.print(fl.get(row,col,0) + ",");
			}
			// System.out.print("\n");
		}
		// imshow("T", hsv);
		// waitKey(5000);
		// System.out.println(hsv);
		Mat hsv2 = grabbedImage.clone();
		// cvtColor(hsv2, hsv2, CV_RGB2BGR);

		// // cvtColor(hsv2, hsv2, CV_RGB2Luv);
		// imwrite("D:/a.jpg", hsv2);
		//
		inRange(hsv2, new Mat(new int[] { 14, 111, 75 }), new Mat(new int[] {
				30, 125, 95 }), hsv2);

		// Mat GrayImage=new Mat(grabbedImage.rows(),grabbedImage.cols(),1);
		// cvtColor(grabbedImage, GrayImage, CV_BGR2GRAY);
		//

		int yelloPixelCount = countNonZero(hsv2);

		System.out.println(currentImage.getName() + "=>" + yelloPixelCount);
		// System.out.println("nzero " + yelloPixelCount);
		// GaussianBlur(GrayImage, GrayImage, new Size(11,11), 0);
		// Canny(GrayImage, GrayImage, 225.0, 175.0);

		// imwrite(dir.getAbsolutePath() + "/hsv_" + fileName + ".jpg",
		// combine);
		// imwrite(dir.getAbsolutePath() + "/hsv2_" + fileName + ".jpg",
		// hsv);
		// imwrite(dir.getAbsolutePath() + "/final_" + fileName + ".jpg",
		// grabbedImage);
		// imwrite(dir.getAbsolutePath() + "/canny_" + fileName + ".jpg",
		// GrayImage);
		// imshow("T", grabbedImage);
		if (omr.showWindow)
			cvWaitKey();
		combine.release();
		omr.grabbedImage.release();
		return yelloPixelCount;
	}

	private static boolean isAndroid = true;

	private static void extractOMR2(File currentImage) {
		String s = currentImage.getAbsolutePath();
		PythonConnector omr = new PythonConnector();
		omr.grabbedImage = imread(s);
		imshow("Input", omr.grabbedImage);
		try {
			if (isAndroid) {

				System.out.println(":Convertign Image to RGB as its BGR");
				cvtColor(omr.grabbedImage, omr.grabbedImage, CV_BGR2RGB);
				imshow("RGBA", omr.grabbedImage);
			}
			System.out.println(dir.getCanonicalPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Mat hsv = grabbedImage.clone();
		cvtColor(grabbedImage, hsv, CV_BGR2HLS_FULL);
		Mat combine = new Mat(hsv.rows(), hsv.cols() * 2, hsv.type());
		grabbedImage.copyTo(new Mat(combine, new Rect(0, 0, hsv.cols(), hsv
				.rows())));
		hsv.copyTo(new Mat(combine, new Rect(hsv.cols(), 0, hsv.cols(), hsv
				.rows())));
		// // 11,17,33 68,251,216 29,32,36 65,96,255
		// for green leaf
		inRange(hsv, new Mat(new int[] { 11, 17, 33 }), new Mat(new int[] { 68,
				251, 216 }), hsv);

		// imshow("hsv", hsv);
		UByteBufferIndexer gr = grabbedImage.createIndexer();
		UByteBufferIndexer fl = hsv.createIndexer();
		for (int row = 0; row < hsv.rows(); row++) {
			// System.out.print(row + ",");
			for (int col = 0; col < hsv.cols(); col++) {
				// if(mat.get(row, col)!=255&&mat.get(row, col)!=0)
				if (fl.get(row, col) == 0)
					gr.put(row, col, new int[] { 0, 0, 0 });
				// System.out.print(fl.get(row,col,0) + ",");
			}
			// System.out.print("\n");
		}

		// waitKey(5000);
		System.out.println(hsv);
		Mat hsv2 = grabbedImage.clone();
		inRange(hsv2, new Mat(new int[] { 14, 111, 75 }), new Mat(new int[] {
				30, 125, 95 }), hsv2);

		// countNonZero(arg0)
		// Mat GrayImage=new Mat(grabbedImage.rows(),grabbedImage.cols(),1);
		// cvtColor(grabbedImage, GrayImage, CV_BGR2GRAY);
		//
		//

		// GaussianBlur(GrayImage, GrayImage, new Size(11,11), 0);
		// Canny(GrayImage, GrayImage, 225.0, 175.0);

		imshow("Test", hsv2);

		imwrite(dir.getAbsolutePath() + "/hsv_" + fileName + ".jpg", combine);
		// imwrite(dir.getAbsolutePath() + "/hsv2_" + fileName + ".jpg",
		// hsv);
		imwrite(dir.getAbsolutePath() + "/final_" + fileName + ".jpg",
				grabbedImage);
		// imwrite(dir.getAbsolutePath() + "/canny_" + fileName + ".jpg",
		// GrayImage);
		// imshow("T", grabbedImage);

		cvWaitKey();
		combine.release();
		omr.grabbedImage.release();
	}

	public static File dir = new File("./Database");

	public boolean checkValidOMR() {
		// File dir = new
		// File(android.os.Environment.getExternalStorageDirectory().toString()
		// + "/Database");
		File dir = new File("./Database");
		try {
			System.out.println(dir.getCanonicalPath());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (!dir.exists()) {
			dir.mkdirs();
		}

		boolean success = false;
		try {

			// Rect cvRect2 = new Rect(cx, cy, cwidth, cheight);
			// Rect cvRect2 = new Rect(0, 0, 100, 120);
			// rectangle(grabbedImage, cvRect2, Scalar.BLUE);

			// System.out.println(fileName);

			Mat grabbedImageRoi = grabbedImage;
			// try {
			// grabbedImageRoi = new Mat(grabbedImage, cvRect2);
			// } catch (Exception e) {
			// print("Error! Image Seems to be of diffren size than expected ROI");
			// e.printStackTrace();
			// success = false;
			// return success;
			// }

			imshow("Test", grabbedImage);
			// imwrite("test.png", grabbedImage);
			// cvWaitKey();
			// imw
			// cvSetImageROI(grabbedImage, cvRect2);
			// Mat localImage = grabbedImage.clone();
			// Mat local = new Mat(localImage, cvRect2);
			if (saveImage) {
				imwrite(dir.getAbsolutePath() + "/original_" + fileName
						+ ".jpg", grabbedImage);
				// imwrite(dir.getAbsolutePath() + "/1_grab.jpg",
				// grabbedImageRoi);
			}
			Mat GrayImage = new Mat(grabbedImageRoi.rows(),
					grabbedImageRoi.cols(), 1);
			//
			cvtColor(grabbedImageRoi, GrayImage, CV_BGR2GRAY);
			// isBlurred = isBlurredImage(GrayImage);
			// if (isBlurred) {
			// print("BLURRED Image===============================" + fileName);
			// GrayImage.release();
			// grabbedImageRoi.release();
			// // cvRect2.deallocate();
			// // return false;
			// }

			// Mat histMat = new Mat(grabbedImageRoi.rows(),
			// grabbedImageRoi.cols(), CV_8UC1);

			// histMat.release();
			if (saveImage) {

				imwrite(dir.getAbsolutePath() + "/2_gray.jpg", GrayImage);
			}

			// CLAHE c= createCLAHE(2.0, new Size(4,4));
			// c.apply(GrayImage, GrayImage);

			// if (saveImage) {
			// imwrite("./Database/histCLAHE_"+fileName,GrayImage);
			// // imwrite(dir.getAbsolutePath() + "/3_smooth.jpg", GrayImage);
			// }

			// if(true){
			// GaussianBlur(GrayImage, GrayImage, new Size(11, 11), 0);
			// imshow("GrayImage",GrayImage);
			// adaptiveThreshold(GrayImage, GrayImage, 255,
			// ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 11, 2);
			imshow("Threshold", GrayImage);
			// }else{
			threshold(GrayImage, GrayImage, 0, 255, THRESH_OTSU);
			// }
			if (saveImage) {
				imwrite(dir.getAbsolutePath() + "/4_threshold" + fileName
						+ ".jpg", GrayImage);
			}
			MatVector contours = new MatVector();
			Mat contourImage = GrayImage.clone();
			findContours(contourImage, contours, CV_RETR_EXTERNAL,
					CV_CHAIN_APPROX_SIMPLE); // CV_RETR_EXTERNAL
			// CV_RETR_LIST
			// imwrite("after_contour.jpg", GrayImage);
			// //CV_CHAIN_APPROX_SIMPLE
			ArrayList<Rect> arrRects = new ArrayList<Rect>();
			ArrayList<Double> arrRectsArea = new ArrayList<Double>();
			ArrayList<ArrayList<Integer>> arrOthers = new ArrayList<ArrayList<Integer>>();
			int i = 1;
			int width = grabbedImageRoi.cols();
			int height = grabbedImageRoi.rows();
			print("width  height in " + width + " " + height);
			int width5per = width * 5 / 100;
			int height5per = height * 5 / 100;
			int width70per = width * 70 / 100;
			int height80per = height * 80 / 100;
			int areaMarkerBulletMin = (int) ((width * 0.01f) * (height * 0.03f));
			int areaMarkerBulletMax = (int) ((width * 0.06f) * (height * 0.1f));
			int area5PerImage = width5per * height5per;
			print("areaMarkerBullet " + areaMarkerBulletMax);
			// print("area5PerImage " + area5PerImage);
			// Polygon.Builder four_corners_polygon = Polygon.Builder();
			ArrayList<Integer> arrCornerXY = new ArrayList<Integer>();
			ArrayList<Boolean> countCornerXY = new ArrayList<Boolean>();

			for (int contourIndex = 0; contourIndex < contours.size(); contourIndex++) {
				Mat currentContour = contours.get(contourIndex);
				if (currentContour != null) {

					Rect boundRect = boundingRect(currentContour);
					int cx = boundRect.x();
					int cy = boundRect.y();
					int contour_width = boundRect.width();
					int contour_height = boundRect.height();
					double widthHeightRatio = (contour_width * 1.0)
							/ contour_height;
					double areaOfContour = (contour_width * contour_height);

					if (areaOfContour > areaMarkerBulletMin) {

						Mat PolyXYPoints = new Mat();
						approxPolyDP(currentContour, PolyXYPoints,
								arcLength(currentContour, true) * 0.02, true);
						int noOfContourPoints = PolyXYPoints.rows();
						// print("vectorPoints.size() " +
						// noOfContourPoints);

						if (noOfContourPoints >= 4) {
						}
						PolyXYPoints.release();
					}
					currentContour.release();

				}

			}
			contourImage.release();
			contourImage.deallocateReferences();
			contours.deallocateReferences();
			contours.deallocate();
			GrayImage.release();

			grabbedImageRoi.release();
			arrRects.clear();
			arrOthers.clear();
			arrRectsArea.clear();
			waitKey();
			// localImage.release();

		} catch (Error e) {
			e.printStackTrace();
		}

		return success;
	}

	public void print(String message) {
		if (showSysout) {
			System.out.println(message);
		}
		// android.util.Log.i(LOG_TAG, message);
	}

}
