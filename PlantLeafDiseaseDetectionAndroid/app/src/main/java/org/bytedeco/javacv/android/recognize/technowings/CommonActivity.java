package org.bytedeco.javacv.android.recognize.technowings;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.widget.Toast;


import util.opencv.AndroidConstants;
import util.opencv.HttpView;
import util.opencv.StringHelper;


@SuppressLint("NewApi")
public class CommonActivity extends AppCompatActivity {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.itemMain:
                go(WelcomeActivity.class);
                break;
            case R.id.action_settings:
                go(ConfigTabActivity.class);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        android.os.StrictMode.ThreadPolicy tp = android.os.StrictMode.ThreadPolicy.LAX;
        android.os.StrictMode.setThreadPolicy(tp);
    }

    public static void CancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx
                .getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    @JavascriptInterface
    public void goClass(String c1) {
        Class c = null;
        try {
            System.out.println("i am in here " + c1);
            c = Class.forName(c1);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Intent intent = new Intent(CommonActivity.this, c);
        startActivity(intent);
    }
    public void sendData(File sourceFileUri, String upLoadServerUri) {

//        String imei=cm.getIMEI(this);
        //System.out.println("IMEI :"+imei);
//        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            requirePermissions();
//        }
//        String imei = manager.getDeviceId();
        // String imei = "aaaa";
        // String url1 = AndroidConstants.AJAX_URL() + "?methodId=updateBusTracking&imei=" + imei + "&lat=" + URLEncoder.encode(AndroidConstants.latitude + "") + "&lang=" + URLEncoder.encode(AndroidConstants.longitude + "");
        System.out.println("Sending FGile to Server " + sourceFileUri);
        System.out.println("Sending upLoadServerUri to Server " + upLoadServerUri);
        try {
            String returnOutput = (String) util.HttpView.uploadFile(sourceFileUri, upLoadServerUri);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Sending ERROR  " + e.getMessage());
        }


    }
    public void go(Class c) {
        Intent intent = new Intent(CommonActivity.this, c);
        startActivity(intent);
//        finish();
    }

    public void goh(Class c) {
        Intent intent = new Intent(CommonActivity.this, c);
        startActivity(intent);

    }

    @JavascriptInterface
    public boolean checkConnectivityServer() {
        boolean success = checkConnectivityServer(AndroidConstants.MAIN_SERVER_IP, StringHelper.n2i(AndroidConstants.MAIN_SERVER_PORT));
        return success;

    }

    public static boolean checkConnectivityServer(String ip, int port) {
        boolean success = false;
        try {
            System.out.println("Checking Connectivity With " + ip + " " + port);
            Socket soc = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(ip, port);
            soc.connect(socketAddress, 3000);
            success = true;
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        System.out.println(" Connecting to server " + success);
        return success;

    }

    public void toast(String message) {
        Toast t = Toast.makeText(CommonActivity.this, message, Toast.LENGTH_SHORT);
        t.show();
    }


    ProgressDialog progressDialog = null;
    AlertDialog alertDialog = null;

    class CheckConnectivityAsyncTask extends AsyncTask<String, String, String> {
        String message = "";
        String title = "";
        String action = "";

        @Override
        protected void onPreExecute() {
            System.out.println("In Aysnc");
            progressDialog = ProgressDialog.show(CommonActivity.this,
                    "Please Wait", "Loading....", true);
            alertDialog = new AlertDialog.Builder(CommonActivity.this).create();
        }

        @Override
        protected String doInBackground(String... params) {
            String ip = params[0];
            int port = StringHelper.n2i(params[1]);
            boolean success = HttpView.checkConnectivityServer(ip, port);

            if (success) {

                title = "Success";
                if (params.length > 2 && params[2].equalsIgnoreCase("UpdateIp")) {
                    action = "1";
                    message = "Connection established with the Main Server.";
                    AndroidConstants.MAIN_SERVER_IP = ip;
                    AndroidConstants.MAIN_SERVER_PORT = port + "";
                } else {
                    message = "Internet Connection Successful!";
                }
            } else {
                action = "";
                message = "Error Connecting to Server http://" + ip + ":"
                        + port;
                title = "Connectivity Error";
            }

            return success + "";
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            alertDialog.setTitle(title);
            alertDialog.setMessage(message);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.hide();
                    if (action.length() > 0) {

                        Intent main = new Intent(CommonActivity.this,
                                WelcomeActivity.class);
                        startActivity(main);
                        finish();

                    }

                }
            });
            alertDialog.show();

        }

        ;

    }

    @JavascriptInterface
    public void finished() {
        try {
            System.runFinalizersOnExit(true);
            finish();
            super.finish();
            super.onDestroy();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            android.os.Process.killProcess(android.os.Process.myPid());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @JavascriptInterface
    public String getIMEI() {

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        System.out.println("Device IMEI is " + imei);
        return imei;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    Object returnObject = null;

    private class AsyncTaskRunner extends AsyncTask<String, String, Object> {
        @Override
        protected Object doInBackground(String... params) {
            publishProgress("Connecting to Server..."); // Calls onProgressUpdate()
            try {
                returnObject = null;
                // Do your long operations here and return the result
                String url = params[0];
                returnObject = HttpView.connect2ServerObject(url);

            } catch (Exception e) {
                e.printStackTrace();

            }
            return returnObject;
        }

        @Override
        protected void onPostExecute(Object result) {
            // execution of result of Long time consuming operation
            progressDialog.setTitle("Success");
            progressDialog.dismiss();
            returnObject = result;
        }


        @Override
        protected void onPreExecute() {
            // Things to be done before execution of long running operation. For
            // example showing ProgessDialog

            progressDialog = ProgressDialog.show(CommonActivity.this,
                    "Please Wait", "Loading....", true);
        }


        @Override
        protected void onProgressUpdate(String... text) {
            progressDialog.setTitle(text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }

    ;

    boolean isServiceRunning(Class service1) {

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);// use context received in broadcastreceiver

        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {

            if (service1.getName().equals(service.service.getClassName())) {
                return true;
            } else {
                return false;
            }

        }
        return false;

    }

}
