package org.bytedeco.javacv.android.recognize.technowings;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;

import util.FileDialog;
import util.OpenCVImageCompareSqrDiff;
import util.opencv.AndroidConstants;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class PlantCompareActivity extends CommonActivity {

    private boolean mPermissionReady;
    String imagePath1 = "";//original Image
    String imagePath2 = "";//Experiemental  Image
    ImageButton image1 = null, image2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plant_compare);


//        try {
//            String filename = getIntent().getStringExtra("filename");
//            readFileAndView(filename);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        image1 = (ImageButton) findViewById(R.id.imageViewPlant1);
        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File mPath = new File(Environment.getExternalStorageDirectory() + "//DIR//");
                FileDialog fileDialog = new FileDialog(PlantCompareActivity.this, mPath, ".PNG,.jpg,.bmp");
                fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                    public void fileSelected(File file) {

//                        Log.d(getClass().getName(), "selected file " + file.toString());
                        System.out.println("Writing selected file " + file.toString());
                        if (file.isFile()) {

                            imagePath1 = file.getAbsolutePath();
                            Bitmap b = BitmapFactory.decodeFile(file.getAbsolutePath());
                            ImageView ima = (ImageView) findViewById(R.id.imageViewPlant1);
                            ima.setImageBitmap(b);

                        }
                    }
                });
                fileDialog.showDialog();
            }
        });
        image2 = (ImageButton) findViewById(R.id.imageViewPlant2);
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File mPath = new File(Environment.getExternalStorageDirectory() + "//DIR//");
                FileDialog fileDialog = new FileDialog(PlantCompareActivity.this, mPath, ".PNG,.jpg,.bmp");
                fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                    public void fileSelected(File file) {

//                        Log.d(getClass().getName(), "selected file " + file.toString());
                        System.out.println("Writing selected file " + file.toString());
                        if (file.isFile()) {

                            imagePath2 = file.getAbsolutePath();
                            Bitmap b = BitmapFactory.decodeFile(file.getAbsolutePath());
                            ImageView ima = (ImageView) findViewById(R.id.imageViewPlant2);
                            ima.setImageBitmap(b);

                        }
                    }
                });
                fileDialog.showDialog();
            }
        });
        ImageButton compare = (ImageButton) findViewById(R.id.compareButton);

        compare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    ((TextView) findViewById(R.id.textView2)).setText("");
                    Mat original = imread(imagePath1);
                    Mat experimental = imread(imagePath2);
                    resize(original, original, new Size(512, 512));
                    resize(experimental, experimental, new Size(512, 512));

                    OpenCVImageCompareSqrDiff ssim = new OpenCVImageCompareSqrDiff();
                    OpenCVImageCompareSqrDiff.Color[][] orginalSignature = ssim.calcSignature(original);
                    OpenCVImageCompareSqrDiff.Color[][] experimentalSignature = ssim.calcSignature(experimental);
                    original.release();
                    experimental.release();
                    double distance = ssim.calcDistance(orginalSignature, experimentalSignature);
                    distance = ronndOff(distance);
                    double perf = (distance * 100.0 / 19125.0);
                    perf= ronndOff(perf);
                    String s = perf + "";
                    System.out.println("data:" + s);

//                    if (s.indexOf(".") != -1) {
//                        try {
//                            s = String.format("%.2f", s);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            s = perf + "";
//                        }
//                    }

//                    perf = perf * 100.0 / 100;
                    ((TextView) findViewById(R.id.textView2)).setText("Distance =>" + Math.round(distance) + "\n % Diffrence=>" + s + "%\n");
                    if (perf < 1) {
                        toast(" Perfect Match % " + s);
                        ((TextView) findViewById(R.id.textView2)).append("Plants are similar");

                    } else {
                        if (perf >= 1 && perf < 30) {
                            ((TextView) findViewById(R.id.textView2)).append("Plant leaf  growth has changed a bit");
                        } else {
                            ((TextView) findViewById(R.id.textView2)).append("Plant leaf  growth has changed drastically!");
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public double ronndOff(double value) {

        double valueRounded = Math.round(value * 100D) / 100D;
        return valueRounded;
    }

}
