package org.bytedeco.javacv.android.recognize.technowings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.widget.Toast;

import org.bytedeco.javacpp.opencv_core;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import util.PlantModel;
import util.opencv.AndroidConstants;
import util.opencv.StringHelper;

import util.HttpView;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY_INV;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

public class PlantAnalysisActivity extends CommonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recognize_picture);


        final TextView textView6 = (TextView) findViewById(R.id.textView6);

        textView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String deci = textView6.getText().toString();
                if (deci.equalsIgnoreCase("Decision")) {
                    toast("Please select an image to recognize");
                    return;
                }
                if (AndroidConstants.symptoms.length() > 0) {

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent intent = new Intent(PlantAnalysisActivity.this, PlantSymptomsActivity.class);
                            intent.putExtra("filename", filename);
                            startActivity(intent);
                            //Do something after 100ms
                        }
                    }, 0 * 1000);
                } else {
                    toast("Disease not found in DB");
                }
            }
        });
        findViewById(R.id.buttonBrowse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (browse) {
                    analyze(filename);
                } else {
                    toast("Please select an image to browse");
                }
            }
        });
        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewDetails();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
//        try {
//            String url = AndroidConstants.AJAX_URL() + "methodId=getPlants";
//            plantList = (List) HttpView.connect2ServerObject(url);
//            addItemsOnSpinner2();
//
//            String filename = StringHelper.n2s(getIntent().getStringExtra("filename"));
//            if (filename.length() > 0) {
////                File f = new File(filename);
//                this.filename = filename;
//                analyze(filename);
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

//    Spinner spinner2 = null;

    public void addItemsOnSpinner2() {
        try {
            List list = new ArrayList<String>();
//            spinner2 = (Spinner) findViewById(R.id.spinnerId);
//            for (int i = 0; plantList != null && i < plantList.size(); i++) {
//
//                PlantModel plant = (PlantModel) plantList.get(i);
//                if (!list.contains(plant.getPalntName())) {
//                    list.add(plant.getPalntName());
//                }
//            }
//
//            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                    R.layout.spinner_item, list);
//            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            spinner2.setAdapter(dataAdapter);
//            spinner2.setSelection(AndroidConstants.previousIndex);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    int PICK_PHOTO_FOR_AVATAR = 1233;

    public void pickImage() {
        browse = false;
//        AndroidConstants.previousIndex = spinner2.getSelectedItemPosition();
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    public void viewDetails() {

        AlertDialog alertDialog = new AlertDialog.Builder(
                PlantAnalysisActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Pesticide Info");

        // Setting Dialog Message
        if (tomatoDetails.length() > 2) {
            String data[] = tomatoDetails.split("->");
            String finalData = "";
            //pm.getPesticide()+"->"+pm.getPesticidecost()+"->"+pm.getPreventive();
            finalData = " Pesticide Name:" + data[0] + "\n Pesticide cost:" + data[1] + "\n Preventive :" + data[2];
            alertDialog.setMessage(finalData);


            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to execute after dialog closed
                    Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }


    }

    boolean browse = false;
    String tomatoDetails = "";

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
//            InputStream inputStream = context.getContentResolver().openInputStream(data.getData());
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                AndroidConstants.bitmap = bitmap;
                // Log.d(TAG, String.valueOf(bitmap));
                browse = true;
                try {
                    File file = new File(getPath(PlantAnalysisActivity.this
                            , uri));
                    filename = file.getAbsolutePath();
//                    spinner2.setSelection(AndroidConstants.previousIndex);
                    readFileAndView(file.getAbsolutePath());
//                    System.out.println("uri.toString() " + uri.toString());
//                    Intent intent = new Intent(MainMenuActivity.this, PlantAnalysisActivity.class);
//                    intent.putExtra("filename", file.getAbsolutePath());
//                    startActivity(intent);
//                    finish();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
//                ImageView imageView = (ImageView) findViewById(R.id.imageView);
//                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }

    String filename = "";

    @SuppressLint("NewApi")
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    List plantList = null;


    public void analyze(final String filename) {
        try {
            readFileAndView(filename);
            sendData(new File(filename), AndroidConstants.AJAX_URL() + "methodId=uploadImage2");
            String url = AndroidConstants.AJAX_URL() + "methodId=getDecision";
            Object obj = HttpView.connect2ServerObject(url);
            String returnOutput = "";
            TextView textView = (TextView) findViewById(R.id.textView6);
//            AndroidConstants.previousIndex = spinner2.getSelectedItemPosition();
            String plantName = "";
            String plantProbability = "";
            String plantDisease = "";
            String completePlantName = "";
//            String selectedPlantName = StringHelper.n2s(spinner2.getSelectedItem());
            if (obj != null) {
                try {
                    returnOutput = (String) obj;
                    String[] t = returnOutput.split("#");
                    String tomatoData = returnOutput.split("=>")[1];
                    tomatoDetails = tomatoData;
                    String plantNameDisease = t[0];
                    completePlantName = t[0];
                    plantName = plantNameDisease.split("___")[0];
                    plantDisease = plantNameDisease.split("___")[1];
                    plantProbability = t[1];
                    AndroidConstants.plantProbabilities_all = t[2];
                    AndroidConstants.plantClasses_all = t[3];
                    AndroidConstants.plantProbability = plantProbability;
                    AndroidConstants.plantDisease = plantDisease;
                    AndroidConstants.plantName = plantName;
//                    if (selectedPlantName.toLowerCase().equalsIgnoreCase(AndroidConstants.plantName))
//                        toast("Leaf belongs to " + AndroidConstants.plantName);
//                    else {
                    Context context = PlantAnalysisActivity.this;

                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(context);
                    }
                    builder.setTitle("Alert")
                            .setMessage(" Leaf belongs to " + plantName)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.cancel();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
//                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                String output = "" + returnOutput;
                textView.setText(plantDisease);
//            if (output != null && plantDisease.toLowerCase().indexOf("healthy") == -1) {
                PlantModel plant = null;
                for (int i = 0; plantList != null && i < plantList.size(); i++) {

                    PlantModel selectedModel = (PlantModel) plantList.get(i);
                    if (selectedModel.getPalntName().equalsIgnoreCase(completePlantName)) {
                        plant = selectedModel;
                        break;
                    }
                }
                if (plant == null) {
                    AndroidConstants.pesticides = "";
                    AndroidConstants.symptoms = "";
                    AndroidConstants.preventive = "";
                    AndroidConstants.plantProbability = "";
                    AndroidConstants.plantDisease = "";
                    AndroidConstants.plantName = "";
                    System.out.println("Disease not found in database.. Very Wierd!!");
                } else {
                    System.out.println("plant " + plant);
//                    plant = (PlantModel) plantList.get((int) (Math.random() * (plantList.size() - 1)));
                    AndroidConstants.pesticides = plant.getPesticide();
                    AndroidConstants.symptoms = plant.getSymptoms();
                    AndroidConstants.preventive = plant.getPreventive();

                }
//            } else {
//
//                AndroidConstants.pesticides = "";
//                AndroidConstants.symptoms = "";
//                AndroidConstants.preventive = "";
//                AndroidConstants.plantProbability = "";
//                AndroidConstants.plantDisease = "";
//                AndroidConstants.plantName = "";
//            }


            }


       /* if(returnOutput.indexOf("Normal")>=0){
            textView.setTextColor(Color.parseColor("#40ff5e"));
        }else{
            textView.setTextColor(Color.parseColor("#DC143C"));
        }*/


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readFileAndView(String filename) {
        try {


            //  toast(filename);
            System.out.println("filename " + filename);
            Bitmap bmp = BitmapFactory.decodeFile(filename);
            ImageView img = (ImageView) findViewById(R.id.imageViewPlant2);
            img.setImageBitmap(bmp);

            String BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Database";
            new File(BASE_PATH).mkdirs();
            File f = new File(filename);
            opencv_core.Mat grabbedImageRoi = imread(filename);
            opencv_core.Mat GrayImage = new opencv_core.Mat(grabbedImageRoi.rows(), grabbedImageRoi.cols(), 1);
            cvtColor(grabbedImageRoi, GrayImage, CV_BGR2GRAY);
            imwrite(BASE_PATH + "/Gray_" + f.getName(), GrayImage);
            threshold(GrayImage, GrayImage, 130, 255, THRESH_BINARY_INV);
            imwrite(BASE_PATH + "/Binary_" + f.getName(), GrayImage);
            GrayImage.release();
            browse = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

}
