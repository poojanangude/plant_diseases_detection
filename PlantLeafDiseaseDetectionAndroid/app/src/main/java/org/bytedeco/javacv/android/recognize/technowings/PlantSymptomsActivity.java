package org.bytedeco.javacv.android.recognize.technowings;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import util.opencv.AndroidConstants;

public class PlantSymptomsActivity extends CommonActivity {

    private boolean mPermissionReady;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plant_symptoms);
        findViewById(R.id.buttonAnalysis).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
go();
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(PlantAnalysisActivity.class);
            }
        });
        try {
            String filename = getIntent().getStringExtra("filename");
            readFileAndView(filename);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void go(){
        setContentView(R.layout.plant_analysis);
        try {
            String BASE_PATH=Environment.getExternalStorageDirectory().getAbsolutePath() + "/Database";
            new File(BASE_PATH).mkdirs();
            String filename = getIntent().getStringExtra("filename");
            File f = new File(filename);
            Bitmap gray = BitmapFactory.decodeFile(BASE_PATH + "/Gray_" + f.getName());
            ImageView img = (ImageView) findViewById(R.id.imageViewPlant1);
            img.setImageBitmap(gray);
            Bitmap binary = BitmapFactory.decodeFile(BASE_PATH + "/Binary_" + f.getName());
            ImageView binaryImg = (ImageView) findViewById(R.id.imageViewPlant2);
            binaryImg.setImageBitmap(binary);
            TextView tv = (TextView) findViewById(R.id.textView);
            tv.setText(AndroidConstants.plantClasses_all + "\n " + AndroidConstants.plantProbabilities_all + "\n\n" + AndroidConstants.plantName + "\n " + AndroidConstants.plantProbability);
            findViewById(R.id.compareButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(PlantSymptomsActivity.this, PlantSymptomsActivity.class);
                    intent.putExtra("filename", filename);
                    startActivity(intent);
                    finish();
//
//                setContentView(R.layout.plant_symptoms);
//                readFileAndView(filename);
//                findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        go(PlantAnalysisActivity.class);
//                    }
//                });
                }
            });
        }catch(Exception e){
            e.printStackTrace();;
        }
    }

    private void readFileAndView(String filename) {

            Bitmap bmp = BitmapFactory.decodeFile(filename);
            ImageView img = (ImageView) findViewById(R.id.imageViewPlant2);
            img.setImageBitmap(bmp);
            ((TextView) findViewById(R.id.textViewSymptoms)).setText(AndroidConstants.symptoms);
            ((TextView) findViewById(R.id.textViewPreventive)).setText(AndroidConstants.preventive);
            ((TextView) findViewById(R.id.textViewPesticides)).setText(AndroidConstants.pesticides);


    }

}
