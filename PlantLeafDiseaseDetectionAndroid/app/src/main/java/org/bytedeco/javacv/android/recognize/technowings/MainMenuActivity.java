package org.bytedeco.javacv.android.recognize.technowings;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import util.HttpView;
import util.opencv.AndroidConstants;
import util.opencv.StringHelper;

public class MainMenuActivity extends CommonActivity {

    private boolean mPermissionReady;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        findViewById(R.id.btnOpenCv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(CameraCaptureActivity.class);
            }
        });

        findViewById(R.id.recognizePictureBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(PlantAnalysisActivity.class);
            }
        });
        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go(PlantCompareActivity.class);
            }
        });

    }

    public void viewDetails() {
        String url = AndroidConstants.AJAX_URL() + "methodId=getPhValue";
        Object obj = HttpView.connect2ServerObject(url);
        String returnOutput = "";
        if (obj != null) {
            try {
                returnOutput = (String) obj;
                AlertDialog alertDialog = new AlertDialog.Builder(
                        this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Pesticide Info");

                // Setting Dialog Message
                if(returnOutput.length()>2) {
                    String data[] = returnOutput.split("->");
                    String finalData = "";
                    //pm.getPesticide()+"->"+pm.getPesticidecost()+"->"     +pm.getPreventive();
                    finalData = "PH Value:" + data[0] + "\nFertilizer to provied:" + data[1];
                    alertDialog.setMessage(finalData);


                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog closed
                            Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
