package util;


import java.io.File;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class OpenCVImageCompareSqrDiff {
    // The reference image "signature" (25 representative pixels, each in R,G,B).
    // We use instances of Color to make things simpler.

    private Color[][] refSignature;
    Mat refImage;
    // The base size of the images.
    private static final int baseSize = 600;
    
    public Color[][] calcSignature(String sourceImage) {
        // read textbox,read image,set icon

        Mat sourceMat = imread(sourceImage, CV_LOAD_IMAGE_COLOR);
        if (sourceMat.channels() == 1) {
            return null;
        }
        return calcSignature(sourceMat);

//  System.out.println("ii "+ii);
    }
    
    public Color[][] calcSignature(Mat sourceImage) {
        // read textbox,read image,set icon

//        Mat m = imread(sourceImage, CV_LOAD_IMAGE_COLOR);
        if (sourceImage.channels() == 1) {
            return null;
        }
        
        
        int width = sourceImage.cols();
        int height = sourceImage.rows();
        if (width != baseSize && height != baseSize) {
            resize(sourceImage, sourceImage, new Size(baseSize, baseSize));
        }
        
        org.bytedeco.javacpp.indexer.UByteIndexer ii = sourceImage.createIndexer();
//        System.out.println("ii " + ii.getClass());
        Color[][] sig = new Color[5][5];
        // For each of the 25 signature values average the pixels around it.
        // Note that the coordinate of the central pixel is in proportions.
        float[] prop = new float[]{1f / 10f, 3f / 10f, 5f / 10f, 7f / 10f, 9f / 10f};
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                sig[x][y] = averageAround(ii, prop[x], prop[y]);
            }
        }
        return sig;

//  System.out.println("ii "+ii);
    }
    
    public Color averageAround(org.bytedeco.javacpp.indexer.UByteIndexer ii, double px, double py) {
        double[] pixel = new double[3];
        double[] accum = new double[3];
        Color[][] sig = new Color[5][5];
        int sampleSize = 15;
        int numPixels = 0;
        for (double x = px * baseSize - sampleSize; x < px * baseSize + sampleSize; x++) {
            for (double y = py * baseSize - sampleSize; y < py * baseSize + sampleSize; y++) {
                //  int pixel = img.getRGB(j, i);
                int xi = (int) x;
                int yi = (int) y;
                int r = ii.get(yi, xi, 2);
                int g = ii.get(yi, xi, 1);
                int b = ii.get(yi, xi, 0);
                accum[0] += r;
                accum[1] += g;
                accum[2] += b;
                numPixels++;
            }
        }
        accum[0] /= numPixels;
        accum[1] /= numPixels;
        accum[2] /= numPixels;
        return new Color((int) accum[0], (int) accum[1], (int) accum[2]);
    }
    
    public double calcDistance(Color[][] reference, Color[][] source) {
        // Calculate the signature for that image.

        // There are several ways to calculate distances between two vectors,
        // we will calculate the sum of the distances between the RGB values of
        // pixels in the same positions.
        double dist = 0;
        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                int r1 = reference[x][y].getRed();
                int g1 = reference[x][y].getGreen();
                int b1 = reference[x][y].getBlue();
                int r2 = source[x][y].getRed();
                int g2 = source[x][y].getGreen();
                int b2 = source[x][y].getBlue();
                double tempDist = Math.sqrt((r1 - r2) * (r1 - r2) + (g1 - g2) * (g1 - g2) + (b1 - b2) * (b1 - b2));
                dist += tempDist;
            }
        }
        return dist;
    }
    
    public static void main(String[] args) {
        long start=System.currentTimeMillis();
        String fileName = "D:\\work\\project\\RawVideoSummarization\\dataset\\video-frames\\1558086459036.avi";
        videoChangeDetector(fileName);
        long end=System.currentTimeMillis();
        System.out.println(" Time "+(end-start)+" MS");
    }
    
    public static void videoChangeDetector(String filePath) {
        
        OpenCVImageCompareSqrDiff sqdiff = new OpenCVImageCompareSqrDiff();
        StringBuffer result = new StringBuffer();
        File videoFrameDir = new File(filePath);
        if (videoFrameDir.exists() && videoFrameDir.isDirectory()) {
            File file[] = videoFrameDir.listFiles();
            
            if (file != null) {
                try {
                    java.util.Arrays.sort(file, new java.util.Comparator<File>() {
                        
                        public int compare(File f1, File f2) {
                            return Long.compare(f1.lastModified(), f2.lastModified());
                        }
                    });
                    Color[][] image1 = sqdiff.calcSignature(file[0].getAbsolutePath());
                    for (int i = 1; i < file.length; i++) {
                        Color[][] image2 = sqdiff.calcSignature(file[i].getAbsolutePath());
                        double d = sqdiff.calcDistance(image1, image2);
//                         System.out.println("==> distance " + d + " " + file[i].getName());
                        int threshold = 200;
                        if (d > threshold) {
                            System.out.println(" distance " + d + " " + file[i].getName());
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
        
    }
    public static class Color {
        private int red;
        private int green;
        private int blue;
        public  Color(int red,int green,int blue ){
            this.setRed(red);
            this.setGreen(green);
            this.setBlue(blue);
        }

        public int getRed() {
            return red;
        }

        public void setRed(int red) {
            this.red = red;
        }

        public int getGreen() {
            return green;
        }

        public void setGreen(int green) {
            this.green = green;
        }

        public int getBlue() {
            return blue;
        }

        public void setBlue(int blue) {
            this.blue = blue;
        }
    }
}
