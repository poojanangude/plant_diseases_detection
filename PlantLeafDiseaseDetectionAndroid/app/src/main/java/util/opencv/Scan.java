package util.opencv;

import static org.bytedeco.javacpp.opencv_core.CV_8UC1;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvSaveImage;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.cvDrawRect;
import static org.bytedeco.javacpp.opencv_imgproc.erode;
import static org.bytedeco.javacpp.opencv_imgproc.putText;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import org.bytedeco.javacpp.opencv_core.CvMat;
import org.bytedeco.javacpp.opencv_core.CvPoint;
import org.bytedeco.javacpp.opencv_core.CvScalar;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.indexer.*;

public class Scan {
	boolean saveImage = true;
	boolean showSysout = false;
	public String fileName = "";
	public String EXT_SD_PATH = "database/";
	public int questions = 1;
	public int answered = 0;
	public	HashMap answersMap =new HashMap();
	// D:\work\project\Test\Test\database\cnt_11515140865886_tempomr.png.jpg
	public static void main22(String[] args) {
		String s = "database\\OMR_tempOMR_Orginal_1515502596323.png.jpg"; // D:\work\project\Test\Test\database
		IplImage img = OpenCVHelper.file2ipl(s);
		Scan scan = new Scan();
		scan.processBubbleOld(img);
	}

	public static void main(String[] args) {
		// String s = "dataset\\cnt1.jpg";

		Scan scan = new Scan();
		scan.showSysout = true;
		String s = "database\\OMR_tempOMR_Orginal_1515565995403.png.jpg";
		scan.fileName=new File(s).getName();
		Mat input = imread(s, CV_8UC1);
		// threshold(input, input, 127, 255, THRESH_BINARY);
		// erode(input, input, new Mat());
		// imshow("dilate", input);
		// waitKey();
		// scan.writeMatCSV(input);
		// waitKey();
		long start = System.currentTimeMillis();

		// imshow("OP", omr);
		// cvWaitKey();
		// IplImage img = OpenCVHelper.file2ipl(s);
		scan.processBubble(input);
		long end = System.currentTimeMillis();
		System.out.println("Time Of scan " + (end - start));
	}

	public void writeMatCSV(Mat mat) {
		UByteIndexer imagePixels = mat.createIndexer();
		StringBuffer sb = new StringBuffer();
		for (int row = 0; row < mat.rows(); row++) {
			System.out.print(row + ",");
			for (int col = 0; col < mat.cols(); col++) {
				// if(mat.get(row, col)!=255&&mat.get(row, col)!=0)
				sb.append(imagePixels.get(row,col) + ",");
			}
			sb.append("\n");
		}
		try {
			new FileOutputStream("test.csv").write(sb.toString().getBytes());
			sb=null;
			imagePixels.release();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public StringBuffer answerString = null;

	public void processBubble(Mat mat) {
		answerString = new StringBuffer();
		// int[] horizontalScan = new int[mat.rows()];
		// int[] verticalScan = new int[mat.cols()];
//		writeMatCSV(mat);
		int startXA[] = new int[] { 35, 205, 372, 538, 705, 875 };
		int initialStartX = 35;
		int startX = 35, starty = 55;
		int boxWidth = 120, boxHeight = 320;
		int noOfCols = 5, noOfRows = 15;
		int margin = 50;
		int[][][] dataPoints = new int[6][noOfRows][noOfCols];
		int[] avgNonMarkedDataPoints = new int[6];
		boolean[][][] dataPointsCenterMarked = new boolean[6][noOfRows][noOfCols];
		int[] whitePoints = new int[mat.cols()];
		UByteIndexer imagePixels2 = mat.createIndexer();
//		UByteRawIndexer imagePixels2 = mat.createIndexer();
		for (int col = 0; col < mat.cols(); col++) {
			for (int row = startX; row < (startX + boxHeight); row++) {
				int pix = imagePixels2.get(row, col);
				if (pix >= 20) {
					whitePoints[col]++;
				}
			}
		}
		if(showSysout)
			System.out.println("\n\n whitePoints Values "+Arrays.toString(whitePoints));
		imagePixels2.release();
		if(showSysout)
			System.out.println("\n\nOld StartXA Values "+Arrays.toString(startXA));
		for (int col = 0; col < startXA.length; col++) {
			int start=startXA[col];
			int end=start-12;
			int startVal=start;
			for (int i = start; i > end; i--){
				if(whitePoints[i]<startVal){
					startXA[col]=i;
					startVal=whitePoints[i];
				}
			}

		}
		if(showSysout)
			System.out.println("\n\nNew StartXA Values "+Arrays.toString(startXA));


//		System.out.println("\nPoints \n\n");
//		for (int col = 0; col < mat.cols(); col++) {
//			System.out.print(whitePoints[col]+",");
//		}


		erode(mat, mat, new Mat());
		erode(mat, mat, new Mat());
		if (saveImage) {
			// imwrite(EXT_SD_PATH+"/5_OMR.jpg", mat);
			imwrite(EXT_SD_PATH + "/Erode" + fileName + "_" + answered + "_.jpg", mat);

		}

		Mat newMat = mat.clone();
		UByteIndexer imagePixels = mat.createIndexer();
//		UByteRawIndexer imagePixels = mat.createIndexer();
		if (showSysout) {
			System.out.println("mat.cols() " + mat.cols() + " " + mat.rows() + " " + mat.channels());
		}


		System.out.println("\n");
		for (int k = 0; k < 6; k++) {
			// if(k==2){
			// boxWidth=110;
			// margin=60;
			// }
			// if(k==3){
			// boxWidth=110;
			// margin=50;
			// }
			startX = startXA[k];
			int smallW = boxWidth / noOfCols, smallHeight = boxHeight / noOfRows;
			int totalSumNonMarked = 0;
			int countNosOfNonMarked = 0;
			for (int j = 0; j < noOfRows; j++) {
				String data = "";
				String ans = "";
				int i = 0;
				// System.out.println("============================");
				for (i = 0; i < noOfCols; i++) {

					// cvDrawRect(img, new CvPoint(35,50), new
					// CvPoint(smallW,smallHeight), CvScalar.WHITE, 2, 8,
					// 0);
					Point start = new Point((startX + (i * smallW)), (starty + (j * smallHeight)));
					// Point end = new Point(start.x() + smallW, start.y() +
					// smallHeight);
					int center_row = (int) (start.y() + smallHeight / 2);
					int center_col = (start.x() + smallW / 2) - 1;
					int cntWhite = 0;

					for (int l = 0; l < smallHeight; l++) {
						for (int l2 = 0; l2 < smallW; l2++) {
							int nrow = (int) (start.y() + l);
							int ncol = (start.x() + l2);
							int pix = imagePixels.get(nrow, ncol);
							if (pix >= 20) {
								cntWhite++;
							}
						}
					}
					int pix = imagePixels.get(center_row, center_col);
					// boolean centerWhite = false;
					if (pix >= 20) {
						// centerWhite = true;
						dataPointsCenterMarked[k][j][i] = true;
					} else {
						dataPointsCenterMarked[k][j][i] = false;
						countNosOfNonMarked++;
						totalSumNonMarked += cntWhite;
					}

					dataPoints[k][j][i] = cntWhite;

					// if (cntWhite >= 200 || (centerWhite&&cntWhite>20)) {
					//
					// // System.out.println(((char) ('A' + i))+" "+cntWhite);
					// // data += "" + j + "_" + i + "=" + pix + ",";
					// ans += ((char) ('A' + i)) + "_" + cntWhite + ",";
					// // circle(newMat, new Point(center_row, center_col), 2,
					// Scalar.GREEN);
					// putText(newMat, ""+((char)('A' + i)), new
					// Point(center_col+5,center_row), 1,0.8,Scalar.WHITE);
					// }
					//
					// rectangle(newMat, start, end, Scalar.WHITE, 1, 8, 0);
				}
				// System.out.println("============================");
				// System.out.println("===================================questions "+questions);
				// if (ans.length() > 0) {
				// answered++;
				// answerString.append("Question " + questions + "=>" + ans
				// +"\n");
				// }
				// if (showSysout)
				// System.out.println("Question " + questions + " " + ans + " "
				// + j + "=>" + data);
				// questions++;
			}
			avgNonMarkedDataPoints[k] = totalSumNonMarked / countNosOfNonMarked;
		}

		int LIMIT_ABOVE_MIN_BLANK = 28;
		for (int k = 0; k < 6; k++) {
			startX = startXA[k];
			int smallW = boxWidth / noOfCols, smallHeight = boxHeight / noOfRows;
			int totalSumNonMarked = 0;
			int countNosOfNonMarked = 0;

			for (int j = 0; j < noOfRows; j++) {
				String data = "";
				String ans = "";
				String unrecognized = "";
				int i = 0;
				for (i = 0; i < noOfCols; i++) {
					Point start = new Point((startX + (i * smallW)), (starty + (j * smallHeight)));
					Point end = new Point(start.x() + smallW, start.y() + smallHeight);
					int center_row = (int) (start.y() + smallHeight / 2);
					int center_col = (start.x() + smallW / 2) - 1;

					int cntWhite = dataPoints[k][j][i];

					if (cntWhite >= (avgNonMarkedDataPoints[k] + LIMIT_ABOVE_MIN_BLANK)
							|| (dataPointsCenterMarked[k][j][i] && cntWhite >= (avgNonMarkedDataPoints[k] + LIMIT_ABOVE_MIN_BLANK))) {

						// System.out.println(((char) ('A' + i))+" "+cntWhite);
						// data += "" + j + "_" + i + "=" + pix + ",";
						ans += ((char) ('A' + i))+ ",";
						// circle(newMat, new Point(center_row, center_col), 2,
						// Scalar.GREEN);
						putText(newMat, "" + ((char) ('A' + i)), new Point(center_col + 5, center_row), 1, 0.8, Scalar.WHITE);
					} else {
						unrecognized += ",N" + ((char) ('A' + i)) + "-_" + cntWhite + ",";
					}

					rectangle(newMat, start, end, Scalar.WHITE, 1, 8, 0);

				}
				if (ans.length() > 0) {
					if(ans.length()>1){
						ans=ans.substring(0,ans.length()-1);
						ans=ans.toLowerCase();
					}
					answersMap.put(questions, ans);
					answered++;
					answerString.append("Question " + questions + "=>" + ans + "\n");
				}
				if (showSysout)
					System.out.println("Question " + questions + " " + ans + " " + j + "<==>" + data + " " + "<==>unrecognized="
							+ unrecognized + "<==> AVG= " + avgNonMarkedDataPoints[k]);
				questions++;

			}
		}

		questions = questions - 1;
		if (showSysout)
			System.out.println("Question " + questions + " Summary " + answered + " unaswered " + (questions - answered));
		answerString.append("Question " + questions + " Summary " + answered + " unaswered " + (questions - answered) + "\n");
		if (saveImage) {
			// imwrite(EXT_SD_PATH+"/5_OMR.jpg", mat);
			imwrite(EXT_SD_PATH + "/5_OMR_Rects" + fileName + "_" + answered + "_.jpg", newMat);

		}
		imagePixels.release();
		newMat.release();

	}

	public static void processBubbleOld(IplImage img) {

		System.out.println(img.nChannels());
		CvMat mat = img.asCvMat();
		System.out.println(mat.get(0, 0) + " BINARY DATA " + mat.get(0, 1));
		int[] horizontalScan = new int[mat.height()];
		int[] verticalScan = new int[mat.width()];
		for (int row = 0; row < mat.height(); row++) {
			for (int col = 0; col < mat.width(); col++) {
				// if(mat.get(row, col)!=255&&mat.get(row, col)!=0)
				System.out.print(mat.get(row, col) + ",");
				if (mat.get(row, col) >= 20) {
					horizontalScan[row]++;
					verticalScan[col]++;
				}
			}
			System.out.println();
			if (true) {
				return;
			}
		}

		System.out.println("\n\n");
		for (int i = 0; i < horizontalScan.length; i++) {
			System.out.print(horizontalScan[i] + ",");
		}
		System.out.println("\n\n");
		for (int i = 0; i < verticalScan.length; i++) {
			System.out.print(verticalScan[i] + ",");
		}
		System.out.println("\n\n");
		int startXA[] = new int[] { 35, 205, 372, 538, 705, 875 };
		int initialStartX = 35;
		int startX = 35, starty = 55;
		int boxWidth = 120, boxHeight = 320;
		int noOfCols = 5, noOfRows = 15;
		int margin = 50;
		int Q = 1;
		for (int k = 0; k < 6; k++) {
			// if(k==2){
			// boxWidth=110;
			// margin=60;
			// }
			// if(k==3){
			// boxWidth=110;
			// margin=50;
			// }
			startX = startXA[k];
			int smallW = boxWidth / noOfCols, smallHeight = boxHeight / noOfRows;

			for (int j = 0; j < noOfRows; j++) {

				String ans = "";
				for (int i = 0; i < noOfCols; i++) {

					// cvDrawRect(img, new CvPoint(35,50), new
					// CvPoint(smallW,smallHeight), CvScalar.WHITE, 2, 8,
					// 0);
					CvPoint start = new CvPoint((startX + (i * smallW)), (starty + (j * smallHeight)));
					CvPoint end = new CvPoint(start.x() + smallW, start.y() + smallHeight);

					// System.out.println((start.x()+(smallW/2))+" "+(start.y()+smallHeight/2));
					if (mat.get((start.y() + smallHeight / 2), (start.x() + smallW / 2)) >= 20) {
						ans += ((char) ('A' + i)) + ",";
					}
					cvDrawRect(img, start, end, CvScalar.WHITE, 1, 8, 0);
				}

				// System.out.println("Question " + Q + " " + ans);
				Q++;
			}
		}
		cvSaveImage("5_OMR.jpg", img);
		// if (showWindow) {
		// cvShowImage("affine transfo", img);
		// cvWaitKey();
		// }

		// for(int i=0; i<img.; i++){
		// for(int j=0; j<img.cols; j++){
		//
		// }
		// }

	}
	public void clear(){
		if(answersMap !=null){
			answersMap.clear();
		}
		if(answerString!=null){
			answerString=null;
		}

	}
}
