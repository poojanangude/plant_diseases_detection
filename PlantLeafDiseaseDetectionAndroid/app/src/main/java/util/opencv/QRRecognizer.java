package util.opencv;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.AndroidFrameConverter;
import org.bytedeco.javacv.Frame;

public class QRRecognizer {
	boolean showSysout=false;
	public static final String LOG_TAG = "CvCameraPreview";
	public String readQRImage(Mat QRCode) {

	//	BitmapFactory.decodeFile(dir.getAbsolutePath() + "/cnt_2" + fileName + ".jpg");
		AndroidFrameConverter converterToBitmap = new AndroidFrameConverter();
		Frame qrframe = OpenCVHelper.mat2frame(QRCode);
		Bitmap qt = converterToBitmap.convert(qrframe);
		String qrText = readQRImageAndroid(qt);
		if (qrframe != null) {
			qrframe = null;

		}
		if (qt != null) {
			qt.recycle();
		}
		return qrText;

	}
	
	  public String readQRImageAndroid(Bitmap bMap) {
	        String contents = null;
	        try {
	            print(" Trying to read QR Code" + bMap.getWidth() + " " +
	                    bMap.getHeight() + " " + bMap + " " + bMap.getConfig());
	            int[] intArray = new int[bMap.getWidth() * bMap.getHeight()];
	            //copy pixel data from the Bitmap into the 'intArray' array

	            bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(),
	                    bMap.getHeight());
	            print("QR Code Read Image " + intArray[0] + " " + intArray[1]);
	            LuminanceSource source = new
	                    com.google.zxing.RGBLuminanceSource(bMap.getWidth(), bMap.getHeight(),
	                    intArray);

	            com.google.zxing.BinaryBitmap bitmap = new
	                    com.google.zxing.BinaryBitmap(new HybridBinarizer(source));
	            print("QR Code BinaryBitmap");
	            MultiFormatReader reader = new MultiFormatReader();// use this otherwise
	            Result result = reader.decode(bitmap);
	            print(" QR Code Text " + result);
	            contents = result.getText();
	            intArray = null;
	            bitmap = null;
	            reader = null;
	            //byte[] rawBytes = result.getRawBytes();
	            //BarcodeFormat format = result.getBarcodeFormat();
	            //ResultPoint[] points = result.getResultPoints();
	        } catch (Exception e) {
	            e.printStackTrace();
	            android.util.Log.e(LOG_TAG, "Eeror", e);
	            print(" QR Code Exception " + e.getMessage());
	        }
	        return contents;
	    }

//  public String readQRImagePC(BufferedImage image) {
//
//      System.out.println(image.getType());
//      LuminanceSource source = new BufferedImageLuminanceSource(image);
//      BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//
//      try {
//          Result result = new MultiFormatReader().decode(bitmap);
//          System.out.println(result.getText());
//      } catch (NotFoundException e) {
//          // fall thru, it means there is no QR code in image
//          e.printStackTrace();
//      }
//      return "";
//  }
public void print(String message) {
	if (showSysout) {
		System.out.println(message);
	}
	// android.util.Log.i(LOG_TAG, message);
}
}
