package backup;

//import android.graphics.Bitmap;
//import android.os.Environment;
//import android.util.Log;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_core.CV_8UC1;
import static org.bytedeco.javacpp.opencv_core.meanStdDev;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_CHAIN_APPROX_SIMPLE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_EXTERNAL;
import static org.bytedeco.javacpp.opencv_imgproc.Laplacian;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY_INV;
import static org.bytedeco.javacpp.opencv_imgproc.approxPolyDP;
import static org.bytedeco.javacpp.opencv_imgproc.arcLength;
import static org.bytedeco.javacpp.opencv_imgproc.boundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.getPerspectiveTransform;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bytedeco.javacpp.FloatPointer;
import org.bytedeco.javacpp.opencv_core.CvRect;
import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_imgproc.CLAHE;

import util.opencv.Point;
import util.opencv.Polygon;
import util.opencv.QRRecognizer;
import util.opencv.Scan;

public class OMRTestPhone {

    public Mat grabbedImage = null;
    public Mat QRCodeImage = null;
    public Mat omrImage = null;
    public ArrayList<Polygon> fourCorners = null;
    public Scan scan = new Scan();
    boolean showWindow = false;
    boolean saveImage = true;
    boolean showSysout = true;
    boolean drawColor = true;
    double dSharpness = 0;
    String qrText = "";
    String fileName = "";
    int[][] cornerBoxes = null;
    boolean OMRSheetDetected = false;
    public int currentIndexFound=-1;
    ArrayList<CvRect> arrCorners = new ArrayList<CvRect>();
    ArrayList<CvRect> arrRects = new ArrayList<CvRect>();
    ArrayList<CvSeq> arrOthers = new ArrayList<CvSeq>();
    public int cx = 0, cy = 0, cwidth = 0, cheight = 0;
    boolean isBlurred = false;
    public static final String LOG_TAG = "CvCameraPreview";
    int cornerPoints = 0;
    public static void main(String[] args) {

        // try {
        // System.out.println(new OMRTestPhone().readQRImage(ImageIO.read(new
        // File("D:\\work\\project\\Test\\Test\\database\\cnt_21515072036113_tempomr.png.jpg"))));
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // E:\Downloads\Images 2
        String dir = "E:\\Downloads\\Files_downloaded_by_AirDroid_30\\a.png";// tempOMR_Orginal_1515502631815.png
        // String dir = "E:\\Downloads\\marcio-images\\data\\";//
        // 1515140841541_tempomr.png
        // String dir = "E:\\Downloads\\data\\";////
        // dataset\\samples\\1515162619821_tempomr.png

        File f = new File(dir);
        if (f.isDirectory()) {
            File[] files = f.listFiles();
            for (int safas = 0; safas < files.length; safas++) {

                // String s = "dataset\\1514878145118_tempomr.png";
                // String s = "dataset\\1515011886658_tempomr.png";
                File currentImage = files[safas];
                if (currentImage.isFile()) {
                    extractOMR(currentImage);
                    // waitKey();
                }
            }
        } else {
            extractOMR(f);
        }
    }



    boolean isBlurredImage(final Mat matImageGray) {
        // Rect cvRect2 = new Rect(matImageGray.cols()/4,0,
        // matImageGray.cols()/2, matImageGray.rows()/2);
        // Mat matImageGrayROI = new Mat(grabbedImage, cvRect2);
        boolean b = false;
        long start = System.currentTimeMillis();
        Mat laplacianImage = new Mat(matImageGray.rows(), matImageGray.cols(), matImageGray.type());
        Canny(matImageGray, laplacianImage, 225.0, 175.0);
        int nCountCanny = countNonZero(laplacianImage);
        dSharpness = (nCountCanny * 1000.0 / (laplacianImage.cols() * laplacianImage.rows()));
        long end = System.currentTimeMillis();
        // System.out.println(dSharpness);
        if (dSharpness <= 30) {
            b = true;
        }

        laplacianImage.release();
        // matImageGrayROI.release();
        // cvRect2.deallocate();
        // System.out.println("Image " + fileName + " Blurness ["+b+"] " +
        // dSharpness + " " + (end - start));

        return b;

    }



    private static void extractOMR(File currentImage) {
        String s = currentImage.getAbsolutePath();
        OMRTestPhone omr = new OMRTestPhone();
        omr.grabbedImage = imread(s);

        long startMemory = Runtime.getRuntime().freeMemory();

        omr.fourCorners = new ArrayList<Polygon>();

        long start = System.currentTimeMillis();
        // 0 109 1920 864
        // omr.cx = 0;
        // omr.cy = 615;
        // omr.cwidth = 1080;
        // omr.cheight = 691;192 107 1536 863
        omr.cx = 192;
        omr.cy = 107;
        omr.cwidth = 1536;
        omr.cheight = 863;
        // int[][] a = new int[][] { { 864, 3, 222, 271 }, { 3, 3, 224, 271
        // }, { 0, 416, 224, 275 }, { 864, 416, 224, 275 } };
        // TOP-LEFT TOP-RIGHT BOTTOM-LEFT BOTTOM-RIGHT
        omr.cornerBoxes = new int[][] { { 192 - omr.cx, 0, 308, 258 }, { 1440 - omr.cx, 0, 308, 258 }, { 192 - omr.cx, 602, 308, 260 },
                { 1440 - omr.cx, 602, 308, 260 } };
        // 864 3 222 271
        // 0 416 224 275
        // 864 416 224 275
        for (int i = 0; i < omr.cornerBoxes.length; i++) {
            int x = omr.cornerBoxes[i][0], y = omr.cornerBoxes[i][1], width = omr.cornerBoxes[i][2], height = omr.cornerBoxes[i][3];
            Polygon left = new Polygon.Builder().addVertex(new Point(x, y)).addVertex(new Point(x + width, y))
                    .addVertex(new Point(x + width, y + height)).addVertex(new Point(x, y + height)).build();

            omr.print(LOG_TAG + " Corner Points  " + x + " " + y + " " + width + " " + height);
            omr.fourCorners.add(left);

        }

        omr.fileName = currentImage.getName();
        boolean b = omr.checkValidOMR();
        omr.print("OMR_DETECTED " + b + "");
        long end = System.currentTimeMillis();

        long endMemory = Runtime.getRuntime().freeMemory();
        long total = startMemory - endMemory;
        if (omr.OMRSheetDetected)
            System.out.println("Image " + currentImage.getName() + "  [OMR Detected " + omr.OMRSheetDetected + "] ==> "
                    + omr.scan.questions + "/" + omr.scan.answered + " isBlurred [" + omr.isBlurred + "=" + omr.dSharpness + "] Corners -"
                    + omr.cornerPoints + " QR -" + omr.qrText + "  Time -[" + (end - start) + "]");
        else {
            System.out.println("SKIPPING Image " + currentImage.getName() + "  [OMR Detected " + omr.OMRSheetDetected + "] ==> "
                    + omr.scan.questions + "/" + omr.scan.answered + " isBlurred [" + omr.isBlurred + "=" + omr.dSharpness + "] Corners -"
                    + omr.cornerPoints + " QR -" + omr.qrText + " Time -[" + (end - start) + "]");
        }
        omr.print("Used Bytes " + total);
        omr.print("------------------------" + (end - start) + " MS-----------------------------");
        omr.print("Time of run " + (end - start));
        omr.grabbedImage.release();
    }

    public static int countGreen( String  grabbedImageFilePath ){
        Mat grabbedImage = imread(grabbedImageFilePath);
//        imshow("SHOWWINDOW2", grabbedImage);
        cvtColor(grabbedImage, grabbedImage, COLOR_BGR2HSV);
        Mat mask = grabbedImage.clone();
        inRange(grabbedImage, new Mat(new int[] { 40, 60, 60 }), new Mat(
                new int[] { 80, 255, 255 }), mask);
        double area=grabbedImage.arrayWidth()*grabbedImage.arrayHeight();
        double whiteCnt=countNonZero(mask);
        double per=whiteCnt/area;
        per=per*100;
        System.out.println("whiteCnt "+whiteCnt+" ==> "+per);
        return (int) per;
     //   imshow("SHOWWINDOW", mask);
    }

    public boolean checkValidOMR() {
        // File dir = new
        // File(android.os.Environment.getExternalStorageDirectory().toString()
        // + "/Database");
        File dir = new File("./Database");
        try {
            System.out.println(dir.getCanonicalPath());
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        if (!dir.exists()) {
            dir.mkdirs();
        }

        boolean success = false;
        try {

            Rect cvRect2 = new Rect(cx, cy, cwidth, cheight);
            // Rect cvRect2 = new Rect(0, 0, 100, 120);
            // rectangle(grabbedImage, cvRect2, Scalar.BLUE);

            // System.out.println(fileName);

            Mat grabbedImageRoi = null;
            try {
                grabbedImageRoi = new Mat(grabbedImage, cvRect2);
            } catch (Exception e) {
                print("Error! Image Seems to be of diffren size than expected ROI");
                e.printStackTrace();
                success = false;
                return success;
            }

            // imshow("Test",grabbedImage);
            // imwrite("test.png", grabbedImage);
            // cvWaitKey();
            // imw
            // cvSetImageROI(grabbedImage, cvRect2);
            // Mat localImage = grabbedImage.clone();
            // Mat local = new Mat(localImage, cvRect2);
            if (saveImage) {
                imwrite(dir.getAbsolutePath() + "/original_" + fileName + ".jpg", grabbedImage);
                // imwrite(dir.getAbsolutePath() + "/1_grab.jpg",
                // grabbedImageRoi);
            }
            Mat GrayImage = new Mat(grabbedImageRoi.rows(), grabbedImageRoi.cols(), 1);
            //
            cvtColor(grabbedImageRoi, GrayImage, CV_BGR2GRAY);
            isBlurred = isBlurredImage(GrayImage);
            if (isBlurred) {
                print("BLURRED Image===============================" + fileName);
                GrayImage.release();
                grabbedImageRoi.release();
                cvRect2.deallocate();
                return false;
            }

            // Mat histMat = new Mat(grabbedImageRoi.rows(),
            // grabbedImageRoi.cols(), CV_8UC1);

            // histMat.release();
            if (saveImage) {

                imwrite(dir.getAbsolutePath() + "/2_gray.jpg", GrayImage);
            }
            // CLAHE c= createCLAHE(2.0, new Size(4,4));
            // c.apply(GrayImage, GrayImage);

            // if (saveImage) {
            // imwrite("./Database/histCLAHE_"+fileName,GrayImage);
            // // imwrite(dir.getAbsolutePath() + "/3_smooth.jpg", GrayImage);
            // }

            // if(true){
            GaussianBlur(GrayImage, GrayImage, new Size(11, 11), 0);
            adaptiveThreshold(GrayImage, GrayImage, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 11, 2);
            // }else{
            // threshold(GrayImage, GrayImage, 0, 255, THRESH_OTSU |
            // THRESH_BINARY_INV);
            // }
            if (saveImage) {
                imwrite(dir.getAbsolutePath() + "/4_threshold" + fileName + ".jpg", GrayImage);
            }
            MatVector contours = new MatVector();
            Mat contourImage = GrayImage.clone();
            findContours(contourImage, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE); // CV_RETR_EXTERNAL
            // CV_RETR_LIST
            // imwrite("after_contour.jpg", GrayImage);
            // //CV_CHAIN_APPROX_SIMPLE
            ArrayList<Rect> arrRects = new ArrayList<Rect>();
            ArrayList<Double> arrRectsArea = new ArrayList<Double>();
            ArrayList<ArrayList<Integer>> arrOthers = new ArrayList<ArrayList<Integer>>();
            int i = 1;
            int width = grabbedImageRoi.cols();
            int height = grabbedImageRoi.rows();
            print("width  height in " + width + " " + height);
            int width5per = width * 5 / 100;
            int height5per = height * 5 / 100;
            int width70per = width * 70 / 100;
            int height80per = height * 80 / 100;
            int areaMarkerBulletMin = (int) ((width * 0.01f) * (height * 0.03f));
            int areaMarkerBulletMax = (int) ((width * 0.06f) * (height * 0.1f));
            int area5PerImage = width5per * height5per;
            print("areaMarkerBullet " + areaMarkerBulletMax);
            print("area5PerImage " + area5PerImage);
            Polygon.Builder four_corners_polygon = Polygon.Builder();
            ArrayList<Integer> arrCornerXY = new ArrayList<Integer>();
            ArrayList<Boolean> countCornerXY = new ArrayList<Boolean>();

            for (int contourIndex = 0; contourIndex < contours.size(); contourIndex++) {
                Mat currentContour = contours.get(contourIndex);
                if (currentContour != null) {

                    Rect boundRect = boundingRect(currentContour);
                    int cx = boundRect.x();
                    int cy = boundRect.y();
                    int contour_width = boundRect.width();
                    int contour_height = boundRect.height();
                    double widthHeightRatio = (contour_width * 1.0) / contour_height;
                    double areaOfContour = (contour_width * contour_height);

                    if (areaOfContour > areaMarkerBulletMin) {

                        Mat PolyXYPoints = new Mat();
                        approxPolyDP(currentContour, PolyXYPoints, arcLength(currentContour, true) * 0.02, true);
                        int noOfContourPoints = PolyXYPoints.rows();
                        // print("vectorPoints.size() " +
                        // noOfContourPoints);

                        if (noOfContourPoints >= 4) {
                            if (showSysout) {
                                print("Contour[" + contourIndex + "] Cx =" + cx + " Cy=" + cy + " CWidth=" + contour_width + " CHeight="
                                        + contour_height + " WidthHeightRatio " + widthHeightRatio + " Area " + areaOfContour + " Max "
                                        + areaMarkerBulletMax + " Min " + areaMarkerBulletMin + " Points " + noOfContourPoints);
                            }
                            // if (drawColor) {
                            // rectangle(grabbedImageRoi, new
                            // org.bytedeco.javacpp.opencv_core.Point(boundRect.x(),
                            // boundRect.y()),
                            // new
                            // org.bytedeco.javacpp.opencv_core.Point(boundRect.x()
                            // + boundRect.width(), boundRect.y()
                            // + boundRect.height()), Scalar.GREEN, 1, 8, 0);
                            // }
                            ArrayList<Integer> pointsArray = getContourPoints(PolyXYPoints);

                            if (pointsArray.size() >= (4 * 2)) {
                                if (widthHeightRatio >= 0.57 && widthHeightRatio <= 0.80 && areaOfContour < areaMarkerBulletMax) {

                                    boolean found = checkIfWithinCorners(pointsArray);
                                    print("MayBe a corner Point Checking if within Boundary - Found in Boxes " + found + " nonzero ");
                                    if (found) {
                                        Mat c = new Mat(GrayImage, boundRect);

                                        // int nonzero=0;
                                        int nonzero = countNonZero(c);
                                        int contour_75_Per=(int) (areaOfContour * 70.0 / 100);
                                        c.release();
                                        boolean b = nonzero > contour_75_Per;
                                        if (b) {
                                            cornerPoints++;
                                            print("Found Corner Point Corners FOund till now - " + cornerPoints + " nonZero " + nonzero);
                                            // four_corners_polygon.addVertex(new
                                            // util.opencv.Point(cx, cy));
                                            arrCornerXY.add(cx);
                                            arrCornerXY.add(cy);
                                        } else {
                                            print("Found Corner Point Corners FOund BUT BLACK COUNT IN LOW  nonZero75Per " + nonzero+" should be >  contour_75_Per "+contour_75_Per);

                                        }
                                        if (drawColor) {
                                            rectangle(
                                                    grabbedImageRoi,
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x(), boundRect.y()),
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x() + boundRect.width(), boundRect
                                                            .y() + boundRect.height()), Scalar.GREEN, 1, 8, 0);
                                        }
                                    } else {
                                        if (drawColor) {
                                            rectangle(
                                                    grabbedImageRoi,
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x(), boundRect.y()),
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x() + boundRect.width(), boundRect
                                                            .y() + boundRect.height()), Scalar.RED, 1, 8, 0);
                                        }
                                    }
                                    pointsArray.clear();
                                    pointsArray = null;
                                } else {
                                    if (contour_width > width70per) { // Main
                                        // OMR
                                        if (drawColor) {
                                            rectangle(
                                                    grabbedImageRoi,
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x(), boundRect.y()),
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x() + boundRect.width(), boundRect
                                                            .y() + boundRect.height()), Scalar.YELLOW, 1, 8, 0);
                                        } // Sheet
                                        print("Found OMR- Adding CurrentContour to others " + pointsArray);
                                        arrRects.add(boundRect);
                                        arrOthers.add(pointsArray);
                                        arrRectsArea.add(areaOfContour);
                                        // if (drawColor) {
                                        // rectangle(
                                        // grabbedImageRoi,
                                        // new
                                        // org.bytedeco.javacpp.opencv_core.Point(boundRect.x(),
                                        // boundRect.y()),
                                        // new
                                        // org.bytedeco.javacpp.opencv_core.Point(boundRect.x()
                                        // + boundRect.width(), boundRect
                                        // .y() + boundRect.height()),
                                        // Scalar.RED, 1, 8, 0);
                                        // }
                                    } else if (widthHeightRatio >= 0.80 && widthHeightRatio <= 1.10 && contour_width > width5per
                                            && contour_height > width5per) { // Main
                                        // QR
                                        // Code
                                        arrRects.add(boundRect);
                                        arrOthers.add(pointsArray);
                                        arrRectsArea.add(areaOfContour);
                                        print("Found QRCode- Adding CurrentContour to others");
                                        if (drawColor) {
                                            rectangle(
                                                    grabbedImageRoi,
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x(), boundRect.y()),
                                                    new org.bytedeco.javacpp.opencv_core.Point(boundRect.x() + boundRect.width(), boundRect
                                                            .y() + boundRect.height()), Scalar.MAGENTA, 1, 8, 0);
                                        }
                                    }
                                }
                            }
                        }
                        PolyXYPoints.release();
                    }
                    currentContour.release();

                }

            }
            contourImage.release();
            contourImage.deallocateReferences();
            contours.deallocateReferences();
            contours.deallocate();
            print("Number of corners " + cornerPoints);
            if (cornerPoints >= 4) { // 4 corner points

                // Rect cornerDummyRect = new Rect(0, 0, cwidth, cheight);

                arrCornerXY = approximateRectanglePoints(arrCornerXY);

                boolean IsAngleBetweenCornersOkay = checkAngle(arrCornerXY);

                if (arrOthers.size() >= 2 && IsAngleBetweenCornersOkay) {
                    currentIndexFound=5;
                    for (int xy = 0; xy < arrCornerXY.size(); xy = xy + 2) {
                        four_corners_polygon.addVertex(new Point(arrCornerXY.get(xy), arrCornerXY.get(xy + 1)));
                        circle(grabbedImageRoi, new org.bytedeco.javacpp.opencv_core.Point(arrCornerXY.get(xy), arrCornerXY.get(xy + 1)),
                                10, Scalar.RED);

                    }
                    //
                    print("arrOthers.size() " + arrOthers.size());
                    Polygon polygon = four_corners_polygon.build();
                    for (int index = 0; index < arrOthers.size(); index++) {
                        ArrayList<Integer> bigContourPoints = (ArrayList<Integer>) arrOthers.get(index);
                        Rect rects = (Rect) arrRects.get(index);
                        boolean b = true;
                        for (int j = 0; j < bigContourPoints.size(); j = j + 2) {
                            int x = bigContourPoints.get(j);
                            int y = bigContourPoints.get(j + 1);
                            b = b & polygon.contains(new Point(x, y));

                        }
                        if (showSysout) {
                            if (rects != null)
                                print("Contour Inside Corner points X Y W H " + rects.x() + " " + rects.y() + " " + rects.width() + " "
                                        + rects.height());
                        }
                        if (b) {
                            if (rects.width() > width70per) {
                                if (showSysout) {
                                    print("OMR Answer Sheet detected");
                                }

                                // Start
                                Mat omrNewResized = new Mat(380, 1000, GrayImage.type());

                                int widthOmr = 1000;
                                int heightOmr = 580;
                                OMRSheetDetected = true;
                                float[] pts2 = new float[] { 0, 0, 0, heightOmr, widthOmr, heightOmr, widthOmr, 0 };

                                float[] pts1 = new float[8];
                                for (int k = 0; k < arrCornerXY.size() && k < pts1.length; k++) {

                                    if (k % 2 == 0) {
                                        pts1[k] = arrCornerXY.get(k).intValue();
                                        // print("OMR " + pts1[k]);
                                    } else {
                                        pts1[k] = arrCornerXY.get(k).intValue();
                                        print("Final OMR Points=> " + pts1[k - 1] + "," + pts1[k]);
                                    }

                                }

                                Mat omr = new Mat(heightOmr, widthOmr, CV_8UC1);
                                Mat y_src = new Mat(new Size(2, 4), CV_32F, new FloatPointer(pts1));
                                Mat y_dest = new Mat(new Size(2, 4), CV_32F, new FloatPointer(pts2));
                                Mat transformed = getPerspectiveTransform(y_src, y_dest);
                                warpPerspective(GrayImage, omr, transformed, new Size(widthOmr, heightOmr));
                                Rect cornerDummyOMR = new Rect(26, 151, 963, 408);

                                Mat omrNew = new Mat(omr, cornerDummyOMR);
                                resize(omrNew, omrNewResized, new Size(1000, 380));
                                threshold(omrNewResized, omrNewResized, 127, 255, THRESH_BINARY);
                                scan.EXT_SD_PATH = dir.getAbsolutePath();
                                scan.fileName = fileName;
                                if (saveImage) {
                                    imwrite(dir.getAbsolutePath() + "/middleImage_1" + fileName + ".jpg", omr);
                                    imwrite(dir.getAbsolutePath() + "/OMR_" + fileName + ".jpg", omrNewResized);
                                }
                                scan.processBubble(omrNewResized);
                                if (showSysout) {
                                    print("OMR Sheet Scan Completed " + scan.answered + "/" + scan.questions);
                                }

                                omrNewResized.release();
                                omrNew.release();
                                omr.release();
                                transformed.release();
                                y_src.release();
                                y_dest.release();
                                cornerDummyOMR.deallocate();

                                arrCornerXY.clear();
                                success = true;

                            } else {
                                Mat QRCode = new Mat(grabbedImageRoi, rects);
                                cvtColor(QRCode, QRCode, CV_BGR2RGB);
                                Mat GrayImageQR = new Mat(QRCode.rows(), QRCode.cols(), 1);
                                cvtColor(QRCode, GrayImageQR, CV_BGR2GRAY);
                                CLAHE c = createCLAHE(2.0, new Size(3, 3));
                                c.apply(GrayImageQR, GrayImageQR);

                                if (saveImage) {
                                    imwrite(dir.getAbsolutePath() + "/QRCode_" + fileName + ".jpg", GrayImageQR);
                                }
                                QRRecognizer recognizer = new QRRecognizer();
                                qrText = recognizer.readQRImage(GrayImageQR);
                                c.deallocate();
                                GrayImageQR.release();
                                QRCode.release();
                                success = true;
                            }
                        }
                    }

                }

            } else {
                print("Image is not  proper image");
                success = false;
            }
            if (saveImage) {
                if (grabbedImageRoi != null)
                    for (i = 0; i < cornerBoxes.length; i++) {
                        int x = cornerBoxes[i][0], y = cornerBoxes[i][1], cwidth = cornerBoxes[i][2], cheight = cornerBoxes[i][3];
                        rectangle(grabbedImageRoi, new org.bytedeco.javacpp.opencv_core.Point(x, y),
                                new org.bytedeco.javacpp.opencv_core.Point(x + cwidth, y + cheight), Scalar.MAGENTA, 1, 8, 0);
                    }
                imwrite(dir.getAbsolutePath() + "/5_colors" + fileName + "_Corners_" + cornerPoints + ".jpg", grabbedImageRoi);

            }

            GrayImage.release();

            grabbedImageRoi.release();
            arrRects.clear();
            arrOthers.clear();
            arrRectsArea.clear();

            // localImage.release();

        } catch (Error e) {
            e.printStackTrace();
        }

        return success;
    }

    public ArrayList<Integer> getContourPoints(Mat PolyXYPoints) {
        ArrayList<Integer> arr = new ArrayList<Integer>();

        for (int j = 0; j < PolyXYPoints.rows(); j++) {
            Mat row = PolyXYPoints.row(j);
            if (row.cols() >= 1) { // sometimes cols return 2 for 2 points
                // sometimes 1 but following code works for
                // both
                int x = row.getIntBuffer(0).get();
                int y = row.getIntBuffer(1).get();
                // print("X Y " + x + "," + y);
                arr.add(x);
                arr.add(y);
            }
        }

        return arr;
    }

    // need to optimize
    public boolean checkIfWithinCorners(ArrayList<Integer> pointsArray) {
        boolean cornerFound = true;
        if (fourCorners.size() == 0) {
            return false;
        }
        int ind = 0;
        for (int j = 0; j < pointsArray.size(); j = j + 2) {
            int x = pointsArray.get(j);
            int y = pointsArray.get(j + 1);
            for (ind = fourCorners.size() - 1; ind >= 0; ind--) {
                Polygon poly = fourCorners.get(ind);
                cornerFound = poly.contains(new Point(x, y));
                if (cornerFound) {
                    break;
                }
            }
        }
        if (cornerFound) {
            Polygon poly = fourCorners.get(ind);
            for (int j = 0; j < pointsArray.size(); j = j + 2) {
                int x = pointsArray.get(j);
                int y = pointsArray.get(j + 1);

                // print("X,Y " + x + " " + y);
                boolean b1 = poly.contains(new Point(x, y));
                cornerFound = cornerFound & b1;
                if (!cornerFound) {
                    break;
                }
            }

            if (cornerFound) {
                print("Corner Found [" + poly + "] as Image Found  At [" + pointsArray + "]");
                print("Removing Corner [" + poly + "] as Image Found  At [" + pointsArray + "]");
                // fourCorners.remove(ind);
                currentIndexFound=ind;
            }
        }
        return cornerFound;

    }

    public boolean checkAngle(ArrayList<Integer> bigContourPoints) {
        int topLeftX = 0, topLeftY = 0, topRightX = 0, topRightY = 0;
        int bottomLeftX = 0, bottomLeftY = 0, bottomRightX = 0, bottomRightY = 0;
        // 0 3
        topLeftX = bigContourPoints.get(0);
        topLeftY = bigContourPoints.get(1);

        topRightX = bigContourPoints.get(6);
        topRightY = bigContourPoints.get(7);

        bottomLeftX = bigContourPoints.get(2);
        bottomLeftY = bigContourPoints.get(3);

        bottomRightX = bigContourPoints.get(4);
        bottomRightY = bigContourPoints.get(5);
        double angle1 = Math.atan2(topLeftY - topRightY, topLeftX - topRightX) * 180 / Math.PI;

        double angle2 = (Math.atan2(bottomLeftY - bottomRightY, bottomLeftX - bottomRightX) * 180) / Math.PI;

        double diff = Math.abs(Math.abs(angle1) - Math.abs(angle2));

        boolean pass = false;
        if (diff <= 3) {
            pass = true;
            print(fileName + " Angle Between Corners " + angle1 + " " + angle2 + " Absolute Diffrence " + diff + " Forwarding");
        } else {
            print(fileName + " Angle Between Corners " + angle1 + " " + angle2 + " Absolute Diffrence " + diff + " Skipping ");
        }

//        double angletopLbottomL = Math.atan2(topLeftY - bottomLeftY,topLeftX - bottomLeftX) * 180 / Math.PI;
//
//        double angletopRbottomR= (Math.atan2(topRightY - bottomRightY,topRightX - bottomRightX) * 180) / Math.PI;
//        double diff2 = Math.abs(Math.abs(angletopLbottomL) - Math.abs(angletopRbottomR));
//        System.out.println(fileName + " Angle Between Corners " + angletopLbottomL + " " + angletopRbottomR + " Absolute Diffrence " + diff2 );
        return pass;
        // 1 2

    }

    // Returns TL TR BR BL
    public ArrayList<Integer> approximateRectanglePoints(ArrayList<Integer> bigContourPoints) {
        if (bigContourPoints.size() >= 4) {
            ArrayList<Integer> bigContourPointsFour = new ArrayList<Integer>();
            // for (int i = 0; i < cornerBoxes.length; i++) {
            //
            // }
            int[] dataxy = new int[] { cornerBoxes[0][0] + (cornerBoxes[0][2] / 2), cornerBoxes[0][1] + (cornerBoxes[0][3] / 2), // tl
                    cornerBoxes[2][0] + (cornerBoxes[2][2] / 2), cornerBoxes[2][1] + (cornerBoxes[2][3] / 2), // bl
                    cornerBoxes[3][0] + (cornerBoxes[3][2] / 2), cornerBoxes[3][1] + (cornerBoxes[3][3] / 2), // br
                    cornerBoxes[1][0] + (cornerBoxes[1][2] / 2), cornerBoxes[1][1] + (cornerBoxes[1][3] / 2), // tr
            };
            print("Input Points " + bigContourPoints);

            int minLeftSum = 100000, maxLeftSum = 0, minRightSum = 100000, maxRightSum = 0;
            for (int boundingIndex = 0; boundingIndex < dataxy.length; boundingIndex = boundingIndex + 2) {
                int bx = dataxy[boundingIndex];
                int by = dataxy[boundingIndex + 1];

                double min = 100000;
                int minIndex = -1;
                for (int i = 0; i < bigContourPoints.size(); i = i + 2) {
                    int x = bigContourPoints.get(i);
                    int y = bigContourPoints.get(i + 1);
                    double d = Math.sqrt(((y - by) * (y - by)) + ((x - bx) * (x - bx)));
                    if (d < min) {
                        min = d;
                        minIndex = i;
                    }
                }
                if (minIndex != -1) {
                    bigContourPointsFour.add(bigContourPoints.get((minIndex)));
                    bigContourPointsFour.add(bigContourPoints.get(minIndex + 1));
                }
            }

            print("Reordered Array [" + bigContourPointsFour + "]");

            bigContourPoints.clear();
            bigContourPoints = null;
            return bigContourPointsFour;
        }
        return bigContourPoints;
    }

    public void print(String message) {
        if (showSysout) {
            System.out.println(message);
        }
//         android.util.Log.i(LOG_TAG, message);
    }
    public void clear(){
        if(arrCorners!=null){
            arrCorners.clear();
        }
        if(arrRects!=null){
            arrRects.clear();
        }
        if(arrOthers!=null){
            arrOthers.clear();
        }

    }
}
