//package backup;
//
//import android.content.Context;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.os.Build;
//import android.util.Log;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Locale;
//
//
//import backup.OMRTestPhone;
//import util.opencv.AndroidConstants;
//import util.opencv.HttpView;
//import util.opencv.StringHelper;
//
///**
// * Created by technowings on 2/22/2018.
// */
//
//public class DeviceInformationModel {
//    Context context = null;
//    HttpView httpView = null;
//
//    DeviceInformationModel(Context context) {
//        this.context = context;
//
//        httpView = new HttpView();
//        httpView.initializeSSL(context);
//    }
//
//    public JSONObject fetchModelInfor() {
//        String manufacturer = Build.MANUFACTURER;
//        String model = Build.MODEL;
//        PackageManager manager = context.getPackageManager();
//        PackageInfo info = null;
//        String version = "";
//        try {
//            info = manager.getPackageInfo(context.getPackageName(), 0);
//            version = info.versionName;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//        String displayLanguages = Locale.getDefault().getDisplayLanguage();
//        int osSDKLevel = Build.VERSION.SDK_INT;
//        String osVersionLevel = Build.VERSION.RELEASE;
//        JSONObject device = new JSONObject();
//        try {
//
//            device.put("model", model);
//            device.put("os", "Android");
//            device.put("os_version", osVersionLevel);
//            device.put("os_language", displayLanguages);
//            device.put("app_version", version);
//            device.put("app_api_version", "0.0.1");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return device;
//    }
//
//    public JSONObject fetchQuestionsJSON(OMRTestPhone omr) {
//
//        JSONObject questions = new JSONObject();
//        try {
////        "questions": {
////            "total": "50",
////                    "answered": "40",
////                    "not_answered": "30"
////        },
//            questions.put("total", omr.scan.questions + "");
//            questions.put("answered", (omr.scan.answered) + "");
//            questions.put("not_answered", (omr.scan.questions - omr.scan.answered) + "");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return questions;
//
//    }
//
//    public JSONObject fetchAnswersJSON(OMRTestPhone omr) {
//
//        JSONObject questionsAnswers = new JSONObject();
//        try {
//
////        "questions": {
////            "total": "50",
////                    "answered": "40",
////                    "not_answered": "30"
////        },
//            for (int i = 0; i < omr.scan.questions; i++) {
//                if (omr.scan.answersMap.get(i) != null) {
//                    questionsAnswers.put("q" + i, omr.scan.answersMap.get(i) + "");
//                }
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return questionsAnswers;
//
//    }
//
//a
//    public boolean scanExamAndSend2Server(OMRTestPhone omr) {
//        try {
//            log("Scanning Exam Paper and Sending Answers to server");
//            JSONObject object = getScanExamPaperJSON(omr);
//            HashMap<String, String> parameters = new HashMap<String, String>();
//            parameters.put("x-api-key", AndroidConstants.OPTICKS_X_API_KEY);
//            parameters.put("Accept", "application/json");
//            parameters.put("Content-Type", "application/json");
//            String data = httpView.sendJSONServer(AndroidConstants.SCAN_EXAM(), object.toString(), parameters);
//            log("Result =>" + data);
//            JSONObject reader = new JSONObject(data);
//            String status = StringHelper.n2s(reader.get("status"));
//            if (status.equalsIgnoreCase("true")) {
//                return true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    public JSONObject getScanExamPaperJSON(OMRTestPhone omr) {
//
//        JSONObject scanExams = new JSONObject();
//        try {
//           boolean b= getAuthAPIToken();
//           log("Auth Token Status "+b);
//            scanExams.put("api_token", AndroidConstants.OPTICKS_API_TOKEN);
//            scanExams.put("exam_id", omr.qrText);
//            scanExams.put("user_id", AndroidConstants.USER_ID);
//            scanExams.put("device", AndroidConstants.USER_ID);
//            JSONObject device = fetchModelInfor();
//            JSONObject questions = fetchQuestionsJSON(omr);
//            JSONObject answers = fetchAnswersJSON(omr);
//            scanExams.put("device", device);
//            scanExams.put("questions", questions);
//            scanExams.put("answers", answers);
//            log("scanExams  " + scanExams.toString());
//
//            return scanExams;
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return null;
//
//    }
//
////    public void showTrusted() {
////
////
////
////
////        TrustManagerFactory tmf = null;
////        try {
////            KeyStore localTrustStore = KeyStore.getInstance("BKS");
////            InputStream in = getResources().openRawResource(R.raw.mytruststore);
////            localTrustStore.load(in, TRUSTSTORE_PASSWORD.toCharArray());
////
////            SchemeRegistry schemeRegistry = new SchemeRegistry();
////            schemeRegistry.register(new Scheme("http", PlainSocketFactory
////                    .getSocketFactory(), 80));
////            SSLSocketFactory sslSocketFactory = new SSLSocketFactory(trustStore);
////            schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));
////            HttpParams params = new BasicHttpParams();
////            ClientConnectionManager cm =
////                    new ThreadSafeClientConnManager(params, schemeRegistry);
////
////            HttpClient client = new DefaultHttpClient(cm, params);
////
////
////            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
//////        tmf.init((KeyStore) null);
////            X509TrustManager xtm = (X509TrustManager) tmf.getTrustManagers()[0];
////            for (X509Certificate cert : xtm.getAcceptedIssuers()) {
////                String certStr = "S:" + cert.getSubjectDN().getName() + "\nI:"
////                        + cert.getIssuerDN().getName();
////                Log.d(AndroidConstants.LOG_TAG, certStr);
////            }
////        } catch (NoSuchAlgorithmException e) {
////            e.printStackTrace();
////        }
////    }
//
//    public boolean getAuthAPIToken() {
//        String data = httpView.connectToServer(AndroidConstants.AUTH_URL());
//        try {
//            JSONObject reader = new JSONObject(data);
//            log("Got  Token==>  " + data);
//            boolean status = reader.getBoolean("status");
//            log("Got  status 2==>  " + status);
//            if (status) {
//
//                JSONObject jobject = (JSONObject) reader.get("data");
//                log("Got  jobject==>  " + jobject);
//                String api_token = jobject.getString("api_token");
//                JSONObject header = (JSONObject) jobject.get("header");
//                String x_api_key = header.getString("x-api-key");
//                AndroidConstants.OPTICKS_API_TOKEN = api_token;
//                AndroidConstants.OPTICKS_X_API_KEY = x_api_key;
//                AndroidConstants.LAST_API_TOKEN_TIMESTAMP = System.currentTimeMillis();
//                log("Got Opticks Token==> API_TOKEN " + AndroidConstants.OPTICKS_API_TOKEN + " X-API_KEY " + AndroidConstants.OPTICKS_X_API_KEY + " API_TOKEN_TIMESTAMP " + AndroidConstants.LAST_API_TOKEN_TIMESTAMP);
//                return true;
//            } else {
//                return false;
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    public void log(String message) {
//        Log.v(AndroidConstants.LOG_TAG, message);
//    }
//}